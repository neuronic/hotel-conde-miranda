# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210409194653) do

  create_table "ad_pdfs", force: :cascade do |t|
    t.string   "image",       limit: 255
    t.string   "name",        limit: 255
    t.boolean  "active",      limit: 1,   default: false
    t.integer  "building_id", limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",   null: false
    t.string   "encrypted_password",     limit: 255, default: "",   null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",                 limit: 1,   default: true
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "ads", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "image",       limit: 255
    t.string   "type",        limit: 255
    t.boolean  "active",      limit: 1
    t.integer  "building_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "apartment_translations", force: :cascade do |t|
    t.integer  "apartment_id", limit: 4,     null: false
    t.string   "locale",       limit: 255,   null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "description",  limit: 65535
  end

  add_index "apartment_translations", ["apartment_id"], name: "index_apartment_translations_on_apartment_id", using: :btree
  add_index "apartment_translations", ["locale"], name: "index_apartment_translations_on_locale", using: :btree

  create_table "apartment_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "building_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "apartment_types", ["building_id"], name: "index_apartment_types_on_building_id", using: :btree

  create_table "apartments", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "ref",               limit: 255
    t.text     "description",       limit: 65535
    t.integer  "apartment_type_id", limit: 4
    t.integer  "building_id",       limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "slug",              limit: 255
    t.string   "metatitle",         limit: 255
    t.string   "metadescription",   limit: 255
    t.string   "metakey",           limit: 255
    t.integer  "num_persons",       limit: 4
    t.integer  "num_bedrooms",      limit: 4
    t.string   "bed_kind",          limit: 255
    t.integer  "badroom",           limit: 4,     default: 0
    t.integer  "double_couch",      limit: 4,     default: 0
    t.integer  "living_room",       limit: 4
    t.string   "cleaning",          limit: 255
    t.boolean  "active",            limit: 1,     default: true
    t.datetime "deleted_at"
  end

  add_index "apartments", ["apartment_type_id"], name: "index_apartments_on_apartment_type_id", using: :btree
  add_index "apartments", ["building_id"], name: "index_apartments_on_building_id", using: :btree
  add_index "apartments", ["slug"], name: "index_apartments_on_slug", unique: true, using: :btree

  create_table "apartments_coupons", id: false, force: :cascade do |t|
    t.integer "apartment_id", limit: 4
    t.integer "coupon_id",    limit: 4
  end

  add_index "apartments_coupons", ["apartment_id"], name: "index_apartments_coupons_on_apartment_id", using: :btree
  add_index "apartments_coupons", ["coupon_id"], name: "index_apartments_coupons_on_coupon_id", using: :btree

  create_table "apartments_equipment", id: false, force: :cascade do |t|
    t.integer "apartment_id", limit: 4
    t.integer "equipment_id", limit: 4
  end

  add_index "apartments_equipment", ["apartment_id"], name: "index_apartments_equipment_on_apartment_id", using: :btree
  add_index "apartments_equipment", ["equipment_id"], name: "index_apartments_equipment_on_equipment_id", using: :btree

  create_table "apartments_reservations", id: false, force: :cascade do |t|
    t.integer "apartment_id",   limit: 4
    t.integer "reservation_id", limit: 4
  end

  create_table "billing_addresses", force: :cascade do |t|
    t.string   "country",        limit: 255
    t.string   "address",        limit: 255
    t.string   "address_1",      limit: 255
    t.string   "city",           limit: 255
    t.string   "zip_code",       limit: 255
    t.integer  "reservation_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "nationality",    limit: 255
  end

  create_table "billings", force: :cascade do |t|
    t.integer  "reservation_id", limit: 4
    t.string   "concept",        limit: 255
    t.float    "amount",         limit: 24
    t.integer  "night",          limit: 4
    t.integer  "service_id",     limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "tax",            limit: 4
    t.float    "price",          limit: 24
    t.string   "description",    limit: 255
    t.integer  "invoice_id",     limit: 4
  end

  add_index "billings", ["invoice_id"], name: "index_billings_on_invoice_id", using: :btree
  add_index "billings", ["reservation_id"], name: "index_billings_on_reservation_id", using: :btree
  add_index "billings", ["service_id"], name: "index_billings_on_service_id", using: :btree

  create_table "block_apartment_reservations", force: :cascade do |t|
    t.integer  "userable_id",   limit: 4
    t.string   "userable_type", limit: 255
    t.integer  "apartment_id",  limit: 4
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "block_apartment_reservations", ["apartment_id"], name: "index_block_apartment_reservations_on_apartment_id", using: :btree
  add_index "block_apartment_reservations", ["userable_type", "userable_id"], name: "bar_userable_index", using: :btree

  create_table "building_translations", force: :cascade do |t|
    t.integer  "building_id",                limit: 4,     null: false
    t.string   "locale",                     limit: 255,   null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.text     "cookies_information",        limit: 65535
    t.text     "privacy_policy_information", limit: 65535
    t.text     "legal_information",          limit: 65535
  end

  add_index "building_translations", ["building_id"], name: "index_building_translations_on_building_id", using: :btree
  add_index "building_translations", ["locale"], name: "index_building_translations_on_locale", using: :btree

  create_table "buildings", force: :cascade do |t|
    t.string   "name",                       limit: 255
    t.string   "address",                    limit: 255
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.integer  "invoice_address_id",         limit: 4
    t.text     "cookies_information",        limit: 65535
    t.text     "privacy_policy_information", limit: 65535
    t.text     "legal_information",          limit: 65535
    t.integer  "mail_config",                limit: 4,     default: 1
  end

  add_index "buildings", ["invoice_address_id"], name: "index_buildings_on_invoice_address_id", using: :btree

  create_table "buildings_channels", id: false, force: :cascade do |t|
    t.integer "building_id", limit: 4
    t.integer "channel_id",  limit: 4
  end

  add_index "buildings_channels", ["building_id"], name: "index_buildings_channels_on_building_id", using: :btree
  add_index "buildings_channels", ["channel_id"], name: "index_buildings_channels_on_channel_id", using: :btree

  create_table "calendar_dates", force: :cascade do |t|
    t.date     "date"
    t.text     "description", limit: 65535
    t.integer  "min_nights",  limit: 4
    t.boolean  "blocked",     limit: 1
    t.integer  "building_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "calendar_dates", ["building_id"], name: "index_calendar_dates_on_building_id", using: :btree
  add_index "calendar_dates", ["date", "building_id"], name: "index_calendar_dates_on_date_and_building_id", using: :btree

  create_table "channels", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "contact_name",  limit: 255
    t.string   "contact_email", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "color",         limit: 255
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.string   "surname",                  limit: 255
    t.string   "nif",                      limit: 255
    t.string   "email",                    limit: 255
    t.string   "phone",                    limit: 255
    t.string   "cif",                      limit: 255
    t.string   "address",                  limit: 255
    t.string   "city",                     limit: 255
    t.string   "country",                  limit: 255
    t.string   "business_name",            limit: 255
    t.string   "type",                     limit: 255
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.boolean  "juvenile",                 limit: 1,   default: false
    t.boolean  "agree_ads",                limit: 1,   default: false
    t.date     "date_birth"
    t.string   "passport",                 limit: 255
    t.string   "nationality",              limit: 255
    t.string   "gender",                   limit: 255
    t.date     "expedition_date_passport"
    t.date     "expedition_date_nif"
    t.integer  "building_id",              limit: 4
    t.boolean  "agree_policy_privacy",     limit: 1,   default: false
  end

  add_index "clients", ["building_id"], name: "index_clients_on_building_id", using: :btree

  create_table "clients_reservations", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "business_id",    limit: 4
    t.integer  "reservation_id", limit: 4
    t.string   "rol",            limit: 255
    t.integer  "client_id",      limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "clients_reservations", ["business_id"], name: "index_clients_reservations_on_business_id", using: :btree
  add_index "clients_reservations", ["client_id"], name: "index_clients_reservations_on_client_id", using: :btree
  add_index "clients_reservations", ["reservation_id"], name: "index_clients_reservations_on_reservation_id", using: :btree
  add_index "clients_reservations", ["user_id"], name: "index_clients_reservations_on_user_id", using: :btree

  create_table "config_calendars", force: :cascade do |t|
    t.integer  "nights",      limit: 4
    t.integer  "building_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "config_calendars", ["building_id"], name: "index_config_calendars_on_building_id", using: :btree

  create_table "coupon_types", force: :cascade do |t|
    t.integer  "coupon_id",  limit: 4
    t.integer  "service_id", limit: 4
    t.integer  "discount",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "coupon_types", ["coupon_id"], name: "index_coupon_types_on_coupon_id", using: :btree
  add_index "coupon_types", ["service_id"], name: "index_coupon_types_on_service_id", using: :btree

  create_table "coupons", force: :cascade do |t|
    t.string   "code",           limit: 255
    t.text     "description",    limit: 65535
    t.boolean  "status",         limit: 1
    t.integer  "discount",       limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "building_id",    limit: 4
    t.date     "deadline"
    t.integer  "minimum_nights", limit: 4
    t.integer  "maximum_nights", limit: 4
  end

  add_index "coupons", ["building_id"], name: "index_coupons_on_building_id", using: :btree

  create_table "coupons_reservation_conditions", id: false, force: :cascade do |t|
    t.integer "coupon_id",                limit: 4
    t.integer "reservation_condition_id", limit: 4
  end

  add_index "coupons_reservation_conditions", ["coupon_id"], name: "coupon_reservation_conditioin", using: :btree
  add_index "coupons_reservation_conditions", ["reservation_condition_id"], name: "reservation_condition_coupon", using: :btree

  create_table "discount_translations", force: :cascade do |t|
    t.integer  "discount_id", limit: 4,     null: false
    t.string   "locale",      limit: 255,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "description", limit: 65535
  end

  add_index "discount_translations", ["discount_id"], name: "index_discount_translations_on_discount_id", using: :btree
  add_index "discount_translations", ["locale"], name: "index_discount_translations_on_locale", using: :btree

  create_table "discounts", force: :cascade do |t|
    t.integer  "days",          limit: 4
    t.integer  "percentage",    limit: 4
    t.integer  "building_id",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "discount_type", limit: 255
    t.datetime "deleted_at"
  end

  add_index "discounts", ["deleted_at"], name: "index_discounts_on_deleted_at", using: :btree

  create_table "discounts_reservations", force: :cascade do |t|
    t.integer  "discount_id",    limit: 4
    t.integer  "reservation_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "equipment", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "key",        limit: 255
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "guests", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invalid_dates", force: :cascade do |t|
    t.date     "date"
    t.text     "description", limit: 65535
    t.integer  "building_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "invalid_dates", ["building_id"], name: "index_invalid_dates_on_building_id", using: :btree

  create_table "invoice_addresses", force: :cascade do |t|
    t.string   "corporate_name",            limit: 255
    t.string   "tax_domicile",              limit: 255
    t.string   "country",                   limit: 255
    t.string   "city",                      limit: 255
    t.string   "cif",                       limit: 255
    t.string   "email_contact",             limit: 255
    t.string   "web_site",                  limit: 255
    t.string   "logo",                      limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "registry_data",             limit: 255
    t.text     "free_text",                 limit: 65535
    t.string   "client_accountant_account", limit: 255
    t.string   "printed",                   limit: 255
    t.string   "invoice_description",       limit: 255
    t.string   "type_opration_one",         limit: 255
    t.string   "type_opration_two",         limit: 255
    t.string   "prefix_invoice_number",     limit: 255
  end

  create_table "invoice_items", force: :cascade do |t|
    t.integer  "invoice_id",       limit: 4
    t.string   "concept",          limit: 255
    t.text     "description",      limit: 65535
    t.integer  "vat_type",         limit: 4
    t.integer  "unit",             limit: 4
    t.float    "unit_price_base",  limit: 24
    t.float    "unit_price",       limit: 24
    t.float    "unit_vat",         limit: 24
    t.float    "total_price_base", limit: 24
    t.float    "total_price",      limit: 24
    t.float    "total_vat",        limit: 24
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "invoice_items", ["invoice_id"], name: "index_invoice_items_on_invoice_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "reservation_id", limit: 4
    t.string   "number",         limit: 255
    t.datetime "date"
    t.integer  "customer_id",    limit: 4
    t.string   "customer_type",  limit: 255
    t.text     "notes",          limit: 65535
    t.float    "total",          limit: 24
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "attachment",     limit: 255
  end

  add_index "invoices", ["customer_type", "customer_id"], name: "index_invoices_on_customer_type_and_customer_id", using: :btree
  add_index "invoices", ["reservation_id"], name: "index_invoices_on_reservation_id", using: :btree

  create_table "page_translations", force: :cascade do |t|
    t.integer  "page_id",         limit: 4,     null: false
    t.string   "locale",          limit: 255,   null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.text     "content",         limit: 65535
    t.string   "metatitle",       limit: 255
    t.string   "metadescription", limit: 255
    t.string   "metakey",         limit: 255
    t.text     "address",         limit: 65535
    t.string   "title",           limit: 255
  end

  add_index "page_translations", ["locale"], name: "index_page_translations_on_locale", using: :btree
  add_index "page_translations", ["page_id"], name: "index_page_translations_on_page_id", using: :btree

  create_table "pages", force: :cascade do |t|
    t.integer  "building_id",     limit: 4
    t.text     "content",         limit: 65535
    t.float    "longitude",       limit: 24
    t.float    "latitude",        limit: 24
    t.text     "address",         limit: 65535
    t.string   "metatitle",       limit: 255
    t.string   "metadescription", limit: 255
    t.string   "metakey",         limit: 255
    t.string   "type",            limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "pages", ["building_id"], name: "index_pages_on_building_id", using: :btree

  create_table "payment_summaries", force: :cascade do |t|
    t.float    "amount",         limit: 24
    t.string   "kind",           limit: 255
    t.integer  "reservation_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "method",         limit: 255
    t.integer  "transaction_id", limit: 4
  end

  add_index "payment_summaries", ["transaction_id"], name: "index_payment_summaries_on_transaction_id", using: :btree

  create_table "photo_translations", force: :cascade do |t|
    t.integer  "photo_id",   limit: 4,   null: false
    t.string   "locale",     limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "caption",    limit: 255
  end

  add_index "photo_translations", ["locale"], name: "index_photo_translations_on_locale", using: :btree
  add_index "photo_translations", ["photo_id"], name: "index_photo_translations_on_photo_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "photographable_id",   limit: 4
    t.string   "photographable_type", limit: 255
    t.string   "caption",             limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "picture",             limit: 255
    t.boolean  "accordion_pic",       limit: 1,   default: false
  end

  create_table "prices", force: :cascade do |t|
    t.date     "night"
    t.decimal  "price",                       precision: 8, scale: 2
    t.integer  "apartment_type_id", limit: 4
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  add_index "prices", ["apartment_type_id"], name: "index_prices_on_apartment_type_id", using: :btree

  create_table "promotion_translations", force: :cascade do |t|
    t.integer  "promotion_id", limit: 4,     null: false
    t.string   "locale",       limit: 255,   null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "title",        limit: 255
    t.text     "text",         limit: 65535
  end

  add_index "promotion_translations", ["locale"], name: "index_promotion_translations_on_locale", using: :btree
  add_index "promotion_translations", ["promotion_id"], name: "index_promotion_translations_on_promotion_id", using: :btree

  create_table "promotions", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "text",        limit: 65535
    t.boolean  "active",      limit: 1,     default: false
    t.string   "image",       limit: 255
    t.integer  "building_id", limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "question_translations", force: :cascade do |t|
    t.integer  "question_id", limit: 4,     null: false
    t.string   "locale",      limit: 255,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "question",    limit: 65535
    t.text     "response",    limit: 65535
  end

  add_index "question_translations", ["locale"], name: "index_question_translations_on_locale", using: :btree
  add_index "question_translations", ["question_id"], name: "index_question_translations_on_question_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.text     "question",   limit: 65535
    t.text     "response",   limit: 65535
    t.string   "image",      limit: 255
    t.integer  "page_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "questions", ["page_id"], name: "index_questions_on_page_id", using: :btree

  create_table "rectification_invoices", force: :cascade do |t|
    t.string   "number",             limit: 255
    t.integer  "invoice_id",         limit: 4
    t.string   "attachment",         limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.float    "base",               limit: 24
    t.float    "total",              limit: 24
    t.float    "tax_amount",         limit: 24
    t.text     "concept",            limit: 65535
    t.date     "date_rectification"
  end

  add_index "rectification_invoices", ["invoice_id"], name: "index_rectification_invoices_on_invoice_id", using: :btree

  create_table "reservation_condition_translations", force: :cascade do |t|
    t.integer  "reservation_condition_id", limit: 4,     null: false
    t.string   "locale",                   limit: 255,   null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "title",                    limit: 255
    t.text     "description",              limit: 65535
  end

  add_index "reservation_condition_translations", ["locale"], name: "index_reservation_condition_translations_on_locale", using: :btree
  add_index "reservation_condition_translations", ["reservation_condition_id"], name: "index_90d6dd980982e061cc0249073d470bac87ce7677", using: :btree

  create_table "reservation_conditions", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.text     "description",     limit: 65535
    t.text     "annotation",      limit: 65535
    t.float    "percent",         limit: 24
    t.boolean  "default",         limit: 1
    t.integer  "building_id",     limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.datetime "deleted_at"
    t.float    "payment_percent", limit: 24,    default: 50.0
    t.datetime "active"
  end

  add_index "reservation_conditions", ["building_id"], name: "index_reservation_conditions_on_building_id", using: :btree
  add_index "reservation_conditions", ["deleted_at"], name: "index_reservation_conditions_on_deleted_at", using: :btree

  create_table "reservations", force: :cascade do |t|
    t.integer  "building_id",              limit: 4
    t.string   "reference",                limit: 255
    t.date     "start_date"
    t.date     "end_date"
    t.float    "normal_price",             limit: 24
    t.float    "total_price",              limit: 24
    t.string   "status",                   limit: 255
    t.integer  "createable_id",            limit: 4
    t.string   "createable_type",          limit: 255
    t.text     "comment",                  limit: 65535
    t.boolean  "accepted_conditions",      limit: 1
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "channel_id",               limit: 4
    t.integer  "discount",                 limit: 4
    t.float    "admin_price",              limit: 24
    t.float    "rest",                     limit: 24
    t.string   "booking_reference",        limit: 255
    t.string   "process",                  limit: 255
    t.integer  "coupon_id",                limit: 4
    t.integer  "discount_id",              limit: 4
    t.integer  "reservation_condition_id", limit: 4
    t.text     "credit_card_number",       limit: 65535
    t.text     "credit_card_date",         limit: 65535
    t.text     "credit_card_holder",       limit: 65535
    t.integer  "childs",                   limit: 4
    t.integer  "adults",                   limit: 4
    t.text     "client_comments",          limit: 65535
  end

  add_index "reservations", ["coupon_id"], name: "index_reservations_on_coupon_id", using: :btree
  add_index "reservations", ["discount_id"], name: "index_reservations_on_discount_id", using: :btree
  add_index "reservations", ["reservation_condition_id"], name: "index_reservations_on_reservation_condition_id", using: :btree

  create_table "service_translations", force: :cascade do |t|
    t.integer  "service_id",  limit: 4,     null: false
    t.string   "locale",      limit: 255,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
  end

  add_index "service_translations", ["locale"], name: "index_service_translations_on_locale", using: :btree
  add_index "service_translations", ["service_id"], name: "index_service_translations_on_service_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.float    "price",        limit: 24
    t.boolean  "active",       limit: 1,   default: false
    t.integer  "building_id",  limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "published",    limit: 1
    t.float    "unique_price", limit: 24
    t.integer  "tax",          limit: 4
    t.boolean  "rest_one_day", limit: 1,   default: false
  end

  create_table "sliders", force: :cascade do |t|
    t.integer  "building_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "sliders", ["building_id"], name: "index_sliders_on_building_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.string   "reference",                limit: 255
    t.integer  "reservation_id",           limit: 4
    t.float    "amount",                   limit: 24
    t.text     "datas",                    limit: 65535
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "Ds_ErrorCode",             limit: 255
    t.string   "Ds_ErrorCode_description", limit: 255
    t.string   "Ds_Response",              limit: 255
    t.string   "Ds_Response_description",  limit: 255
    t.boolean  "sent",                     limit: 1,     default: false
    t.string   "type",                     limit: 255
    t.string   "token_paypal",             limit: 255
    t.string   "payer_id",                 limit: 255
    t.string   "lang",                     limit: 255
  end

  add_index "transactions", ["reservation_id"], name: "index_transactions_on_reservation_id", using: :btree

  create_table "valid_dates", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "validable_id",   limit: 4
    t.string   "validable_type", limit: 255
  end

  add_index "valid_dates", ["validable_type", "validable_id"], name: "index_valid_dates_on_validable_type_and_validable_id", using: :btree

  add_foreign_key "apartments", "apartment_types"
  add_foreign_key "apartments", "buildings"
  add_foreign_key "billings", "reservations"
  add_foreign_key "billings", "services"
  add_foreign_key "block_apartment_reservations", "apartments"
  add_foreign_key "buildings", "invoice_addresses"
  add_foreign_key "clients_reservations", "clients"
  add_foreign_key "clients_reservations", "reservations"
  add_foreign_key "config_calendars", "buildings"
  add_foreign_key "coupon_types", "coupons"
  add_foreign_key "coupon_types", "services"
  add_foreign_key "invalid_dates", "buildings"
  add_foreign_key "invoice_items", "invoices"
  add_foreign_key "pages", "buildings"
  add_foreign_key "prices", "apartment_types"
  add_foreign_key "questions", "pages"
  add_foreign_key "rectification_invoices", "invoices"
  add_foreign_key "reservation_conditions", "buildings"
  add_foreign_key "reservations", "discounts"
  add_foreign_key "reservations", "reservation_conditions"
  add_foreign_key "sliders", "buildings"
  add_foreign_key "transactions", "reservations"
end
