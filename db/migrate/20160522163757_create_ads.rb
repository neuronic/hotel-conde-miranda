class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :name
      t.string :image
      t.string :type
      t.boolean :active
      t.references :building

      t.timestamps null: false
    end
  end
end
