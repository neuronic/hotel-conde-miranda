class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :reference
      t.references :reservation, index: true, foreign_key: true
      t.integer :amount
      t.text :datas

      t.timestamps null: false
    end
  end
end
