class AddMailConfigToBuilding < ActiveRecord::Migration
  def change
    add_column :buildings, :mail_config, :integer, default: 1
  end
end
