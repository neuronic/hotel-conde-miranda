class CreateBlockApartmentReservations < ActiveRecord::Migration
  def change
    create_table :block_apartment_reservations do |t|
      t.references :userable, polymorphic: true, index: {:name => "bar_userable_index"}
      t.references :apartment, index: true, foreign_key: true
      t.date :start_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
