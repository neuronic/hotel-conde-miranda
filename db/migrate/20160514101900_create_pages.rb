class CreatePages < ActiveRecord::Migration
  def up
    create_table :pages do |t|
      t.references :building, index: true, foreign_key: true
      t.text :content
      t.float :longitude
      t.float :latitude
      t.text :address
      t.string :metatitle
      t.string  :metadescription
      t.string  :metakey
      t.string :type

      t.timestamps null: false
    end

    Page.create_translation_table! :content => :text, :metatitle => :string, :metadescription => :string, :metakey => :string, :address => :text
  end

  def down
    drop_table :pages 
    Page.drop_translation_table!
  end


end
