class AddDescriptionToSevices < ActiveRecord::Migration
  def up
  	Service.add_translation_fields! :description => :text
  end

  def down
  	remove_column :service_translations, :description
  end
end
