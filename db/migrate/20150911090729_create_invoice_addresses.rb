class CreateInvoiceAddresses < ActiveRecord::Migration
  def change
    create_table :invoice_addresses do |t|
      t.string :corporate_name
      t.string :tax_domicile
      t.string :country
      t.string :city
      t.string :cif
      t.string :email_contact
      t.string :web_site
      t.string :logo

      t.timestamps null: false
    end
  end
end
