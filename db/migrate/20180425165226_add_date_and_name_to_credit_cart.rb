class AddDateAndNameToCreditCart < ActiveRecord::Migration
  def change
    add_column :reservations, :credit_card_date, :text, default: nil
    add_column :reservations, :credit_card_holder, :text, default: nil
  end
end
