class CreatePaymentSummaries < ActiveRecord::Migration
  def change
    create_table :payment_summaries do |t|
      t.float :amount
      t.string :reason
      t.belongs_to :reservation
      t.timestamps null: false
    end

  end
end
