class CreateAdPdfs < ActiveRecord::Migration
  def change
    create_table :ad_pdfs do |t|
      t.string :image
      t.string :name
      t.boolean :active, :default => false
      t.references :building

      t.timestamps null: false
    end
  end
end
