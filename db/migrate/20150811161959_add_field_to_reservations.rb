class AddFieldToReservations < ActiveRecord::Migration
  def up
    add_column :reservations, :process, :string

    Reservation.unscoped.all.each do |r|
    	r.finished!
    end
  end

  def down
  	remove_column :reservations, :process
  end
end
