class CreateValidDates < ActiveRecord::Migration
  def change
    create_table :valid_dates do |t|
      t.date :start_date
      t.date :end_date
      t.references :coupon

      t.timestamps null: false
    end
  end
end
