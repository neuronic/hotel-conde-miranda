class AddActiveToApartment < ActiveRecord::Migration
  def change
  	add_column :apartments, :active, :boolean, :default => true
  end
end
