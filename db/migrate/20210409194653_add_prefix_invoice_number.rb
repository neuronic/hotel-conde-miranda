class AddPrefixInvoiceNumber < ActiveRecord::Migration
  def change
    add_column :invoice_addresses, :prefix_invoice_number, :string
  end
end
