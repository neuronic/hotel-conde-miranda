class CreateCmsQuestions < ActiveRecord::Migration
  def up
    create_table :questions do |t|
      t.text :question
      t.text :response
      t.string :image
      t.belongs_to :page, index: true, foreign_key: true

      t.timestamps null: false
    end
    Cms::Question.create_translation_table!(:question => :text, :response => :text)
    Building.all.each{ |building| building.create_faq_page(content: "Página de Preguntas Frecuentes") }
  end

  def down
    Cms::Question.drop_translation_table!
    drop_table :questions
  end
end
