class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.integer :days
      t.integer :percentage
      t.references :building

      t.timestamps null: false
    end
  end
end
