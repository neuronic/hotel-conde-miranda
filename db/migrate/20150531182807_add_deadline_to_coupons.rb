class AddDeadlineToCoupons < ActiveRecord::Migration
  def change
  	add_column :coupons, :deadline, :date
  end
end
