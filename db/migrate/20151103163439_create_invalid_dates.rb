class CreateInvalidDates < ActiveRecord::Migration
  def change
    create_table :invalid_dates do |t|
    	t.date :date
    	t.text :description
    	t.references :building, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
