class AddCreditCartToReservation < ActiveRecord::Migration
  def change
    add_column :reservation_conditions, :active, :datetime, default: nil
    add_column :reservations ,:credit_card_number, :text, default: nil
  end
end
