class AddTitleMetadescriptionMetakeywordToApartment < ActiveRecord::Migration
  def change
  	add_column :apartments, :metatitle, :string
  	add_column :apartments, :metadescription, :string
  	add_column :apartments, :metakey, :string
  end
end
