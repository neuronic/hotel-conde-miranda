class AddChildsAdultCommentsToReservation < ActiveRecord::Migration
  def change
    add_column :reservations, :childs, :integer
    add_column :reservations, :adults, :integer
    add_column :reservations, :client_comments, :text
  end
end
