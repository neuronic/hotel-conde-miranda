class AddDeletedAtToDiscounts < ActiveRecord::Migration
  def change
    add_column :discounts, :deleted_at, :datetime
    add_index :discounts, :deleted_at
  end
end
