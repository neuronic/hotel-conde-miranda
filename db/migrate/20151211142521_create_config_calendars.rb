class CreateConfigCalendars < ActiveRecord::Migration
  def change
    create_table :config_calendars do |t|
      t.integer :nights
      t.references :building, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
