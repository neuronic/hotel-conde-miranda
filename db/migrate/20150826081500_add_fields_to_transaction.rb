class AddFieldsToTransaction < ActiveRecord::Migration
  def change
  	add_column :transactions, :type, :string
  	add_column :transactions, :token_paypal, :string
  	add_column :transactions, :payer_id, :string
  end
end
