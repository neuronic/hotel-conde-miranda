class AddUserAndBusinessToClientsReservation < ActiveRecord::Migration
  def change
  	add_column :clients_reservations, :user_id, :integer, index: true
  	add_column :clients_reservations, :business_id, :integer, index: true
  	add_column :reservations, :rest, :float
  end
end
