class RelationHasManyApartmentsReservations < ActiveRecord::Migration
  def change
		create_table :apartments_reservations, id: false do |t|
      t.belongs_to :apartment
      t.belongs_to :reservation
    end
  end
end
