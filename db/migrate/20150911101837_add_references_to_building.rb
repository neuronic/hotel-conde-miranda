class AddReferencesToBuilding < ActiveRecord::Migration
  def change
  	add_reference :buildings, :invoice_address, index: true, foreign_key: true
  end
end
