class AddConditionsAndMoreThan18ToUser < ActiveRecord::Migration
  def change
  	add_column :clients, :juvenile, :boolean, default: false
  	add_column :clients, :agree_ads, :boolean, default: false
  end
end
