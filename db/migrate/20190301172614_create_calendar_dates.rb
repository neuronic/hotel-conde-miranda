class CreateCalendarDates < ActiveRecord::Migration
  def up
    create_table :calendar_dates do |t|
      t.date :date
      t.text :description
      t.integer :min_nights
      t.boolean :blocked
      t.references :building, index: true

      t.timestamps null: false
    end
    add_index :calendar_dates, [:date, :building_id]

    Building.all.each do |building|
      building.invalid_dates.each do |invalid_date|
        building.calendar_dates.create(
          date: invalid_date.date,
          blocked: true,
          description: invalid_date.description
        )
      end
    end
  end

  def down
    remove_index :calendar_dates, [:date, :building_id]
    drop_table :calendar_dates
  end
end
