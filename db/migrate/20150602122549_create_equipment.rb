class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :name

      t.timestamps null: false
    end

    create_table :apartments_equipment, id: false do |t|
      t.belongs_to :apartment, index: true
      t.belongs_to :equipment, index: true
    end

  end
end
