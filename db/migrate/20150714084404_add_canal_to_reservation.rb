class AddCanalToReservation < ActiveRecord::Migration
  def change
  	add_column :reservations, :channel_id, :integer, index: true
  	add_column :reservations, :discount, :integer
  end
end
