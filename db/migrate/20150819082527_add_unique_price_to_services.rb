class AddUniquePriceToServices < ActiveRecord::Migration
  def change
    add_column :services, :unique_price, :float
  end
end
