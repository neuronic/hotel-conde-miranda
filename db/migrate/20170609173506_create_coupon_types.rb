class CreateCouponTypes < ActiveRecord::Migration
  def change
    create_table :coupon_types do |t|
      t.belongs_to :coupon, index: true, foreign_key: true
      t.belongs_to :service, index: true, foreign_key: true
      t.integer :discount

      t.timestamps null: false
    end
  end
end
