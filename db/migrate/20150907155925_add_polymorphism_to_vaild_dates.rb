class AddPolymorphismToVaildDates < ActiveRecord::Migration
  def change
  	remove_reference :valid_dates, :coupon, index: true
  	add_reference :valid_dates, :validable, polymorphic: true, index: true

  end
end
