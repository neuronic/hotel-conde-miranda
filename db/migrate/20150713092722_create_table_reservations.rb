class CreateTableReservations < ActiveRecord::Migration
  def change
  	drop_table 'reservations' if ActiveRecord::Base.connection.table_exists? 'reservations'
		
		create_table :reservations do |t|
      t.references :building
      t.string :reference
      t.date :start_date
      t.date :end_date
      t.float :normal_price
      t.float :total_price
      t.string :status
      t.integer :createable_id
      t.string :createable_type
      t.text :comment
      t.boolean :accepted_conditions

      t.timestamps null: false
    end
  end
end
