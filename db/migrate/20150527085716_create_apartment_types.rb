class CreateApartmentTypes < ActiveRecord::Migration
  def change

    create_table :apartment_types do |t|
      t.string :name
      t.integer :num_persons
      t.integer :num_bedrooms
      t.references :building

      t.timestamps null: false
    end
    add_index :apartment_types, :building_id

    Building.create!(:name => "Palacio Pl Conde de Miranda", :address => "Plaza Conde de Miranda, 1" )
    building = Building.find_by_name("Palacio Pl Conde de Miranda").id
    ApartmentType.create!(:name => "Tipo A", :building_id => building)
    ApartmentType.create!(:name => "Tipo B", :building_id => building)
    ApartmentType.create!(:name => "Tipo C", :building_id => building)

  end
end
