class AddDatasToTransactions < ActiveRecord::Migration
  def change
  	add_column :transactions, :Ds_ErrorCode, :string
  	add_column :transactions, :Ds_ErrorCode_description, :string
  	add_column :transactions, :Ds_Response, :string
  	add_column :transactions, :Ds_Response_description, :string
  	add_column :transactions, :sent, :boolean, default: false
  end
end
