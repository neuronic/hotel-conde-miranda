class CreateRectificationInvoices < ActiveRecord::Migration
  def change
    create_table :rectification_invoices do |t|
      t.string :number
      t.references :invoice, index: true, foreign_key: true
      t.string :attachment

      t.timestamps null: false
    end
  end
end
