class AddTitleToPage < ActiveRecord::Migration
  def change
    Page.add_translation_fields! title: :string
  end
end
