class AddRegistryDataToInvoiceAddress < ActiveRecord::Migration
  def change
  	add_column :invoice_addresses, :registry_data, :string
  end
end
