class AddMinimumNightsToCoupon < ActiveRecord::Migration
  def change
  	add_column :coupons, :minimum_nights, :integer
  	add_column :coupons, :maximum_nights, :integer
  end
end
