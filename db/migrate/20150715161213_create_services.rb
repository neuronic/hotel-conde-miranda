class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.float :price
      t.boolean :active, :default => false
      t.references :building

      t.timestamps null: false
    end
  end
end
