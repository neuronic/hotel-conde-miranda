class CreateDiscountsReservations < ActiveRecord::Migration
  def change
    create_table :discounts_reservations do |t|
    	t.belongs_to :discount
      t.belongs_to :reservation
     
      t.timestamps null: false
    end

    Reservation.all.each do |r|
    	r.discounts_reservations.create(:discount_id => r.discount_id) 
    end
  end
end
