class AddInoviceInformationToBuilding < ActiveRecord::Migration
  def change
    add_column :invoice_addresses, :client_accountant_account, :string
    add_column :invoice_addresses, :printed, :string
    add_column :invoice_addresses, :invoice_description, :string
    add_column :invoice_addresses, :type_opration_one, :string
    add_column :invoice_addresses, :type_opration_two, :string
  end
end
