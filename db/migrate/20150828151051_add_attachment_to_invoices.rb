class AddAttachmentToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :attachment, :string
  end
end
