class AddAccordionPicToPhotos < ActiveRecord::Migration
  def change
  	add_column :photos, :accordion_pic, :boolean, :default => false
  end
end
