class AddFieldRestOneDayToServices < ActiveRecord::Migration
  def change
    add_column :services, :rest_one_day, :boolean, default: false
  end
end
