class AddFieldsToTypeApartments < ActiveRecord::Migration
  def up
  	remove_column :apartment_types, :num_persons
  	remove_column :apartment_types, :num_bedrooms

  	add_column :apartments, :num_persons, :integer
  	add_column :apartments, :num_bedrooms, :integer
  	add_column :apartments, :bed_kind, :string
  	add_column :apartments, :badroom, :integer, :default => 0
  	add_column :apartments, :double_couch, :integer, :default => 0
  end

  def down
  	
  	add_column :apartment_types, :num_persons, :integer
  	add_column :apartment_types, :num_bedrooms, :integer

  	remove_column :apartments, :num_persons
  	remove_column :apartments, :num_bedrooms
  	remove_column :apartments, :bed_kind
  	remove_column :apartments, :badroom
  	remove_column :apartments, :double_couch
  end
end
