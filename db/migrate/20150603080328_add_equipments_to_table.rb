class AddEquipmentsToTable < ActiveRecord::Migration
  def up
  	
    unless column_exists? :equipment, :key
  	 add_column :equipment, :key, :string
    end
  	Equipment.destroy_all
  	

  	Equipment.create(:name => "WIFI", :key => "wifi" )
  	Equipment.create(:name => "Chimenea", :key => "fireplace" )
  	Equipment.create(:name => 'TV plasma 42"', :key => "tv_42" )
  	Equipment.create(:name => "Nevera", :key => "firdge" )
  	Equipment.create(:name => "Vitrocerámica", :key => "cooktop" )
  	Equipment.create(:name => "Horno", :key => "oven" )
  	Equipment.create(:name => "Menaje de cocina", :key => "kitchenware" )
  	Equipment.create(:name => "Microondas", :key => "microwave" )
  	Equipment.create(:name => "Ropa de cama y toallas alta calidad", :key => "bed_sheet_and_towel" )
  	Equipment.create(:name => "Cafetera", :key => "coffee_maker" )
  	Equipment.create(:name => "Hervidor de agua", :key => "water_heater" )
  	Equipment.create(:name => "Secador de pelo", :key => "hairdryer" )
  	Equipment.create(:name => "Lavadora y secadora(zonas comunes", :key => "washing_machine_and_dryer" )
  end
  def down
  	Equipment.destroy_all
  	remove_column :equipment, :key
  end
end
