class AddBookingReferenceToReservations < ActiveRecord::Migration
  def change
  	add_column :reservations, :booking_reference, :string
  end
end
