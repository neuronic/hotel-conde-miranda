class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.date :night
      t.decimal :price, :precision => 8, :scale => 2
      t.references :apartment_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
