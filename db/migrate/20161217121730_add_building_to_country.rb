class AddBuildingToCountry < ActiveRecord::Migration
  def change
  	add_reference :clients, :building, index: true

  	building = Building.first
  	Client.all.each do |client|
  		client.update(building_id: building.id)
  	end
  end
end
