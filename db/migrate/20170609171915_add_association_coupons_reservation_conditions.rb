class AddAssociationCouponsReservationConditions < ActiveRecord::Migration
  def change
    create_table :coupons_reservation_conditions, id: false do |t|
      t.belongs_to :coupon, index: {name: "coupon_reservation_conditioin"}
      t.belongs_to :reservation_condition, index: {name: "reservation_condition_coupon"}
    end

  end
end
