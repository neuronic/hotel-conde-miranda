class AddPercentPaymentToConditions < ActiveRecord::Migration
  def change
    add_column :reservation_conditions, :payment_percent, :float, default: 50
  end
end
