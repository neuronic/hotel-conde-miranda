class CreatePhotos < ActiveRecord::Migration
  def up
    create_table :photos do |t|
      t.integer :photographable_id
      t.string :photographable_type
      t.string :caption

      t.timestamps null: false
    end

    Photo.create_translation_table! :caption => :string
  end

  def down
    drop_table :photos
    Photo.drop_translation_table!
  end
end
