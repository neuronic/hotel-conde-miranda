class CreateBillings < ActiveRecord::Migration
  def change
    create_table :billings do |t|
      t.references :reservation, index: true, foreign_key: true
      t.string :concept
      t.float :amount
      t.integer :night
      t.references :service, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
