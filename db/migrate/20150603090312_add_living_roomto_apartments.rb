class AddLivingRoomtoApartments < ActiveRecord::Migration
  def up
  	add_column :apartments, :living_room, :integer
  	Equipment.find_by(:key => "fireplace" ).destroy
  	Equipment.create(:name => "Armarios de gran capacidad. Aptos para largas estancias.", :key => "closet" )
  end

  def down
  	remove_column :apartments, :living_room
  	Equipment.create(:name => "Chimenea", :key => "fireplace" )
  	Equipment.find_by(:key => "closet" ).destroy
  end
end
