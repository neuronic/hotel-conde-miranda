class AddFieldsToBillings < ActiveRecord::Migration
  def change
  	add_column :billings, :tax, :integer
  	add_column :billings, :price, :float
  	add_column :billings, :description, :string
  	add_column :services, :tax, :integer
  end
end
