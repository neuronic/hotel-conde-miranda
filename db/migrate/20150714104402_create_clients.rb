class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :surname
      t.string :nif
      t.string :email
      t.string :phone
      t.string :cif
      t.string :address
      t.string :city
      t.string :country
      t.string :business_name
      t.string :type

      t.timestamps null: false
    end
  end
end
