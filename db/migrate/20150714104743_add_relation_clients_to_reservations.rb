class AddRelationClientsToReservations < ActiveRecord::Migration
  def change
		create_table :clients_reservations, id: false do |t|
      t.belongs_to :client
      t.belongs_to :reservation
    end
  end
end
