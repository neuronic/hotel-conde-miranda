class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :reservation, index: true
      t.references :billing_address, index: true
      t.string :number
      t.datetime :date
      t.references :customer, polymorphic: true, index: true
      t.text :notes
      t.float :total

      t.timestamps null: false
    end

  end
end
