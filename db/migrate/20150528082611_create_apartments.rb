class CreateApartments < ActiveRecord::Migration
  def up
    create_table :apartments do |t|
      t.string :name
      t.string :ref
      t.text :description
      t.references :apartment_type, index: true, foreign_key: true
      t.references :building, index: true, foreign_key: true

      t.timestamps null: false
    end

    Apartment.create_translation_table! :description => :text
  end

  def down
    drop_table :apartments
    Apartment.drop_translation_table!
  end

end
