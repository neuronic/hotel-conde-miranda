class AddAcceptedComminicationsToUser < ActiveRecord::Migration
  def change
    add_column :clients, :agree_policy_privacy, :boolean, default: false
    Client.all.update_all(agree_policy_privacy: true)
  end
end
