class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :title
      t.text :text
      t.boolean :active, default: false
      t.string :image
      t.references :building

      t.timestamps null: false
    end
  end

  def down
    drop_table :promotions 
  end

end
