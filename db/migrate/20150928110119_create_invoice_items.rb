class CreateInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoice_items do |t|
      t.references :invoice, index: true, foreign_key: true
      t.string :concept
      t.text :description
      t.integer :vat_type
      t.integer :unit
      t.float :unit_price_base
      t.float :unit_price
      t.float :unit_vat
      t.float :total_price_base
      t.float :total_price
      t.float :total_vat
      

      t.timestamps null: false
    end
  end
end
