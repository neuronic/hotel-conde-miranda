class CreateClientsReservations < ActiveRecord::Migration
  def up
    drop_table :clients_reservations if ActiveRecord::Base.connection.table_exists? 'clients_reservations'
    create_table :clients_reservations do |t|
      t.references :user, index: true
      t.references :business, index: true
      t.references :reservation, index: true, foreign_key: true
      t.string :rol
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  def down
    drop_table :clients_reservations
  end
end
