class ChangePaymentsSummaries < ActiveRecord::Migration
  def up
  	add_column :payment_summaries, :method, :string
  	rename_column :payment_summaries, :reason, :kind
  end

  def down
  	remove_column :payment_summaries, :method
  	rename_column :payment_summaries, :kind, :reason
  end
end
