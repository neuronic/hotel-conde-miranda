class AddDeletedAtToApartment < ActiveRecord::Migration
  def change
  	add_column :apartments, :deleted_at, :datetime, default: nil
  end
end
