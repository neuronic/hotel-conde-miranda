class AddNationalityToBillingAddress < ActiveRecord::Migration
  def up
    add_column :billing_addresses, :nationality, :string
  end

  def down
    remove_column :billing_addresses, :nationality
  end
end
