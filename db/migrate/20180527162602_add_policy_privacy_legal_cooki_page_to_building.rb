class AddPolicyPrivacyLegalCookiPageToBuilding < ActiveRecord::Migration
  def change
    add_column :buildings, :cookies_information, :text
    add_column :buildings, :privacy_policy_information, :text
    add_column :buildings, :legal_information, :text

    reversible do |dir|
      dir.up do
        Building.create_translation_table! cookies_information: :text, privacy_policy_information: :text, legal_information: :text
      end

      dir.down do
        Building.drop_translation_table!
      end
    end

  end
end
