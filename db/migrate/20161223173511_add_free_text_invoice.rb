class AddFreeTextInvoice < ActiveRecord::Migration
  def change
  	add_column :invoice_addresses, :free_text, :text
  end
end
