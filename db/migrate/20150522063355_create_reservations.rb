class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      # t.references :user, index: true, foreign_key: true
      # t.references :apartment, index: true, foreign_key: true
      t.date :start_date
      t.date :end_date
      t.float :total_price
      t.string :status
      # t.references :payment, index: true, foreign_key: true
      t.string :random_code
      t.boolean :accepted_conditions

      t.timestamps null: false
    end
  end
end
