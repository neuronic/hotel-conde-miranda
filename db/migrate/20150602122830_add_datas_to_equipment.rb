class AddDatasToEquipment < ActiveRecord::Migration
  def up
  	Equipment.create(:name => "Nevera")
  	Equipment.create(:name => "Vitrocerámica")
  	Equipment.create(:name => "Horno")
  	Equipment.create(:name => "Menaje de cocina")
  	Equipment.create(:name => "Microondas")
  	Equipment.create(:name => "Ropa de cama y toallas alta calidad")
  	Equipment.create(:name => "Cafetera")
  	Equipment.create(:name => "Hervidor de agua")
  	Equipment.create(:name => "Secador de pelo")
  	Equipment.create(:name => "Lavadora y secadora(zonas comunes")
  end
  def down
  	Equipment.where(:name => "Nevera").first.destroy
  	Equipment.where(:name => "Vitrocerámica").first.destroy
  	Equipment.where(:name => "Horno").first.destroy
  	Equipment.where(:name => "Menaje de cocina").first.destroy
  	Equipment.where(:name => "Microondas").first.destroy
  	Equipment.where(:name => "Ropa de cama y toallas alta calidad").first.destroy
  	Equipment.where(:name => "Cafetera").first.destroy
  	Equipment.where(:name => "Hervidor de agua").first.destroy
  	Equipment.where(:name => "Secador de pelo").first.destroy
  	Equipment.where(:name => "Lavadora y secadora(zonas comunes").first.destroy

  end
end
