class AddReferenceToDiscounts < ActiveRecord::Migration
  def change
    add_reference :reservations, :discount, index: true, foreign_key: true
  end
end
