class AddTranslateTableToDiscount < ActiveRecord::Migration
  def up
    Discount.create_translation_table! :description => :text
  end
  def down
    Discount.drop_translation_table!
  end
end
