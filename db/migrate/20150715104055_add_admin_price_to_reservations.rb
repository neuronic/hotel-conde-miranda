class AddAdminPriceToReservations < ActiveRecord::Migration
  def change
  	add_column :reservations, :admin_price, :float
  end
end
