class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :name
      t.string :contact_name
      t.string :contact_email

      t.timestamps null: false

      create_table :buildings_channels, id: false do |t|
      	t.belongs_to :building, index: true
      	t.belongs_to :channel, index: true
      end
    end
  end
end
