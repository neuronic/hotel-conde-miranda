class AddRelationshipToSummaryPayments < ActiveRecord::Migration
  def change
  	add_reference :payment_summaries, :transaction, index: true
  end
end
