class CreateBillingAddresses < ActiveRecord::Migration
  def change
    create_table :billing_addresses do |t|
      t.string :country
      t.string :address
      t.string :address_1
      t.string :city
      t.string :zip_code
      t.references :reservation

      t.timestamps null: false
    end
  end
end
