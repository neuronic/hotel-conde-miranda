class AddDateBirthPassportNationalityGenderExpeditionDateNifExpeditionDatePassport < ActiveRecord::Migration
  def change
  	add_column :clients, :date_birth, :date
  	add_column :clients, :passport, :string
  	add_column :clients, :nationality, :string
  	add_column :clients, :gender, :string
  	add_column :clients, :expedition_date_passport, :date
  	add_column :clients, :expedition_date_nif, :date
  end
end
