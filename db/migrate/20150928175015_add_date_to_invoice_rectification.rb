class AddDateToInvoiceRectification < ActiveRecord::Migration
  def change
    add_column :rectification_invoices, :date_rectification, :date
  end
end
