class PromotionsTranslations < ActiveRecord::Migration
  def up
    Promotion.create_translation_table! :title => :string, :text => :text
  end

  def down
    Promotion.drop_translation_table!
  end
end
