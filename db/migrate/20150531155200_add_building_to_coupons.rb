class AddBuildingToCoupons < ActiveRecord::Migration
  def change
  	add_column :coupons, :building_id, :integer
  	add_index :coupons, :building_id
  end
end
