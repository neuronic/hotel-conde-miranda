class CreateApartmentsCoupons < ActiveRecord::Migration
  def change
    create_table :apartments_coupons, id: false do |t|
      t.belongs_to :apartment, index: true
      t.belongs_to :coupon, index: true
    end
  end
end
