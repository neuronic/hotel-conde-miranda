class RelationshipReservationCoupons < ActiveRecord::Migration
  def change
  	add_reference :reservations, :coupon, index: true
  end
end
