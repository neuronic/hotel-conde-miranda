class CreateReservationConditions < ActiveRecord::Migration
  def up
    create_table :reservation_conditions do |t|
      t.string :title
      t.text :description
      t.text :annotation
      t.float :percent
      t.boolean :default
      t.references :building, index: true, foreign_key: true
      t.timestamps null: false
    end

    add_column :reservation_conditions, :deleted_at, :datetime
    add_index :reservation_conditions, :deleted_at

    #add_references_to_reservation
    add_reference :reservations, :reservation_condition, index: true, foreign_key: true
    ReservationCondition.create_translation_table! :title => :string, :description => :text
  end

  def down
    remove_reference :reservations, :reservation_condition, index: true, foreign_key: true
    drop_table :reservation_conditions 
    ReservationCondition.drop_translation_table!
  end
end
