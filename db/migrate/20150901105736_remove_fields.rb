class RemoveFields < ActiveRecord::Migration
  def change
  	add_column :rectification_invoices, :base, :float
  	add_column :rectification_invoices, :total, :float
  	add_column :rectification_invoices, :tax_amount, :float
  	add_column :rectification_invoices, :concept, :text
  	remove_reference :invoices, :billing_address
  	add_reference :billings, :invoice, index: true

  end
end
