class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.references :building, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
