namespace :setup do
  desc 'crate sitemap'
  task :create_sitemap => [:set_rails_env] do

    on primary(:app) do
      within release_path do
        with rails_env: fetch(:production) do
          execute :rake, "setup:create_sitemap"
        end
      end
    end

  end
end
