# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumbnail do
    #search
    process :resize_to_fit => [100, 100000]
  end

  version :promotion, :if => :attached_to_promotion? do
    process :resize_to_fit => [400, 300]
  end

  version :question, :if => :attached_to_question? do
    process :resize_to_fit => [Cms::Question::IMAGE_DIMENSION[:width], Cms::Question::IMAGE_DIMENSION[:height]]
  end

  def attached_to_promotion?(uploaded_file)
    model.class.to_s.underscore == 'promotion'
  end

  def attached_to_question?(uploaded_file)
    model.class.to_s.underscore == 'cms/question'
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # for image size validation
  # fetching dimensions in uploader, validating it in model
  attr_reader :width, :height
  before :cache, :capture_size

  def capture_size(file)
    if version_name.blank? # Only do this once, to the original version
      if file.path.nil? # file sometimes is in memory
        img = ::MiniMagick::Image::read(file.file)
        @width = img[:width]
        @height = img[:height]
      else
        @width, @height = `identify -format "%wx %h" #{file.path}`.split(/x/).map{|dim| dim.to_i }
      end
    end
  end

end
