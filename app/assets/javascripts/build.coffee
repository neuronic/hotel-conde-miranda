# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $.validate()
$(document).ready ->
  $('.dni').mask '99999999a'
  $('.phone').mask '+99 999 999 999'
  $('.dates').mask '99/99/9999'
  $('.times').mask '99:99'
  $('.credit_card').mask '9999 9999 9999 9999'
  $('.credit_card_date').mask '99/99'

  $('#tpv_payment').on 'click', (e) ->
    e.preventDefault()
    $.ajax(
      type: 'PUT'
      url: "/reservations/check_send/#{$('#Ds_Merchant_Order').val()}"
      dataType: 'script').success (json) ->
      $('form#tpv_payment_form').submit()