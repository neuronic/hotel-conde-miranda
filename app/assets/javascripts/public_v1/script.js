// menu burguer
$(function(){
    $('.btn_menu').click(function(){
      $('.btn_menu').toggleClass('close_btn_menu');
    });
    $('.btn_reserva_mobile').click(function(){
      $('.btn_reserva_mobile').toggleClass('close_reserva');
    });
});

// MAIN SLIDER
var slideWrapper = $(".main-slider"),
  lazyImages = slideWrapper.find('.slide-image'),
  lazyCounter = 0;
var slideVentajas = $(".ventajas_slider"),
  lazyImagesVentajas = slideVentajas.find('.slide_ventajas'),
  lazyCounterVentajas = 0;

// DOM Ready
$(function() {
  // Initialize
  slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
    lazyCounter++;
    if (lazyCounter === lazyImages.length){
      lazyImages.addClass('show');
    }
  });
  //start the slider
  slideWrapper.slick({
    //fade:true,
    autoplay: true,
    autoplaySpeed:5000,
    lazyLoad:"progressive",
    speed:1000,
    arrows:false,
    dots:true,
    cssEase:"cubic-bezier(0.87, 0.03, 0.41, 0.9)"
  });

  $(".slide_ventajas").addClass('show');
  slideVentajas.slick({
    //fade:true,
    autoplay: false,
    autoplaySpeed:5000,
    lazyLoad:"progressive",
    speed:1000,
    arrows:true,
    dots:false,
    cssEase:"cubic-bezier(0.87, 0.03, 0.41, 0.9)",

    prevArrow: '<i class="fa arrow-left"></i>',
    nextArrow: '<i class="fa arrow-right"></i>'

  });

});

// select persons
$("#persons").TouchSpin({
  buttondown_class: "btn",
  buttonup_class: "btn",
  min: 1,
  max: 6
});
