// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require cookies_eu
//= require functions
//= require jquery_nested_form
//= require jquery.form-validator
//= require maskedinput
//= require build
//= require twitter/bootstrap/tooltip
//= require carousel.min
//= require twitter/bootstrap/modal
//= require_self

$(function() {
  $('[data-toggle="tooltip"]').tooltip();
});

$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});


(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}(function( datepicker ) {

datepicker.regional['es'] = {
	closeText: 'Cerrar',
	prevText: '&#x3C;Ant',
	nextText: 'Sig&#x3E;',
	currentText: 'Hoy',
	monthNames: ['enero','febrero','marzo','abril','mayo','junio',
	'julio','agosto','septiembre','octubre','noviembre','diciembre'],
	monthNamesShort: ['ene','feb','mar','abr','may','jun',
	'jul','ago','sep','oct','nov','dic'],
	dayNames: ['domingo','lunes','martes','miércoles','jueves','viernes','sábado'],
	dayNamesShort: ['dom','lun','mar','mié','jue','vie','sáb'],
	dayNamesMin: ['D','L','M','X','J','V','S'],
	weekHeader: 'Sm',
	dateFormat: 'dd/mm/yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['es']);

return datepicker.regional['es'];

}));

$(document).on('click', 'a[disabled=disabled]', function(event){
  event.preventDefault();
});

$(function() {
  $('a[disabled=disabled]').each(function(link) {
    if ($(this).attr('title')) {
      $(this).tooltip();
    }
  });

  // reset conditions radio-buttons when reload page
  $('.reservation-condition-radio').each(function(link) {
    $(this).prop('checked', false)
  });

  $('.reservation-condition-radio-mobile').each(function(link) {
    $(this).prop('checked', false)
  });

  $('.reservation-condition-radio').change(function() {
    var table = $(this).closest('table');
    var link = $(table).find("a");
    var condition_reservation =  $(this).val();
    var href = $(link).attr('href');
    var reExp = new RegExp("(condition_reservation=)[0-9]+", 'ig');
    href = href.replace( reExp, 'condition_reservation='+condition_reservation );
    $(link).attr('href', href);

    var link = $(table).find('a[disabled=disabled]');
    removeDisabledAttrFromLink(link)
  });

  $('.reservation-condition-radio-mobile').change(function() {
    var table = $(this).closest('table');
    var link = $(table).find("a");
    var condition_reservation =  $(this).val();
    var href = $(link).attr('href');
    var reExp = new RegExp("(condition_reservation=)[0-9]+", 'ig');
    href = href.replace( reExp, 'condition_reservation='+condition_reservation );
    $(link).attr('href', href);

    var link = $(table).find('a[disabled=disabled]');
    removeDisabledAttrFromLink(link)
  });

  function removeDisabledAttrFromLink(link) {
    // remove disable button
    if (link.length > 0){
      $(link).removeAttr('disabled');
      $(link).tooltip('destroy');
    }
  }
});

$(function() {
  var visit = $.cookie("isshow"); ;
  if (visit==null){
    var offset = $('.caja-ventajas').offset();
    if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
      var height = $('.caja-ventajas').height();
      var width = $('.caja-ventajas').outerWidth();
      var top = offset.top-12 + height + "px";
      $('.modal-dialog').css( {
        'position': 'absolute',
        'top': top,
        'left': '50%',
        'transform': 'translateX(-50%)',
        'width': width
      });
    }
    $('#myModal').modal('show');
    var date = new Date()
    date.setDate(date.getDate() + 1);
    expire=new Date(date);
    document.cookie="isshow=1; expires="+expire;
  }
});
