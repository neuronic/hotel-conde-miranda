$(document).on('nested:fieldAdded', function(event){
  // this field was just inserted into your form
  var field = event.field;
  // it's a jQuery object already! Now you can find date input
  var dateField = field.find('.date');
  // and activate datepicker on it
  dateField.datetimepicker();
})


/* Data table */
/* ------------------ */
$(document).ready(function() {
  $('#data-table, .data-table').dataTable({
    'sPaginationType': 'full_numbers'
  });
  $('#data-table-finances').dataTable({
    'sPaginationType': 'full_numbers',
    "order": [[ 2, "desc" ]]
  });
});

$(document).ready(function() {
  $('#data-table-without-searching').dataTable({
    'sPaginationType': 'full_numbers',
    "searching": false
  });

});

$(document).ready(function() {
  $('#user-table-without-searching').dataTable({
    'sPaginationType': 'full_numbers',
    "searching": false
  });
});

//añadir ususario a apartamento
$(document).ready(function() {
  $('#add-user-table').dataTable({
    'sPaginationType': 'full_numbers',
    "order": [[ 0, "desc" ]]
  });
});

//crear reserva añadir usuario como titular
$(document).ready(function() {
  $('#client-table').dataTable({
    'sPaginationType': 'full_numbers',
    "order": [[ 0, "desc" ]]
  });
});

//crear reserva añadir usuario como titular
$(document).ready(function() {
  $('#business-table').dataTable({
    'sPaginationType': 'full_numbers',
    "order": [[ 0, "desc" ]]
  });
});




$(document).ready(function() {
  $(".prettyphoto").prettyPhoto({
    overlay_gallery: false, social_tools: false
  });
});

/* Sidebar navigation */
/* ------------------ */

/* Show navigation when the width is greather than or equal to 991px */

$(document).ready(function(){

  $(window).resize(function()
  {
    if($(window).width() >= 767){
      $(".side-nav").slideDown(150);
    }
	else{
	  $(".side-nav").slideUp(150);
	}
  });

});

/* ****************************************** */
/* Sidebar dropdown */
/* ****************************************** */

$(document).ready(function(){
  $(".sidebar-dropdown a").on('click',function(e){
      e.preventDefault();

      if(!$(this).hasClass("open")) {
        // hide any open menus and remove all other classes
        $(".sidebar .side-nav").slideUp(150);
        $(".sidebar-dropdown a").removeClass("open");

        // open our new menu and add the open class
        $(".sidebar .side-nav").slideDown(150);
        $(this).addClass("open");
      }

      else if($(this).hasClass("open")) {
        $(this).removeClass("open");
        $(".sidebar .side-nav").slideUp(150);
      }
  });

});

/* ****************************************** */
/* Sidebar dropdown menu */
/* ****************************************** */

$(document).ready(function(){

  $(".has_submenu > a").click(function(e){
    e.preventDefault();
    var menu_li = $(this).parent("li");
    var menu_ul = $(this).next("ul");

    if(menu_li.hasClass("open")){
      menu_ul.slideUp(150);
      menu_li.removeClass("open");
	  $(this).find("span").removeClass("fa-caret-up").addClass("fa-caret-down");
    }
    else{
      $(".side-nav > li > ul").slideUp(150);
      $(".side-nav > li").removeClass("open");
      menu_ul.slideDown(150);
      menu_li.addClass("open");
	  $(this).find("span").removeClass("fa-caret-down").addClass("fa-caret-up");
    }
  });

});

/* ****************************************** */
/* Slim Scroll */
/* ****************************************** */

// $(function(){
//     $('.scroll').slimScroll({
//         height: '315px',
// 		size: '5px',
// 		color:'rgba(50,50,50,0.3)'
//     });
// });

/* ****************************************** */
/* Knob */
/* ****************************************** */

// $(function() {
//     $(".knob").knob();
// });

/* ****************************************** */
/* JS for UI Tooltip */
/* ****************************************** */



/* ****************** */
/* Date Time Picker JS */
/* ****************** */

// $(function() {
// 	$('#datetimepicker1').datetimepicker({
// 		pickTime: false,
//     minDate: "0d",
//     maxDate: "+2M"
// 	});
// });

// $(function() {
// 	$('#datetimepicker2').datetimepicker({
// 		pickDate: false
// 	});
// });

/* ****************************** */
/* Slider */
/* ******************************* */

    // $(function() {
    //     // Horizontal slider
    //     $( "#master1, #master2" ).slider({
    //         value: 60,
    //         orientation: "horizontal",
    //         range: "min",
    //         animate: true
    //     });

    //     $( "#master4, #master3" ).slider({
    //         value: 80,
    //         orientation: "horizontal",
    //         range: "min",
    //         animate: true
    //     });

    //     $("#master5, #master6").slider({
    //         range: true,
    //         min: 0,
    //         max: 400,
    //         values: [ 75, 200 ],
    //         slide: function( event, ui ) {
    //             $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
    //         }
    //     });


    //     // Vertical slider
    //     $( "#eq > span" ).each(function() {
    //         // read initial values from markup and remove that
    //         var value = parseInt( $( this ).text(), 10 );
    //         $( this ).empty().slider({
    //             value: value,
    //             range: "min",
    //             animate: true,
    //             orientation: "vertical"
    //         });
    //     });
    // });

/* ****************** */
/* Calendar */
/* ****************************************** */

  // $(document).ready(function() {

  //   var date = new Date();
  //   var d = date.getDate();
  //   var m = date.getMonth();
  //   var y = date.getFullYear();

  //   $('#calendar').fullCalendar({
  //     header: {
  //       left: 'prev',
  //       center: 'title',
  //       right: 'month,agendaWeek,agendaDay,next'
  //     },
  //     editable: true,
  //     events: [
  //       {
  //         title: 'All Day Event',
  //         start: new Date(y, m, 1)
  //       },
  //       {
  //         title: 'Long Event',
  //         start: new Date(y, m, d-5),
  //         end: new Date(y, m, d-2)
  //       },
  //       {
  //         id: 999,
  //         title: 'Repeating Event',
  //         start: new Date(y, m, d-3, 16, 0),
  //         allDay: false
  //       },
  //       {
  //         id: 999,
  //         title: 'Repeating Event',
  //         start: new Date(y, m, d+4, 16, 0),
  //         allDay: false
  //       },
  //       {
  //         title: 'Meeting',
  //         start: new Date(y, m, d, 10, 30),
  //         allDay: false
  //       },
  //       {
  //         title: 'Lunch',
  //         start: new Date(y, m, d, 12, 0),
  //         end: new Date(y, m, d, 14, 0),
  //         allDay: false
  //       },
  //       {
  //         title: 'Birthday Party',
  //         start: new Date(y, m, d+1, 19, 0),
  //         end: new Date(y, m, d+1, 22, 30),
  //         allDay: false
  //       },
  //       {
  //         title: 'Click for Google',
  //         start: new Date(y, m, 28),
  //         end: new Date(y, m, 29),
  //         url: 'http://google.com/'
  //       }
  //     ]
  //   });

  // });

/* ****************************************** */
/* Form Validate // Form Validation */
/* ****************************************** */

// $.validator.setDefaults({
// 	submitHandler: function() { alert("submitted!"); }
// });

// $().ready(function() {

// 	// validate signup form on keyup and submit
// 	$("#signupForm").validate({
// 		rules: {
// 			firstname: "required",
// 			lastname: "required",
// 			username: {
// 				required: true,
// 				minlength: 2
// 			},
// 			password: {
// 				required: true,
// 				minlength: 5
// 			},
// 			confirm_password: {
// 				required: true,
// 				minlength: 5,
// 				equalTo: "#password"
// 			},
// 			email: {
// 				required: true,
// 				email: true
// 			},
// 			gender: {
// 				required: true
// 			},
// 			limit: {
// 				required: true
// 			},
// 			location: {
// 				required: true
// 			},
// 			agree: "required"
// 		},
// 		messages: {
// 			firstname: "Please enter your firstname",
// 			lastname: "Please enter your lastname",
// 			username: {
// 				required: "Please enter a username",
// 				minlength: "Your username must consist of at least 2 characters"
// 			},
// 			password: {
// 				required: "Please provide a password",
// 				minlength: "Your password must be at least 5 characters long"
// 			},
// 			confirm_password: {
// 				required: "Please provide a password",
// 				minlength: "Your password must be at least 5 characters long",
// 				equalTo: "Please enter the same password as above"
// 			},
// 			email: "Please enter a valid email address",
// 			gender: "Please choose gender",
// 			limit: "Please choose at least one option",
// 			location: "Please select your location",
// 			agree: "Please accept our policy"
// 		}
// 	});

// });

/* ****************************************** */
/* Form Wizard */
/* ****************************************** */

// $(document).ready(function() {

// 	$("#wizard").steps({
// 		headerTag: "h2",
// 		transitionEffect: "slide",
// 		autoFocus: true
// 	});
// });

/* ****************************************** */
/* Compose Mail Form word processor JS */
/* ****************************************** */

// $('.textarea').wysihtml5();
// $(prettyPrint);

/* ****************************************** */
/* Task widget */
/* ****************************************** */

$(document).ready(function(){
   $('.tasks-widget input:checkbox').removeAttr('checked').on('click', function(){
      if(this.checked){
         $(this).next("span").css('text-decoration','line-through');
      }
      else{
         $(this).next("span").css('text-decoration','none');
      }
	});
});



function previewFile() {

  var preview = document.querySelector('img');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

//CALENDARIO RESERVAS

Date.prototype.getDayOfWeek = function(){
    return ["D","L","M","X","J","V","S"][ this.getDay() ];
};

$(document).ready(function(){
  if($('.contenedor-calendario').length > 0){
    //inicializo variables para pintar el calendario
    var meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
    var month = new Date();
    var mes = month.getMonth() + 1;
    var ano = new Date();
    var year = ano.getFullYear();

    for(var i = 0; i < dias(mes); i++ ){
      var date = new Date(year, mes-1, i+1);
      var diaMes = $('<td class="dia-mes">'+ date.getDayOfWeek() + (i+1) +'</td>');
      $(".calendario-reservas thead tr").append(diaMes);
    }

    $(".nombre-mes").html(meses[+mes - 1] +" "+ year);

    //llamo a la data por primera vez y pinto el calendario
    traerData(mes, year);

    //enlaces de prev y next
    $(".enlace-mes").click(function(e){
      e.preventDefault();

      if($(this).hasClass("prev-mes")){
        if(mes > 1){
          mes--;
        }else{
          mes = 12;
          year--;
        }


        $(".nombre-mes").html(meses[+mes - 1] +" "+ year);

        $(".dia-mes").remove();

        for(var i = 0; i < dias(mes); i++ ){
          var date = new Date(year, mes-1, i+1);
          var diaMes = $('<td class="dia-mes">'+ date.getDayOfWeek() + (i+1)  +'</td>');
          $(".calendario-reservas thead tr").append(diaMes);
        }

        $(".calendario-reservas tbody").empty();
        traerData(mes, year);

      }else{
        if(mes < 12){
          mes++;
        }else{
          mes = 1;
          year++;
        }

        $(".nombre-mes").html(meses[+mes - 1] +" "+ year);

        $(".dia-mes").remove();

        for(var i = 0; i < dias(mes); i++ ){
          var date = new Date(year, mes-1, i+1);
          var diaMes = $('<td class="dia-mes">'+date.getDayOfWeek()+ (i+1) +'</td>');
          $(".calendario-reservas thead tr").append(diaMes);
        }

        $(".calendario-reservas tbody").empty();
        traerData(mes, year);
      }
    });
  }
  //funciones mágicas

  function traerData(mes, year){
    $.ajax({
      url:"/admin/buildings/"+$('#building_id').text()+"/reservations/read_reservations_for_custom_calendar",
      data: {month: mes, year: year},//AQUI VA LA URL
      success:function(data){
        var apartamentos = data;

        for(var i = 0; i < apartamentos.length; i++){
          var row = $('<tr></tr>');
          var apt = $('<td class="col-md-1 col-xs-1">'+apartamentos[i].reference+'</td>');
          $(row).append(apt);
          for(var j = 0; j < dias(mes);j++){
            var celda = $('<td style="width: 3%; background-color: rgb(244, 255, 244);" class="apt-'+i+'-'+(j+1)+'"></td>');
            $(row).append(celda);
          }
          $(".calendario-reservas tbody").append(row);

          if(apartamentos[i].reservations.length > 0){
            for(var j = 0; j < apartamentos[i].reservations.length; j++){
              var ini = apartamentos[i].reservations[j].start_date.split("-");
              var inicio = new Date(ini[0],parseInt(ini[1])-1,ini[2]);
              var date = new Date(ini[0],parseInt(ini[1])-1,parseInt(ini[2])+1);
              var fi = apartamentos[i].reservations[j].end_date.split("-");
              var fin = new Date(fi[0],parseInt(fi[1])-1,fi[2]);
              var date_fin = new Date(fi[0],parseInt(fi[1])-1,parseInt(fi[2])-1);
              if (inicio.getMonth()+1 == mes && inicio.getFullYear() == year){
                $('.apt-'+i+'-'+inicio.getDate()).html('<a style="color: black;position: relative;z-index: 1190;" data-placement="top" data-toggle="tooltip" title="Reserva REF: '+apartamentos[i].reservations[j].reference+'"><i class="fa fa-eye"></i></a>');
                $('.apt-'+i+'-'+inicio.getDate()).addClass("reservado-inicio");
                $('.apt-'+i+'-'+inicio.getDate()).addClass("reservado");
                $('.apt-'+i+'-'+inicio.getDate()).addClass(apartamentos[i].reservations[j].color);
              }
              for (date; date <= date_fin; date.setDate(date.getDate() + 1)) {
                if (date.getMonth()+1 == mes && date.getFullYear() == year){
                  $('.apt-'+i+'-'+date.getDate()).addClass("reservado");
                  $('.apt-'+i+'-'+date.getDate()).addClass(apartamentos[i].reservations[j].color);
                }
              }
              if (fin.getMonth()+1 == mes && fin.getFullYear() == year){
                $('.apt-'+i+'-'+fin.getDate()).addClass("reservado-final");
                $('.apt-'+i+'-'+fin.getDate()).addClass("reservado");
                $('.apt-'+i+'-'+fin.getDate()).addClass(apartamentos[i].reservations[j].color);
              }
            }
          }
        }
      }

    });
  }

  function dias(mes){
    if(+mes < 8){
      if(+mes % 2 == 0){
        if(mes == 2){
          if(bisiesto(year)){
            return 29;
          }else{
            return 28;
          }
        }else{
          return 30;
        }
      }else{
        return 31;
      }
    }else{
      if(+mes % 2 == 0){
        return 31;
      }else{
        return 30;
      }
    }
  }


  function bisiesto(year){
    if(year % 4 == 0){
      if(year % 100 == 0){
        if(year % 400 == 0){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    }else{
      return false;
    }
  }
});
