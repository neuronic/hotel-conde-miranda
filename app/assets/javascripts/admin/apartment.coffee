$(document).ready ->
  options = onSwitchChange: (event, state) ->
    $.ajax(url:"/admin/buildings/#{$(this).attr('data_building')}/apartments/#{$(this).attr('id')}/change_clean", type: 'PUT', dataType: "script")
  $('[name="my-checkbox"]').bootstrapSwitch options

  options2 = onSwitchChange: (event, state) ->
    $.ajax(url:"/admin/buildings/#{$(this).attr('data_building')}/apartments/#{$(this).attr('id')}/change_status", type: 'PUT', dataType: "script")
  $('[name="my-checkbox1"]').bootstrapSwitch options2

  option_general = onSwitchChange: (event, state) ->
    $.ajax(url: $(this).data("url"), type: $(this).data("method"), dataType: "script")
  $('[name="active"]').bootstrapSwitch option_general

$(document).ready ->
  date = new Date
  d = date.getDate()
  m = date.getMonth()
  y = date.getFullYear()
  $('#calendar_apartment').fullCalendar

    monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "December"],
    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    dayNamesShort: ["Dom", "Lun", "Mar", "Mier", "Juev", "Vier", "Sab"],
    buttonText:
      today: "Hoy",
      month: "Mes",
      week: "Semana",
      day: "Por día"

    header:
      left: 'title'
      right: 'prev,next today,month,agendaWeek,agendaDay'
      #right: 'prev,next,month'
    editable: true,
    selectable: true,
    select: (start, end, jsEvent, view) ->
      # start contains the date you have selected
      # end contains the end date.
      # Caution: the end date is exclusive (new since v2).
      start_date = moment(start).format()
      end_date = moment(end).format()
      $.get "/admin/buildings/#{$('#building_id').text()}/reservations/#{$('#apartment_id').text()}/summary_reservation", start_date: start_date, end_date: end_date

    lang: 'es',
    timeFormat: ' '

    eventSources: [{
      url: "/admin/buildings/#{$('#building_id').text()}/apartments/#{$('#apartment_id').text()}/read_reservations"
    }],

    dayClick: (date, jsEvent, view) ->
      day = date.getDate()
      month = date.getMonth()
      year = date.getFullYear()
      hour = date.getHours()
      minutes = date.getMinutes()

      if ($(this).hasClass('fc-disabled') or $(this).hasClass('fc-past'))
        alert("No puedes cambiar el precio de días anteriores")
      else
        $.get "/admin/buildings/#{$('#building_id').text()}/reservations/new", day: day, month: month+1, year: year


  return
