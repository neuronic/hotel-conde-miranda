$(document).ready ->
  if($("#map").length != 0)
    $(document).on 'change', '#cms_location_longitude, #cms_location_latitude', {}, (e) ->
      lon = $('#cms_location_longitude').val()
      lat = $('#cms_location_latitude').val()
      if (lon.length!=0 && lat.length!=0)
        initMap(lon, lat)

    initMap = (lon, lat) ->
      handler = Gmaps.build('Google')
      handler.buildMap {
        provider: {}
        internal: id: 'map'
      }, ->
        markers = handler.addMarkers([ {
          'lat': lat
          'lng': lon
        } ])
        handler.bounds.extendWith markers
        handler.fitMapToBounds()
        handler.getMap().setZoom 17
        return