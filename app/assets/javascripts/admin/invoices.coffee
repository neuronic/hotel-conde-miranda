# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->

  if document.location.pathname.search(/\/invoices\//) != -1 || document.location.pathname.search(/\/reservations\//) 
    window.NestedFormEvents::insertFields = (content, assoc, link) ->
      if $(link).attr('id') == 'add_line_item'
        $tr = $(link).closest('tr')
        $(content).insertBefore $tr
      else
        targe = $(link).data('target')
        if targe
          return $(content).appendTo($(target))
        else
          return $(content).insertBefore(link)

$(document).on 'click', 'select[name*=\'invoice[billings_attributes]\']', {}, (e) ->
  id_field = $(this).attr('id')

  if id_field.indexOf("service_id") > -1
    $.ajax(
      type: 'GET'
      url: "/admin/buildings/#{$('#building_id').text()}/services/#{$(this).val()}/read_datas"
      dataType: 'JSON').success (json) ->
      id_night = id_field.replace('service_id', 'night')
      id_price = id_field.replace('service_id', 'price')
      id_amount = id_field.replace('service_id', 'amount')
      id_tax = id_field.replace('service_id', 'tax')

      if json.unique_price == null || json.unique_price == ""
        $("##{id_night}").val($('#nights').text())
        $("##{id_price}").val(json.price)
        $("##{id_amount}").val(parseFloat(json.price * $("##{id_night}").val()).toFixed(2))

      else
        $("##{id_night}").val(1)
        $("##{id_price}").val(json.unique_price)
        $("##{id_amount}").val(parseFloat(json.unique_price * $("##{id_night}").val()).toFixed(2))
      $("##{id_tax}").val(json.tax)
      total_price()


$(document).on 'change', '.price', {}, (e) ->
  id_field = $(this).attr('id')
  id_amount = id_field.replace('price', 'amount')
  id_night = id_field.replace('price', 'night')
  amount = $(this).val() * $("##{id_night}").val()
  $("##{id_amount}").val(parseFloat(amount).toFixed(2))
  total_price()

$(document).on 'change', '.night', {}, (e) ->
  id_field = $(this).attr('id')
  id_amount = id_field.replace('night', 'amount')
  id_price = id_field.replace('night', 'price')
  amount = $(this).val() * $("##{id_price}").val()
  $("##{id_amount}").val(parseFloat(amount).toFixed(2))
  total_price()

$(document).on 'change', '.amount', {}, (e) ->
  total_price()

total_price = ->
  total = 0.0
  $('.amount').each (i, obj) ->
    total = total + parseFloat($(obj).val())
  $('.price_with_tax').text(total.toFixed(2) + ' €')