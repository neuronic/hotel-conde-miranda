$(document).ready ->

  date = new Date
  d = date.getDate()
  m = date.getMonth()
  y = date.getFullYear()
  $('#calendar').fullCalendar

    monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "December"],
    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    dayNamesShort: ["Dom", "Lun", "Mar", "Mier", "Juev", "Vier", "Sab"],
    buttonText:
      today: "Hoy",
      month: "Mes",
      week: "Semana",
      day: "Por día"

    header:
      left: 'title'
      #right: 'prev,next today,month,agendaWeek,agendaDay'
      right: 'prev,next,month'
    editable: true,
    lang: 'es',
    timeFormat: ' '

    eventSources: [{
      url: "/admin/buildings/#{$('#building_id').text()}/apartment_types/#{$('#apartment_type_id').text()}/prices/read_prices"
    }],

    dayClick: (date, jsEvent, view) ->
      day = date.getDate()
      month = date.getMonth()
      year = date.getFullYear()
      hour = date.getHours()
      minutes = date.getMinutes()

      if ($(this).hasClass('fc-disabled') or $(this).hasClass('fc-past'))
        alert("No puedes cambiar el precio de días anteriores")
      else
        $.get "/admin/buildings/#{$('#building_id').text()}/apartment_types/#{$('#apartment_type_id').text()}/prices/new", day: day, month: month+1, year: year


    droppable: true
    drop: (date, allDay) ->
      # this function is called when something is dropped
      # retrieve the dropped element's stored Event Object
      originalEventObject = $(this).data('eventObject')
      # we need to copy it, so that multiple events don't have a reference to the same object
      copiedEventObject = $.extend({}, originalEventObject)
      # assign it the date that was reported
      copiedEventObject.start = date
      copiedEventObject.allDay = allDay
      # render the event on the calendar
      # the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
      $('#calendar').fullCalendar 'renderEvent', copiedEventObject, true
      # is the "remove after drop" checkbox checked?
      if $('#drop-remove').is(':checked')
        # if so, remove the element from the "Draggable Events" list
        $(this).remove()
      return

  return

  $('#calendar').fullCalendar eventClick: (calEvent, jsEvent, view) ->
    alert 'Event: ' + calEvent.title
    alert 'Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY
    alert 'View: ' + view.name
    # change the border color just for fun
    $(this).css 'border-color', 'red'
    return



