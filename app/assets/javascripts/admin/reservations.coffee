# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  if($('#calendar_reservations').length > 0)
    date = new Date
    d = date.getDate()
    m = date.getMonth()
    y = date.getFullYear()
    $('#calendar_reservations').fullCalendar

      monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "December"],
      monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
      dayNamesShort: ["Dom", "Lun", "Mar", "Mier", "Juev", "Vier", "Sab"],
      buttonText:
        today: "Hoy",
        month: "Mes",
        week: "Semana",
        day: "Por día"

      header:
        left: 'title'
        right: 'prev,next today,month,agendaWeek,agendaDay'
        #right: 'prev,next,month'
      editable: true,
      lang: 'es',
      timeFormat: ' '

      eventSources: [{
        url: "/admin/buildings/#{$('#building_id').text()}/reservations/read_reservations"
      }],

      dayClick: (date, jsEvent, view) ->
        day = date.getDate()
        month = date.getMonth()
        year = date.getFullYear()
        hour = date.getHours()
        minutes = date.getMinutes()

        if ($(this).hasClass('fc-disabled') or $(this).hasClass('fc-past'))
          alert("No puedes cambiar el precio de días anteriores")
        else
          $.get "/admin/buildings/#{$('#building_id').text()}/reservations/new", day: day, month: month+1, year: year
    return




$(document).on 'change', '#reservation_admin_price', {}, (e) ->
  calculate_price()


$(document).on 'change', '#reservation_discount', {}, (e) ->
  calculate_price()


$(document).on 'click', 'a#change_view', {}, (e) ->
  e.preventDefault()
  if $(this).text() == 'Vista calendario'
    $(this).text('Vista tablas')
    $('#view_table').addClass('hidden')
    $('#view_calendar').removeClass('hidden')
    date = new Date
    d = date.getDate()
    m = date.getMonth()
    y = date.getFullYear()
    $('#calendar_reservations').fullCalendar 'render'


  else
    $(this).text('Vista calendario')
    $('#view_calendar').addClass('hidden')
    $('#view_table').removeClass('hidden')


#------------ ADD USERS TO RESERVATION ----------------------------#
$(document).on 'click', '#add-user-table tbody a.delete', {}, (e) ->
  table = $('#add-user-table').DataTable()
  table.row($(this).parents('tr')).remove().draw()



$(document).on 'change', 'select[name*=\'reservation[billings_services_attributes]\']', {}, (e) ->
  id_field = $(this).attr('id')
  if id_field.indexOf("service_id") > -1
    $.ajax(
      type: 'GET'
      url: "/admin/buildings/#{$('#building_id').text()}/services/#{$(this).val()}/read_datas"
      dataType: 'JSON').success (json) ->
      id_night = id_field.replace('service_id', 'night')
      id_price = id_field.replace('service_id', 'price')
      id_amount = id_field.replace('service_id', 'amount')
      id_tax = id_field.replace('service_id', 'tax')

      if json.unique_price == null || json.unique_price == ""
        $("##{id_night}").val($('#nights').text())
        $("##{id_price}").val(json.price)
        $("##{id_amount}").val(parseFloat(json.price * $("##{id_night}").val()).toFixed(2))
      else
        $("##{id_night}").val(1)
        $("##{id_price}").val(json.unique_price)
        $("##{id_amount}").val(parseFloat(json.unique_price * $("##{id_night}").val()).toFixed(2))
      $("##{id_tax}").val(json.tax)


# FORMULARIO EDIT RESERVA - CAMBIAR FECHAS
$(document).on 'dp.show', '.datetimerange .input-group', (e) ->
  $(this).attr 'dp.shown', 1

$(document).on 'ready', (e) ->
  if($('.datetimerange.edit_reservation').length > 0)
    read_apartments_available()
    loading_apartments()

jQuery ->
  $('.datetimerange.edit_reservation').each ->
    $this = $(this)
    range1 = $($this.find('.input-group')[0])
    range2 = $($this.find('.input-group')[1])

    range1.bind 'dp.change', (e) ->
      shown = $(this).attr('dp.shown')
      if(typeof shown != 'undefined' && shown && e.date != e.oldDate)
        loading_apartments()
        read_apartments_available()

    range2.bind 'dp.change', (e) ->
      shown = $(this).attr('dp.shown')
      if(typeof shown != 'undefined' && shown && e.date != e.oldDate)
        loading_apartments()
        read_apartments_available()


loading_apartments = ->
  $('.reservation_apartment_reservation .help-block').text('Cargando apartamentos para las fechas escogidas ...')
  $('.reservation_apartment_reservation .help-block').addClass('parpadea')


$(document).on 'change', '#reservation_apartment_reservation', {}, (e) ->
  change_apartment_reservation()
  $( "#advise" ).hide().html( "<div class='alert alert-warning' role='alert'><strong>Has seleccionado otro apartamento y por tanto los precios han cambiado.</strong> Asegúrate de revisarlos antes de guardar</div>" ).fadeIn('slow');

read_apartments_available =  ->
  start_date = $('#reservation_start_date').val()
  end_date = $('#reservation_end_date').val()
  apartment_id = $('#apartment_id').text()
  reservation_id = $('#reservation_id').text()
  #ver si las fechas están libres
  $.ajax(
    type: 'GET'
    url: "/admin/buildings/#{$('#building_id').text()}/reservations/search_apartmens"
    data: {start_date: start_date, end_date: end_date, apartment_id: apartment_id, reservation_id: reservation_id}
    dataType: 'JSON').success (json) ->

    $el = $('#reservation_apartment_reservation')
    $el.empty()
    # remove old options
    same_reservation = false
    $.each json, (value, key) ->
      if parseInt(key) == parseInt(apartment_id)
        same_reservation = true
      $el.append $('<option></option>').attr('value', key).text(value)
    if same_reservation == false
      $( "#advise_dates" ).hide().html( "<div class='alert alert-warning' role='alert'><strong>Has seleccionado otras fechas en la que el apartamento que pertenece esta reserva no se encuentra disponible.</strong> Asegúrate de seleccionar un apartamento de los mostrado justo abajo de este mensaje y comprobar los precios y datos de la reserva.</div>" ).fadeIn('slow')
    change_apartment_reservation()


change_apartment_reservation = ->
  $.ajax(
    type: 'GET'
    url: "/admin/buildings/#{$('#building_id').text()}/apartments/#{$('#reservation_apartment_reservation').val()}/read_price"
    data: {start_date : $('#reservation_start_date').val(), end_date: $('#reservation_end_date').val() }
    dataType: 'JSON').success (json) ->
    $('#reservation_normal_price').val(json)
    $('#normal_price_disabled').val(json)
    calculate_price()
    $('.reservation_apartment_reservation .help-block').html('')

calculate_price = ->
  condition = $("#reservation_reservation_condition").val()
  discount = $('#reservation_discount').val()
  amount_admin = $('#reservation_admin_price').val()
  amount_system = $('#reservation_normal_price').val()
  condition_percent = $('#reservation_reservation_condition').val()
  if amount_admin == ""
    if amount_system != ""
      if discount != ""
        total = (parseFloat(amount_system) + (amount_system * (condition_percent/100))).toFixed(2)
        $('#reservation_with_condition_price').val(total)
        total = (total - (total * (discount /100.0))).toFixed(2)
        $('.total_price').text(total+' €')
        $('#reservation_total_price').val(total)
        $('.normal_price').text(amount_system+' €')
      else
        total = (parseFloat(amount_system)+(amount_system * (condition_percent/100.0))).toFixed(2)
        $('#reservation_with_condition_price').val(total)
        $('.total_price').text(total+' €')
        $('#reservation_total_price').val(total)
        $('.normal_price').text(amount_system+' €')
    else
      $('.total_price').text('- €')
      $('#reservation_total_price').val('')
      $('.normal_price').text('- €')
  else
    if discount == ""
      total = (parseFloat(amount_admin) + (amount_admin * (condition_percent/100))).toFixed(2)
      $('#reservation_with_condition_price').val(total)
      $('.total_price').text(total+' €')
      $('#reservation_total_price').val(total)
      $('.normal_price').text(amount_admin+' €')
    else
      total = (parseFloat(amount_admin) + (amount_admin * (condition_percent/100))).toFixed(2)
      $('#reservation_with_condition_price').val(total)
      total = (total - (total * (discount /100.0))).toFixed(2)
      $('.total_price').text(total+' €')
      $('#reservation_total_price').val(total)
      $('.normal_price').text(amount_admin+' €')
$(document).on 'change', '#reservation_reservation_condition_id', {}, (e) ->
  $.ajax(
    type: 'GET'
    url: "/admin/buildings/#{$('#building_id').text()}/reservation_conditions/#{$(this).val()}/read_percent"
    dataType: 'JSON').success (json) ->
    condition_percent = json
    $("#reservation_reservation_condition").val(condition_percent)
    calculate_price()



# SEARCH CLIENT TO SET RENTER ON EDIT AND NEW RESERVATION
$(document).on 'click', '#submit-search-client', {}, (e) ->
  e.preventDefault();
  parentForm = $(this).closest('#form-search-clients')
  method = $(parentForm).attr('data-method')
  url = $(parentForm).attr('data-url')
  inputs = $(parentForm).find('input');
  data = {};

  inputs.each (index, input) ->
    data[ $(input).attr('name') ] = $(input).val()

  $.ajax(
    url: url,
    type: method,
    dataType: "script",
    data: data
  )









