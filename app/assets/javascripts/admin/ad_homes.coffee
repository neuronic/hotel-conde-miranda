$(document).ready ->
  options = onSwitchChange: (event, state) ->
    $.ajax(url:"/admin/buildings/#{$(this).attr('data_building')}/ad_homes/#{$(this).attr('id')}/change_status", type: 'PUT', dataType: "script")
  $('[name="ad-home-active"]').bootstrapSwitch options

  