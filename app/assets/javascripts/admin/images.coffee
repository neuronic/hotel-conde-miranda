$(document).ready ->
  if input_image = $('input[type="file"]')
    if input_image.data('width') != undefined
      _URL = window.URL or window.webkitURL

      $(document).on 'change', 'input[type="file"]', {}, (e) ->
        e.preventDefault()
        self = this
        file = $(this)[0].files[0]
        img = new Image
        imgwidth = 0
        imgheight = 0
        maxwidth = $(this).data('width')
        maxheight = $(this).data('height')
        img.src = _URL.createObjectURL(file)

        img.onload = ->
          imgwidth = @width
          imgheight = @height
          $('#width').text imgwidth
          $('#height').text imgheight
          if imgwidth == maxwidth and imgheight == maxheight
            $(self).parent().find('.help-block').html('')
            return true
          else
            $(self).val('')
            $(self).parent().parent().find('.image_errors').html('<span class="help-block">Tamaño inválido para la imagen, por favor cargue una imagen de ' + maxwidth + 'px de ancho y ' + maxheight + 'px de alto</span></div>')