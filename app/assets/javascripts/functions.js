$(document).ready(function(){
//función que lee los días inválidos para mostrarlos como tal en el calendario

  function disableInvalidDates(date) {
    var m = date.getMonth()+1;
    var d = date.getDate();
    var y = date.getFullYear();
    m = m<10? '0'+m:m;
    d = d<10? '0'+d:d;

    var dateCalendar = y + '-' + m + '-' + d;

    var result = calendarRestrictions.find(function(element) {
      return element.date === dateCalendar;
    });

    if(result === undefined)
      return [true]
    else
      return [!result.blocked]
  }
/* Inicialización en español para la extensión 'UI date picker' para jQuery. */
/* Traducido por Vester (xvester@gmail.com). */
  // RADIO BUTTON RESERVATION FORMS
    $('input[type=radio]').change(function() {
        if (this.value == 'user') {
            $('#form_user_reservation').removeClass('hidden')
            $('#form_business_reservation').addClass('hidden')
        }
        else if (this.value == 'business') {
            $('#form_business_reservation').removeClass('hidden')
            $('#form_user_reservation').addClass('hidden')
        }
    });


  $( "form.barra-busqueda" ).submit(function( event ) {
    var fechaInicio = [];
    var fechaFin = [];
    fechaInicio = $('#check-in').val();
    fechaFin = $("#check-out").val();
    if (fechaFin.length == 0 || fechaInicio.length == 0){
      $(".error-barra-reserva-no-blank").fadeIn().delay(2000).slideUp();
      event.preventDefault();
    }
  });


  //CALENDARIOS BUSQUEDA
  if($(".contenedor-barra-busqueda").length > 0 || $(".contenedor-interno-general .barra-busqueda").length > 0){
    var min_nights;
    var calendarRestrictions;

    $.ajax({
      type: 'GET',
      url: '/calendars/dates_conditions',
      dataType: 'json',
      success:function(data){
        calendarRestrictions = data.special_min_dates;
        min_nights = data.custom_min_dates.nights;
      }
    });

    $("#check-in").datepicker({dateFormat: "dd-mm-yy", minDate: "0", beforeShowDay: disableInvalidDates });
    $("#check-out").datepicker({dateFormat: "dd-mm-yy", minDate: "0", beforeShowDay: disableInvalidDates});


    $("#check-in").change(function(){
      fechaInicio = $(this).val().split("-");
      fechaInicioDate = new Date(fechaInicio[2], fechaInicio[1]-1, fechaInicio[0]);

      // restrictionMinNights = calendarRestrictions.find(x => x.date === $(this).val())
      var splitted = $(this).val().split('-')

      var year = parseInt(splitted[2])
      var month = parseInt(splitted[1])
      var day = parseInt(splitted[0])

      month = month < 10 ? '0' + month : month;
      day = day < 10 ? '0' + day : day;

      var dateChoosen =  year + '-' + month + '-' + day;

      restrictionMinNights = calendarRestrictions.find(function(element) {
        return element.date === dateChoosen;
      });

      if(restrictionMinNights === undefined || (restrictionMinNights !== undefined && restrictionMinNights.min_nights == null)){
        //if weekend? min_nights = 2 else min_nights is config of panel admin
        if(day_is_weekend(fechaInicioDate)){
          min_nights = 2
        }
      }else{ // has a configuration special
        min_nights = restrictionMinNights.min_nights
      }
      datepicker_change_end(fechaInicioDate);
    });

    $("#check-out").change(function(){
      fechaFin = $(this).val().split("-");
      fechaFinDate = new Date(fechaFin[2], fechaFin[1]-1, fechaFin[0]);
      datepicker_change_start(fechaFinDate);
    });


    function datepicker_change_end(date) {
      $('#check-out').datepicker('destroy');
      $( "#check-out" ).datepicker({
        inline: true,
        minDate: new Date(date.getTime() + (min_nights*24) * 60 * 60 * 1000),
        dateFormat: 'dd-mm-yy',
        beforeShowDay: disableInvalidDates
      });
    }

    function datepicker_change_start(date) {
      $('#check-in').datepicker('destroy');
      $( "#check-in" ).datepicker({
        inline: true,
        minDate: "0",
        maxDate: new Date(date.getTime() - (min_nights*24) * 60 * 60 * 1000),
        dateFormat: 'dd-mm-yy'
      });
    }

    function day_is_weekend(date){
      return (date.getDay() == 5 || date.getDay() == 6)
    }
  }

  if($(".boton-responsive-barra-busqueda").length > 0){
    var estadoBarraResponsive = 0;

    $(".boton-responsive-barra-busqueda").click(function(){
      if(estadoBarraResponsive == 0){
        $(".contenedor-barra-busqueda").slideDown();
        estadoBarraResponsive = 1;
      }else{
        $(".contenedor-barra-busqueda").slideUp();
        estadoBarraResponsive = 0;
      }
    });
  }

  //acordeon
  if($(".contenedor-acordeon").length > 0){
    var altoVentana = window.innerHeight - 165;

    $(".contenedor-acordeon,.contenedor-text-acordeon,.slice-acordeon").css("height",altoVentana+"px");

    window.onresize = function(){
      altoVentana = window.innerHeight - 165;
      $(".contenedor-acordeon,.contenedor-text-acordeon,.slice-acordeon").css("height",altoVentana+"px");
    }

    var arregloSlices = [];
    var estado = 0;

    $(".slice-acordeon").each(function(index){
      arregloSlices[index] = {indice : index , objeto : $(this), estado : 0};
    });

    var contadorInicial = 0;

    (function(){
      animar();
      function animar(){
        for(var i=0;i < arregloSlices.length;i++){
          if(i == contadorInicial){
            arregloSlices[i].objeto.animate({width : "40%" }, 4000, function() {
                text = $(this).find(".text-titulo-acordeon");
                text.fadeIn("slow");
              });

          }else{
            arregloSlices[i].objeto.animate({ width : "2.5%"}, 4000);
            text = arregloSlices[i].objeto.find(".text-titulo-acordeon");
            text.fadeOut("slow");
          }
        }
        contadorInicial = contador(contadorInicial);
        setTimeout(function(){animar();},10000);
      }
    })();
  }


  function contador(indice){
    if(indice < 4 ){
      indice++;
    }else{
      indice = 0;
    }

    return indice;
  }

  //NAVIGATION
  var estadoNav = 0;

  $("footer .boton-mobile").click(function(e){
    e.preventDefault();
    if(estadoNav != 1){
      $("footer nav").css("height", "0px")
      $("footer .menu-mobile").animate({height : 334}, 600);
      estadoNav = 1;
    }else{
      $("footer .menu-mobile").animate({height : 0},600);
      estadoNav = 0;
    }

  });

  $(".contenedor-navegacion .boton-mobile").click(function(e){
    e.preventDefault();
    if(estadoNav != 1){
      $(".contenedor-navegacion .menu-mobile").slideDown(600);
      estadoNav = 1;
    }else{
      $(".contenedor-navegacion .menu-mobile").slideUp(600);
      estadoNav = 0;
    }

  });


  if($(".contenedor-interno-contacto").length > 0){
    if(window.innerHeight > 640){
      $(".footer-interno").css({position : "fixed" , bottom : 0});
    }
  }

  if($(".services-page").length > 0){
    $(".footer-interno").css({position : "fixed" , bottom : 0});
  }

  if($(".promotions-page, .faqs_page").length > 0){
    $(".footer-interno").css({position : "fixed" , bottom : 0});
  }

  if($(".interno-general-localizacion").length > 0){
    $(".interno-general-localizacion").css("height",(window.innerHeight - 150) + "px");
  }

  if($(".interno-general-legal").length > 0){
    $(".footer-interno").css({position : "fixed" , bottom : 0});
  }

});


$(document).on('click', 'a#modal-advantage', function(){
  var offset = $('.caja-ventajas').offset();
  var height = $('.caja-ventajas').height();
  var width = $('.caja-ventajas').outerWidth();
  var top = offset.top-12 + height + "px";
  if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    top = offset.top+12 + height + "px";
  }
  $('.modal-dialog').css( {
      'position': 'absolute',
      'top': top,
      'left': '50%',
      'transform': 'translateX(-50%)',
      'width': width
  });
});

//GDPR
$(document).on('click', 'input.agree_policy_privacy', function(){
  if($(this).is(":checked")) {
    $('input[type="submit"]').attr('disabled', false)
  }else{
    $('input[type="submit"]').attr('disabled', true)
  }
});

$(document).on('click', 'input.agree_policy_privacy_user', function(){
  if($(this).is(":checked")) {
    $('input[type="submit"].user').attr('disabled', false)
  }else{
    $('input[type="submit"].user').attr('disabled', true)
  }
});

$(document).on('click', 'input.agree_policy_privacy_business', function(){
  if($(this).is(":checked")) {
    $('input[type="submit"].business').attr('disabled', false)
  }else{
    $('input[type="submit"].business').attr('disabled', true)
  }
});
