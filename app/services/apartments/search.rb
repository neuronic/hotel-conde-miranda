class Apartments::Search
  attr_accessor :errors, :apartments_available

  def initialize(building:, current_user:, start_date:, end_date:, exclude_inactives: true)
    @building          = building
    @current_user      = current_user
    @start_date        = start_date
    @end_date          = end_date
    @exclude_inactives = exclude_inactives

    @errors = {}
    @apartments_available = []

    validate
  end

  def call
    # Apartments with reservation in thoses dates.
    apartments_ids_unavailable = @building.reservations
                                          .actives
                                          .between_times(@start_date.beginning_of_day, @end_date.end_of_day - 1.days)
                                          .joins(:apartments)
                                          .pluck(:apartment_id)
    
    # Add apartments with disabled days config into admin area.
    apartments_ids_unavailable +=  ValidDate.where(validable_type: "Apartment")
                                            .between_times(@start_date.beginning_of_day, @end_date.end_of_day - 1.days)
                                            .pluck(:validable_id)
    
    # Add apartments wich are bloqued by process reservation
    apartments_ids_unavailable += apartments_blocked(@current_user, @building, @start_date, @end_date)
    
    # Add apartments witch rae inactive if exclude_inactives is activate in initializer.
    apartments_ids_unavailable += @building.apartments.inactive.pluck(:id) if @exclude_inactives
    
    # Apartments avaiable for dates and conditions
    @apartments_available = @building.apartments
                                     .includes(photos: :translations)
                                     .includes(:apartment_type)
                                     .no_deleted
                                     .where.not(id: apartments_ids_unavailable)
  end

  def valid?
    @errors.empty?
  end

  private

  def validate
    @errors[:start_date] = 'Not valid' if @start_date.nil?
    @errors[:end_date]   = 'Not valid' if @end_date.nil?
    @errors[:start_date] = 'Not valid' if !@start_date.is_a?(Date)
    @errors[:end_date]   = 'Not valid' if !@start_date.is_a?(Date)
    @errors[:start_date] = 'Not valid' if @errors.empty? && @start_date < Date.today

    @errors[:start_date] = 'Cannot be greather than end date' if @errors.empty? && @start_date > @end_date
  end

  def apartments_blocked(current_user, building, start_date, end_date)
    BlockApartmentReservation.where("updated_at > ?", BlockReservationService::TIME_RESERVATION_PER_STEP)
                             .where.not("userable_id = ? AND userable_type = ?", @current_user.id, @current_user.class.name)
                             .between_times(@start_date.beginning_of_day, @end_date.end_of_day-1.days)
                             .pluck(:apartment_id)
  end
end
