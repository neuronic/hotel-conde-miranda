class Building::Creator < ApplicationService
  attr_reader :attributes, :building, :success

  def initialize(attributes:)
    @attributes = attributes
  end

  def call
    build_building
    build_apartment_types
    build_cms_pages
    save

    return self
  end

  def success?
    success
  end

  private

  def build_building
    @building = Building.new(attributes)
  end

  def build_apartment_types
    @building.apartment_types.new([
      { name: "Tipo A" },
      { name: "Tipo B" },
      { name: "Tipo C" },
      { name: "Tipo D" },
    ])
  end

  def build_cms_pages
    @building.build_faq_page
    @building.build_page_location
  end

  def save
    if @building.save
      @success = true
    else
      @success = false
    end
  end
end