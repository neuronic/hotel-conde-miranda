class BlockReservationService

  TIME_RESERVATION_PER_STEP = 7.minutes.ago

  def initialize(current_user, start_date, end_date, apartment_id)
    @apartment_id = apartment_id
    @start_date = start_date
    @end_date = end_date
    @current_user = current_user
  end

  def blocked_dates_for_apartment?
    block_apartments_for_other_user = BlockApartmentReservation.where("apartment_id = ? AND updated_at > ?", @apartment_id, TIME_RESERVATION_PER_STEP)
      .where.not("userable_id = ? AND userable_type = ?", @current_user.id, @current_user.class.name)
      .between_times(@start_date.beginning_of_day, @end_date.end_of_day-1.days)
      .order(id: :desc)
    return !block_apartments_for_other_user.empty? ? true : false
  end

  def block_apartment_and_date!
    # Vemos si el tiene el apartamento bloqueado ya.
    blocked = @current_user.block_apartment_reservations.where("apartment_id = ?", @apartment_id)
      .where("DATE(start_date) = ? AND DATE(end_date) = ?", @start_date.to_date, @end_date.to_date).try(:first)
    if blocked
      blocked.update_attribute(:updated_at, DateTime.now)
    else
      @current_user.block_apartment_reservations.create(apartment_id: @apartment_id, start_date: @start_date, end_date: @end_date)
    end
  end

end