# class ApartmentsService
#   class << self
#     def apartment_free_for(current_user, building, start_date, end_date, public_zone = false)
#       apartments_ids_unavailable = building.reservations
#                                            .actives
#                                            .between_times(start_date.beginning_of_day, end_date.end_of_day - 1.days)
#                                            .joins(:apartments)
#                                            .pluck(:apartment_id)
#       apartments_ids_unavailable += apartments_blocked(current_user, building, start_date, end_date)
#       apartments_ids_unavailable += building.apartments.inactive.pluck(:id) if public_zone
#       apartments_available = building.apartments
#                                      .no_deleted
#                                      .where.not(id: apartments_ids_unavailable)
#                                      .includes(:translations)

#       apartments_available
#     end

#     private

#     def apartments_blocked(current_user, building, start_date, end_date)
#       BlockApartmentReservation.where("updated_at > ?", BlockReservationService::TIME_RESERVATION_PER_STEP)
#                                .where.not("userable_id = ? AND userable_type = ?", current_user.id, current_user.class.name)
#                                .between_times(start_date.beginning_of_day, end_date.end_of_day-1.days)
#                                .pluck(:apartment_id)
#     end
#   end
# end