class ApartmentService

  def initialize(apartment)
    @apartment = apartment
    @building = @apartment.building
  end

  def get_from_min_price
    min_price = @apartment.apartment_type.lowest_price
    return false unless min_price

    max_percentage_discount = @building.reservation_conditions
                                       .actives
                                       .minimum(:percent)

    min_price_on_future = if max_percentage_discount
      min_price = ((min_price*max_percentage_discount)/100 + min_price).round(2)
    else
      min_price
    end

    return min_price_on_future
  end

end