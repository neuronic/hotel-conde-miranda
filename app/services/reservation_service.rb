# Resevation service
class ReservationService
  def initialize(coupon, building, start_date, end_date, discount_nights = nil, discount_beforehand = nil)
    @coupon              = coupon
    @start_date          = start_date.to_date
    @end_date            = end_date.to_date
    @building            = building
    @discount_nights     = discount_nights
    @discount_beforehand = discount_beforehand
    @coupon_service      = CouponService.new(coupon, start_date, end_date)
  end

  # condition -> If not especified conditions, get alls of them actives.
  def get_prices_for_apartment_and_conditions(apartment, normal_price, condition = nil)
    conditions = condition ? [condition] : @building.reservation_conditions.includes(:translations).actives

    return get_discount_by_coupon(apartment, normal_price, conditions) if @coupon_service.coupon_is_on_time_and_conditions?
    return get_discount_by_nigths(normal_price, conditions)            if @discount_nights
    return get_discount_beforehand(normal_price, conditions)           if @discount_beforehand

    price_whithout_discount(normal_price, conditions)
  end

  def get_discount_by_coupon(apartment, normal_price, conditions)
    hash_discounts = []
    coupon_discount = @coupon_service.coupon_is_valid_for_apartment?(apartment) ? @coupon.discount : 0
    coupons_for_conditions = @coupon.reservation_conditions.pluck(:id)

    conditions.each do |condition|
      coupon_discount_for_condition = coupons_for_conditions.include?(condition.id) ? coupon_discount : 0

      normal_price_condition = ((normal_price * condition.percent) / 100 + normal_price).round(2)
      hash_discounts << {
        id: condition.id,
        title: condition.title,
        description: condition.description,
        default: condition.default,
        normal_price: normal_price_condition,
        discount: coupon_discount_for_condition,
        discount_price: calculate_discount(normal_price_condition, coupon_discount_for_condition)
      }
    end

    hash_discounts
  end

  def get_discount_beforehand(normal_price, conditions)
    hash_discounts = []
    discount = @discount_beforehand.try(:percentage) || 0

    conditions.each do |condition|
      normal_price_condition = ((normal_price * condition.percent) / 100 + normal_price).round(2)
      hash_discounts << {
        id: condition.id,
        title: condition.title,
        description: condition.description,
        default: condition.default,
        normal_price: normal_price_condition,
        discount: discount,
        discount_price: calculate_discount(normal_price_condition, discount)
      }
    end
    hash_discounts
  end

  def get_discount_by_nigths(normal_price, conditions)
    hash_discounts = []
    discount = @discount_nights.try(:percentage) || 0

    conditions.each do |condition|
      normal_price_condition = ((normal_price * condition.percent) / 100 + normal_price).round(2)
      hash_discounts << {
        id: condition.id,
        title: condition.title,
        description: condition.description,
        default: condition.default,
        normal_price: normal_price_condition,
        discount: discount,
        discount_price: calculate_discount(normal_price_condition, discount)
      }
    end
    hash_discounts
  end

  def price_whithout_discount(normal_price, conditions)
    hash_discounts = []
    discount = 0

    conditions.each do |condition|
      coupon_discount = 0
      normal_price_condition = ((normal_price * condition.percent) / 100 + normal_price).round(2)
      hash_discounts << {
        id: condition.id,
        title: condition.title,
        description: condition.description,
        default: condition.default,
        normal_price: normal_price_condition,
        discount: coupon_discount,
        discount_price: calculate_discount(normal_price_condition, discount)
        # (normal_price_condition - (normal_price_condition * discount) / 100).round(2)
      }
    end
    hash_discounts
  end

  private

  def calculate_discount(price, discount)
    (price - (price * discount) / 100).round(2)
  end
end
