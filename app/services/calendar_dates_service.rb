# Service to check if reservation can be created
# successfully.
class CalendarDatesService
  attr_accessor :date_records, :start_date, :end_date
  MIN_NIGHTS_WEEKEND = 2

  def initialize(start_date, end_date)
    @error = []
    @start_date = start_date
    @end_date = end_date
    @date_records = CalendarDate.select(:date, :blocked, :min_nights)
                                .between_times(
                                  start_date.beginning_of_day,
                                  end_date.end_of_day
                                )
  end

  def valid?
    end_date_greather_than_start_date? &&
      no_date_disabled? &&
      min_nights_condition?
  end

  def error
    @error.join(', ')
  end

  private

  # Comprobar que el dia de comienzo es menor que el fin.
  def end_date_greather_than_start_date?
    if @end_date <= @start_date
      @error << I18n.t('reservation.error.invalid_dates')
      return false
    end
    true
  end

  # Busca si hay algun dia disabled
  def no_date_disabled?
    record = @date_records.find { |date| date.blocked == true }
    if record.present?
      @error << I18n.t('reservation.error.invalid_dates')
      return false
    end
    true
  end

  # 1. Condicion especial del dia
  # 2. Fin de semana = 2 noches
  # 3. Minimo noches en el sistema.
  def min_nights_condition?
    min_night = if (date = @date_records.find { |record| record.date == @start_date })
                  date.min_nights
                elsif weekend?
                  MIN_NIGHTS_WEEKEND
                else
                  ConfigCalendar.select(:nights).first.try(:nights)
                end
    if (@end_date - @start_date).to_i < min_night
      @error << I18n.t('reservation.error.invalid_dates')
      return false
    end
    true
  end

  def weekend?
    @start_date.saturday? || @start_date.friday?
  end
end
