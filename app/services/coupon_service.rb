class CouponService

  def initialize(coupon, start_date, end_date)
    @coupon = coupon
    @start_date = start_date.to_date
    @end_date = end_date.to_date
    @error = []
  end

  def coupon_is_on_time_and_conditions?
    return false unless @coupon.present?
    is_valid_coupon_expiration? && is_valid_minimum_nights? &&
      is_valid_maximum_nights? && is_on_valid_range?
  end

  def coupon_is_valid_for_apartment?(apartment)
    return false unless @coupon.present?
    @coupon.apartments.include?(apartment) && coupon_is_on_time_and_conditions?
  end 

  def discount_for_service(service)
    return 0 unless @coupon.present? 
    return 0 unless @coupon.coupon_types.any?
    @coupon.coupon_types.where(service_id: service.id).try(:first).try(:discount) || 0
  end

  def get_errors_from_coupon
    return false unless @coupon.present?
    @error << "El cupón es válido hasta #{@coupon.deadline.strftime("%d/%m/%Y")}" if !is_valid_coupon_expiration?
    @error << "El cupón no está en el rango válido. Rangos válidos:  #{@coupon.valid_dates.each{|valid_date| valid_date}}" if !is_on_valid_range?
    @error << "El cupón debe es válido para mas de #{@coupon.minimum_nights}}" if !is_valid_minimum_nights?
    @error << "El cupón debe es válido para menos de #{@coupon.maximum_nights}}" if !is_valid_maximum_nights?
    return @error
  end


  private

  #### CUPONES
  def is_valid_coupon_expiration?
    return true unless @coupon.deadline.present?
    Date.today < @coupon.deadline
  end

  def is_valid_minimum_nights?
    return true unless @coupon.minimum_nights.present?
    nights = (@end_date - @start_date).to_i
    nights >= @coupon.minimum_nights
  end

  def is_valid_maximum_nights?
    return true unless @coupon.maximum_nights.present?
    nights = (@end_date - @start_date).to_i
    nights <= @coupon.maximum_nights
  end

  def is_on_valid_range?
    return true unless @coupon.valid_dates.any?
    @coupon.valid_dates.where('DATE(start_date) <= ? and DATE(start_date) <= ? and DATE(end_date) >= ? and DATE(end_date) >= ? ', @start_date, @end_date, @start_date, @end_date).count != 0
  end
  ###

end