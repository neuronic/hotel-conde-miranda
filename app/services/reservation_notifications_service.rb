class ReservationNotificationsService
  attr_accessor :email

  def initialize(reservation, email)
    @email = email
    @reservation = reservation
    @errors = []
  end

  def valid?
    @errors << 'Email obligatorio' if @email.blank?
    @errors << 'Reserva obligatoria' if @reservation.blank?
    @errors.empty?
  end

  def send_confirmation
    begin
      if valid?
        UserMailer.reservation_accepted(@reservation, @email).deliver_now
      else
        return false
      end
    rescue Exception => e
      @errors << e
      return false
    end
  end

  def errors
    @errors.join(', ')
  end
end
