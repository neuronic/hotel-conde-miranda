class ReservationsService
  class << self
    def duplicate(building, reservation, current_user)
      new_reservation = building.reservations.new(
        reservation.attributes.slice(
          'start_date', 'end_date', 'comment', 'channel_id',
          'booking_reference', 'reservation_condition_id'
        )
      )
      # Busqueda primer apartamento libre que haya
      # apartment = ApartmentsService.apartment_free_for(
      #   current_user,
      #   building,
      #   reservation.start_date,
      #   reservation.end_date
      # ).first

      apartments_available = Apartments::Search.new(
        current_user: current_user,
        building: building,
        start_date: reservation.start_date,
        end_date: reservation.end_date,
        exclude_inactives: false
      ).call

      apartment = apartments_available.first

      raise 'No se puede duplicar esta reserva. No hay apartamentos disponibles para estas fechas' if apartment.nil?

      new_reservation.apartments << apartment
      block_reservation_apartment = BlockReservationService.new(
        current_user,
        new_reservation.start_date,
        new_reservation.end_date,
        apartment.id
      )

      block_reservation_apartment.block_apartment_and_date!

      new_reservation.clients_reservations = reservation.clients_reservations.dup if reservation.clients_reservations
      new_reservation.billings_services    = reservation.billings_services.dup if reservation.billings_services
      new_reservation.billing_address      = reservation.billing_address.dup if reservation.billing_address
      new_reservation.createable           = current_user

      new_reservation.normal_price = apartment.get_price(
        new_reservation.start_date.to_date,
        new_reservation.end_date.to_date
      )

      new_reservation.total_price  = new_reservation.normal_price
      new_reservation.rest         = new_reservation.total_price
      new_reservation.save

      creadit_card_datas = reservation.attributes.slice(
        'credit_card_number', 'credit_card_date', 'credit_card_holder'
      )

      new_reservation.assign_attributes(creadit_card_datas)

      # agregar a billing de la reserva
      new_reservation.billings.create(
        concept: 'apartment',
        amount: new_reservation.total_price,
        night: new_reservation.nights,
        tax: Reservation::IVA,
        price: (new_reservation.total_price / new_reservation.nights).round(2)
      )

      # Calculo precio restante
      new_reservation.calculate_rest_money
      new_reservation
    end
  end
end
