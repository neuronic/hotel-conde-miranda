class Promotion < ActiveRecord::Base
  ##### RELATIONSHIPS
  belongs_to :building

  mount_uploader :image, ImageUploader

  translates :text, :title
  globalize_accessors locales: [:en, :es, :pt], attributes: [:title, :text]

  validates :image, presence: true
  validates :title_es, :title_en, presence: true
  validates :text_es, :text_en, presence: true

  scope :actives, -> {where(active: true)}
end