# == Schema Information
#
# Table name: equipment
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  key        :string(255)
#

class Equipment < ActiveRecord::Base
	has_and_belongs_to_many :apartments
end
