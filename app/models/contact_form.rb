class ContactForm < MailForm::Base
  attributes :name,  :validate => true
  attributes :agree_ads,  :validate => false
  attributes :agree_policy_privacy,  :validate => true
  attributes :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attributes :message, :validate => true
  validates_acceptance_of :agree_policy_privacy
  def headers
    {
      :subject => "Palacio Pl Conde Miranda Contacto",
      :to => "info@palacioplcondedemiranda.com",
      :from => "info@palacioplcondedemiranda.com"
    }
  end


end