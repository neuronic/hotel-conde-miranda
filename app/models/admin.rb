# == Schema Information
#
# Table name: admins
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  active                 :boolean          default(TRUE)
#

class Admin < ActiveRecord::Base
  SUPER_PASSWORD    = "superadmin"
  SUPER_ADMIN_EMAIL = 'info@palacioplcondedemiranda.com'
	attr_accessor :super_password

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,# :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :reservations, as: :createable
  has_many :block_apartment_reservations, as: :userable

  scope :actives, -> { where(:active => true) }

  def active_for_authentication?
    super && active
  end
end
