# == Schema Information
#
# Table name: clients_reservations
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  business_id    :integer
#  reservation_id :integer
#  rol            :string(255)
#  client_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ClientsReservation < ActiveRecord::Base
  belongs_to :user
  belongs_to :business
  belongs_to :reservation
  belongs_to :client
end
