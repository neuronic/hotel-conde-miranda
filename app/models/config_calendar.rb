# == Schema Information
#
# Table name: config_calendars
#
#  id          :integer          not null, primary key
#  nights      :integer
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ConfigCalendar < ActiveRecord::Base
  belongs_to :building
  def self.valid_minimum_nights?(start_date, end_date)
    if min_night = self.first
      (end_date - start_date).to_i < min_night.nights
    else
      true
    end
  end
end
