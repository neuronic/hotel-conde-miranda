# == Schema Information
#
# Table name: guests
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Guest < ActiveRecord::Base
	has_many :block_apartment_reservations, as: :userable
end
