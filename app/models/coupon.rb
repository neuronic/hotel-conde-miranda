# == Schema Information
#
# Table name: coupons
#
#  id             :integer          not null, primary key
#  code           :string(255)
#  description    :text(65535)
#  status         :boolean
#  discount       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  building_id    :integer
#  deadline       :date
#  minimum_nights :integer
#  maximum_nights :integer
#

class Coupon < ActiveRecord::Base
	belongs_to :building
  has_many :valid_dates, as: :validable
  has_and_belongs_to_many :apartments
  has_and_belongs_to_many :reservation_conditions
  has_many :reservations
  has_many :coupon_types, dependent: :destroy

  accepts_nested_attributes_for :valid_dates, allow_destroy: true
  accepts_nested_attributes_for :coupon_types, allow_destroy: true

	def self.generate_code
    code = loop do
      random_code = (0...7).map { [*'0'..'9',*'A'..'Z'].to_a[rand(26)] }.join
      break random_code unless self.exists?(code: random_code)
    end
  end

  validates :code, presence: true
  validates :deadline, presence: true
  validates :discount, presence: true

  def is_valid_for_dates?(start_date, end_date)
    start_date = start_date.to_date
    end_date = end_date.to_date
    if self.is_on_date?
      if self.num_nights_valid?(start_date, end_date)
        if self.is_on_valid_range?(start_date, end_date)
          return true
        else
          'coupon_no_valid_for_dates'
        end
      else  
        'coupon_invalid_num_nights'
      end
    else
      'coupon_invalid_date_expire'
    end

  end

  def is_valid_for_apartment?(apartment)
    self.apartments.include?(apartment)
  end

  def is_on_valid_range?(start_date, end_date)
    return true if !self.valid_dates.any?
    self.valid_dates.where('DATE(start_date) <= ? and DATE(start_date) <= ? and DATE(end_date) >= ? and DATE(end_date) >= ? ', start_date, end_date, start_date, end_date).count != 0
  end

  def is_on_date?
    Date.today <= self.deadline
  end

  def num_nights_valid?(start_date, end_date)
    nights = (end_date - start_date).to_i
    if self.minimum_nights.present? && self.maximum_nights.present?
      return (nights >= self.minimum_nights && nights <= self.maximum_nights)
    else
      return true
    end
  end

end
