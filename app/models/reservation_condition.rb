# == Schema Information
#
# Table name: reservation_conditions
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  description     :text(65535)
#  annotation      :text(65535)
#  percent         :float(24)
#  default         :boolean
#  building_id     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#  payment_percent :float(24)        default(50.0)
#

# Class represent conditions can have a reservation
class ReservationCondition < ActiveRecord::Base
  acts_as_paranoid
  translates :title, :description
  globalize_accessors :locales => [:en, :es, :pt], :attributes => [:description, :title]

  belongs_to :building
  has_many :reservations

  validates :title_es, :title_en, :title_pt, presence: true
  validates :percent, presence: true
  validates :payment_percent, presence: true

  scope :actives, -> { where(active: nil) }

  def self.default
    find_by(default: true)
  end

  def has_payment_condition?
    !payment_percent.zero?
  end

  translation_class.class_eval do
    def destroy
      run_callbacks :destroy
      true
    end
  end
end
