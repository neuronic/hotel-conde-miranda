# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  reservation_id :integer
#  number         :string(255)
#  date           :datetime
#  customer_id    :integer
#  customer_type  :string(255)
#  notes          :text(65535)
#  total          :float(24)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  attachment     :string(255)
#

class Invoice < ActiveRecord::Base
  before_destroy :delete_relationship
  attr_accessor :billing_address, :customer_invoice
  by_star_field :date
  mount_uploader :attachment, AttachmentUploader

  belongs_to :reservation
  belongs_to :customer, polymorphic: true
  belongs_to :billing_address
  has_many :billings, -> {where invoice_id: nil}, :through => :reservation
  has_many :billings_in, :class_name => 'Billing'
  has_many :invoice_items, :dependent => :destroy
  has_one :rectification_invoice, :dependent => :destroy

  scope :with_no_rectification, -> { includes(:rectification_invoice).where( :rectification_invoices => { :id => nil } )}

  accepts_nested_attributes_for :billings

  def total_no_taxes
    self.invoice_items.present? ? self.invoice_items.sum(:total_price_base).round(2) : 0
  end

  def taxes
    self.invoice_items.present? ? self.invoice_items.sum(:total_vat).round(2) : 0
  end

  def total
    self.invoice_items.present? ? self.invoice_items.sum(:total_price).round(2) : 0
  end

  def self.get_next_number_invoice(building)
    prefix         = building.invoice_address.prefix_invoice_number
    number_invoice = building.invoices.any? ? building.invoices.last.number.split('-') : ["#{prefix}000", '16']
    number         = number_invoice[0].scan(/\d+/).first
    year           = Date.today.year.to_s.last(2)

    num =
      if year != number_invoice[1]
        "#{prefix}1-#{year}"
      else
        "#{prefix}#{number.to_i + 1}-#{year}"
      end

    num
  end

  def get_customer_name
    if self.customer.class.name == "User"
      name = customer.name
      cif = customer.nif.presence || customer.passport.presence
    else
      name = customer.business_name
      cif = customer.cif
    end
    "#{name} - #{cif}"
  end

  def get_customer_datas
    if self.customer.class.name == "User"
      name_customer = [customer.name, customer.try(:surname)].join(" ")
      cif = customer.nif.presence || customer.passport.presence
    else
      name_customer = customer.business_name
      cif = customer.cif
    end

    return name_customer, cif
  end

  def get_payment_types
    self.reservation.payment_summaries.pluck(:method).try(:join, ',')
  end

  def has_rectification?
    false
  end

  def delete_relationship
    self.billings_in.each do |b|
      b.update_attribute(:invoice_id, nil)
    end
  end

  def self.destroy_all_invoices
    Billing.all.each do |b|
      if b.invoice_id.present?
        b.invoice = nil
        b.save!
      end
    end

    self.all.each do |inv|
      if inv.billings_in.present?
        inv.billings_in.delete_all
      end
      inv.destroy
    end
  end
end
