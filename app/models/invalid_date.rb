# == Schema Information
#
# Table name: invalid_dates
#
#  id          :integer          not null, primary key
#  date        :date
#  description :text(65535)
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class InvalidDate < ActiveRecord::Base
  belongs_to :building

  validates :date, presence: true

  def self.invalid_date?(day)
    InvalidDate.find_by_date(day)
  end
end
