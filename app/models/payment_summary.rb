# == Schema Information
#
# Table name: payment_summaries
#
#  id             :integer          not null, primary key
#  amount         :float(24)
#  kind           :string(255)
#  reservation_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  method         :string(255)
#  transaction_id :integer
#

class PaymentSummary < ActiveRecord::Base
	METHODS = ["visa", "paypal", "transfer", "cash", "mastercard", "american_express"]
	TYPES = ["deposit", "payment", "payment_tpv", "payment_paypal"]


	belongs_to :reservation
	belongs_to :transaction_payment, class_name: "Transaction", foreign_key: "transaction_id"

	validates :amount, presence: true
	validates :kind, presence: true
	validates :method, presence: true

	def translation_method
		case self.method
		when "cash"
			"Efectivo/cash"
		when "american_express"
			"American Express"
		when "transfer"
			"Transferencia bancaria/transfer"
		else
			self.method
		end
	end
end
