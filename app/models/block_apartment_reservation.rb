# == Schema Information
#
# Table name: block_apartment_reservations
#
#  id            :integer          not null, primary key
#  userable_id   :integer
#  userable_type :string(255)
#  apartment_id  :integer
#  start_date    :date
#  end_date      :date
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class BlockApartmentReservation < ActiveRecord::Base
  belongs_to :userable, polymorphic: true
  belongs_to :apartment

  by_star_field :start_date, :end_date
end
