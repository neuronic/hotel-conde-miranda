# == Schema Information
#
# Table name: sliders
#
#  id          :integer          not null, primary key
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Slider < ActiveRecord::Base
  belongs_to :building
  has_many :photos, :dependent => :destroy, :as => :photographable
  accepts_nested_attributes_for :photos, allow_destroy: true
end
