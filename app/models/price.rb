# == Schema Information
#
# Table name: prices
#
#  id                :integer          not null, primary key
#  night             :date
#  price             :decimal(8, 2)
#  apartment_type_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Price < ActiveRecord::Base
	attr_accessor :from, :to
  belongs_to :apartment_type
  by_star_field :night
  #id, night, apartment_type_id, created_at, updated_at

  def as_json(options = {})
    {
      :id => self.id,
      :title => "#{self.price} €",
      :start => self.night,
      :end => self.night,
      :allDay => false,
      :recurring => false,
      :color => "rgb(164, 183, 233)"
    }
  end
end
