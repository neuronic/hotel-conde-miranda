# == Schema Information
#
# Table name: reservations
#
#  id                       :integer          not null, primary key
#  building_id              :integer
#  reference                :string(255)
#  start_date               :date
#  end_date                 :date
#  normal_price             :float(24)
#  total_price              :float(24)
#  status                   :string(255)
#  createable_id            :integer
#  createable_type          :string(255)
#  comment                  :text(65535)
#  accepted_conditions      :boolean
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  channel_id               :integer
#  discount                 :integer
#  admin_price              :float(24)
#  rest                     :float(24)
#  booking_reference        :string(255)
#  process                  :string(255)
#  coupon_id                :integer
#  discount_id              :integer
#  reservation_condition_id :integer
#

class Reservation < ActiveRecord::Base
  include AASM
  ransacker :created_at, type: :date do
    Arel.sql('date(created_at)')
  end

  IVA = 10
  DESCRIPTION = "Alquiler apartamento"
  attr_accessor :type, :user, :business, :apartment_id, :services,
                :deposit, :apartment_reservation

  before_save      EncryptionWrapper.new([:credit_card_number, :credit_card_date, :credit_card_holder])
  after_save       EncryptionWrapper.new([:credit_card_number, :credit_card_date, :credit_card_holder])
  after_initialize EncryptionWrapper.new([:credit_card_number, :credit_card_date, :credit_card_holder])

  by_star_field :start_date, :end_date
  after_create :generate_reference_and_process
  before_destroy :destroy_transactions

  belongs_to :building
  has_and_belongs_to_many :apartments, dependent: :destroy
  belongs_to :channel
  belongs_to :coupon
  belongs_to :createable, polymorphic: true
  belongs_to :reservation_condition, -> { with_deleted }

  has_many :discounts_reservations
  has_many :discount_systems, :through => :discounts_reservations, source: :discount
  has_many :billings, dependent: :destroy
  has_many :billings_services, -> {where(:concept => "service")}, class_name: 'Billing'
  has_many :payment_summaries, dependent: :destroy
  has_many :invoices, dependent: :destroy
  has_many :rectification_invoices, :through => :invoices
  has_many :clients_reservations, dependent: :destroy
  has_many :users, through: :clients_reservations
  has_many :businesses, through: :clients_reservations
  has_many :clients, through: :clients_reservations

  has_one :billing_address, dependent: :destroy
  has_one :transaction_payment, class_name: 'Transaction', dependent: :destroy
  has_one :transaction_tpv, dependent: :destroy
  has_one :transaction_paypal, dependent: :destroy

  accepts_nested_attributes_for :payment_summaries, allow_destroy: true
  accepts_nested_attributes_for :billing_address
  accepts_nested_attributes_for :billings_services, allow_destroy: true
  accepts_nested_attributes_for :billings

  scope :canceled, -> {where(status: "canceled")}
  scope :finished, -> {where(process: "finished")}
  scope :actives, -> {where.not("status = 'canceled' or process = 'created'")}
  scope :unfinished, -> {where("process = 'created' and DATE(created_at) < ?", Date.today - 1.weeks)}
  scope :created, -> {where(process: 'created')}
  scope :past, -> {where("DATE(end_date) < ?", Date.today)}
  scope :current, -> {where.not("DATE(end_date) < ?", Date.today)}
  scope :nexts, -> {where("DATE(end_date) >= ?", Date.today)}
  scope :start_today, -> {where("DATE(start_date) = ?", Date.today)}
  scope :end_today, -> {where("DATE(end_date) = ?", Date.today)}
  scope :reserved_today, -> {where("DATE(created_at) = ?", Date.today)}

  def as_json(options = {})
    r = self.get_renter
    renter = if r.nil?
      "Sin titular"
    elsif r.is_user?
      r.name
    else
      r.business_name
    end

    title =
      if options[:individual].present?
        "#{self.reference}, #{self.get_renter}"
      else
        "#{self.reference}, #{self.apartments.map(&:name).join(", ")}, #{renter}"
      end

    {
      :id => self.id,
      :title => title,
      :start => (self.start_date.to_time + 12.hours),
      :end => (self.end_date.to_time + 12.hours),
      :allDay => false,
      :recurring => false,
      :color => "rgb(164, 183, 233)"
    }
  end

  aasm :column => 'status' do
    state :no_paid, :initial => true
    state :deposit
    state :paid
    state :canceled

    event :paid_50 do
      transitions :from => [:no_paid, :paid], :to => :deposit
    end

    event :paid do
      transitions :from => [:deposit, :no_paid], :to => :paid
    end

    event :cancel do
      transitions :from => [:deposit, :no_paid, :paid], :to => :canceled
    end

    event :no_paid do
      transitions :from => [:deposit, :paid], :to => :no_paid
    end
  end

  def finished!
    self.update_attribute(:process, 'finished')
  end

  def finished?
    self.process == 'fisnished'
  end

  def self.today
    Reservation.actives.all.between_times(Date.today, Date.today+1)
  end

  def generate_reference_and_process
    reference = loop do
      ref = (0...7).map { [*'0'..'9',*'A'..'Z'].to_a[rand(26)] }.join
      break ref unless Reservation.exists?(reference: ref)
    end
    self.update_attributes(:reference => reference, :process => 'created')
  end

  def self.to_csv(options = {})
    column_names = [:reference, :start_date, :end_date, :normal_price, :discount, :total_price, :status, :admin_price, :rest, :comment]
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |reser|
        csv << reser.attributes.values_at(*column_names)
      end
    end
  end

  def is_renter?(client)
    if client.is_user?
      u = self.users.where('clients_reservations.rol = "renter"').first
      u == client
    else
      u = self.businesses.where('clients_reservations.rol = "renter"').first
      u == client
    end
  end

  def change_rol_renter(client)
    #buscamos el antiguo rol renter
    old_renter = self.clients_reservations.where(:rol => "renter").first
    if !old_renter.nil?
      if old_renter.user.present?
        old_renter.update_attribute(:rol, "companion")
      else
        old_renter.destroy
      end
    end
    #crear el renter o acutalizarlo
    if client.reservations.include?(self)
      client.clients_reservations.find_by_reservation_id(self.id).update_attribute(:rol, "renter")
    else
     client.clients_reservations.create(reservation_id: self.id, rol: "renter")
    end

  end

  def get_renter
    if cr = self.clients_reservations.find_by_rol('renter')
      if cr.user.present?
        cr.user
      else
        cr.business
      end
    else
      nil
    end
  end

  def get_renter_name
    if cr = self.clients_reservations.find_by_rol('renter')
      if cr.user.present?
        cr.user.name
      else
        cr.business.business_name
      end
    else
      "No hay titular"
    end
  end

  def calculate_rest_money
    billings = self.billings.sum(:amount)
    rest =  billings - self.payment_summaries.sum(:amount)
    self.update_attribute(:rest, rest )
    if self.rest <= 0
      self.paid! if self.aasm.current_state != :paid
    elsif self.payment_summaries.find_by_kind('deposit')
      self.paid_50! if self.aasm.current_state !=  :deposit
    elsif rest == billings
      self.no_paid! if self.aasm.current_state != :no_paid
    end
  end

  def total
    self.billings.sum(:amount)
  end

  def paid
    self.payment_summaries.sum(:amount)
  end

  def calculate_deposit
    self.total * (self.reservation_condition.payment_percent/100)
  end

  def nights
    (self.end_date - self.start_date).to_i
  end

  def days_before
    (self.start_date - Date.today).to_i
  end

  def description
    apartment = self.apartments.first
    description = apartment.apartment_description
    self.billings.services.each do |b|
      description += " + " + b.service.name
    end
    description += " - " + self.nights.to_s + "N."
  end

  def has_tpv_payment?
    self.payment_summaries.where(:kind => PaymentSummary::TYPES[2]).count != 0
  end

  def has_payment_transaction?
    (self.transaction_paypal.present? or self.transaction_tpv.present?) and ((self.transaction_paypal.present? and self.transaction_paypal.payer_id.present?) or (self.transaction_tpv.present? and self.transaction_tpv.Ds_Response == '0000'))
  end

  def can_create_invoice?
    can = true
    self.invoices.each do |inv|
      can = false if !inv.has_rectification?
    end
    can
  end

  def destroy_transactions
    if self.transaction_tpv.present?
      self.transaction_tpv = nil
    end
    if self.transaction_paypal.present?
      self.transaction_paypal = nil
    end
  end
end
