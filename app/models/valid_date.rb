# == Schema Information
#
# Table name: valid_dates
#
#  id             :integer          not null, primary key
#  start_date     :date
#  end_date       :date
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  validable_id   :integer
#  validable_type :string(255)
#

class ValidDate < ActiveRecord::Base
	belongs_to :validable, polymorphic: true

	validates :end_date, presence: true, date: true
	validates :start_date, presence: true, date: true, date: {before: :end_date}

	by_star_field :start_date, :end_date
end
