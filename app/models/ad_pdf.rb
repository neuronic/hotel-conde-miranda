# == Schema Information
#
# Table name: ad_pdfs
#
#  id          :integer          not null, primary key
#  image       :string(255)
#  name        :string(255)
#  active      :boolean          default(FALSE)
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AdPdf < ActiveRecord::Base
  belongs_to :building

  validates :name, presence: true
  validates :image, presence: true

  mount_uploader :image, PictureUploader

  scope :actives, -> {where(:active => true)}

  def active?
    self.active
  end
end
