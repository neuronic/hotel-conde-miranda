class Cms::FaqPage < Page
  has_many :questions, class_name: 'Cms::Question', foreign_key: 'page_id'
  accepts_nested_attributes_for :questions, allow_destroy: true
end
