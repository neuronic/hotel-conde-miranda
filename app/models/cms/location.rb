# == Schema Information
#
# Table name: pages
#
#  id              :integer          not null, primary key
#  building_id     :integer
#  content         :text(65535)
#  longitude       :float(24)
#  latitude        :float(24)
#  address         :text(65535)
#  metatitle       :string(255)
#  metadescription :string(255)
#  metakey         :string(255)
#  type            :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Cms::Location < Page
  before_create :set_default_location

  private
  def set_default_location
    self.latitude = 40.416696
    self.longitude = -3.703776
  end
end
