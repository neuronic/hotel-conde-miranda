class Cms::Question < ActiveRecord::Base
  self.table_name = :questions

  IMAGE_DIMENSION = { width: 400, height: 150 }

  translates :question, :response
  globalize_accessors locales: [:en, :es, :pt], attributes: [:question, :response]

  mount_uploader :image, ImageUploader

  belongs_to :page
  validates :image, presence: true
  validates :question_es, :question_en, :response_es, :response_en, presence: true
end
