# == Schema Information
#
# Table name: buildings
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  address            :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  invoice_address_id :integer
#
class Building < ActiveRecord::Base
  MAIL_CONFIG = { conde: 1, arralde: 2 }.freeze

  translates :cookies_information, :privacy_policy_information, :legal_information
  globalize_accessors locales: [:en, :es, :pt],
    attributes: [:cookies_information, :privacy_policy_information, :legal_information]

  has_many :apartment_types, dependent: :destroy
  has_many :prices, through: :apartment_types
  has_many :apartments, dependent: :destroy
  has_many :coupons, dependent: :destroy
  has_many :calendar_dates
  has_many :discounts, dependent: :destroy
  has_many :reservations, dependent: :destroy
  has_and_belongs_to_many :channels
  has_many :services, dependent: :destroy
  belongs_to :invoice_address
  has_many :invoices, through: :reservations
  has_many :rectification_invoices, through: :invoices
  has_many :invalid_dates, dependent: :destroy
  has_many :ad_pdfs, dependent: :destroy
  has_many :ad_homes, dependent: :destroy
  has_many :config_calendars, dependent: :destroy
  has_one  :slider, dependent: :destroy
  has_one  :page_location, class_name: 'Cms::Location', dependent: :destroy
  has_one  :faq_page, class_name: 'Cms::FaqPage', dependent: :destroy
  has_many :reservation_conditions, dependent: :destroy
  has_many :clients, dependent: :destroy
  has_many :users
  has_many :businesses
  has_many :promotions, dependent: :destroy

  accepts_nested_attributes_for :invoice_address

  validates :name, presence: true

  def load_mail_config
    MAIL_CONFIG.find { |_key, value| value == mail_config }[0]
  end
end
