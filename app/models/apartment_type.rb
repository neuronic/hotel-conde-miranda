# == Schema Information
#
# Table name: apartment_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ApartmentType < ActiveRecord::Base

	belongs_to :building
	has_many :apartments
	has_many :prices, dependent: :destroy

	def apartment_type_select
		"#{self.name}"
	end

	def lowest_price
		min_price = self.prices.after(Time.now, field: :night).order(price: :asc).first
		min_price.try(:price)
	end

end
