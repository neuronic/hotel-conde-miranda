require 'openssl'
require 'base64'

class EncryptionWrapper

  PUBLIC_KEY_FILE = "#{Rails.root}/config/public.pem"
  PRIVATE_KEY_FILE = "#{Rails.root}/config/private.pem"
  PASSWORD = Rails.application.secrets.key_pem

  def initialize(attributes = [])
    @attributes = attributes
  end

  def before_save(record)
    @attributes.each do |attribute|
      record.send("#{attribute}=", encrypt(record.send("#{attribute}")))
    end
  end

  def after_save(record)
    @attributes.each do |attribute|
      record.send("#{attribute}=", decrypt(record.send("#{attribute}")))
    end
  end

  alias_method :after_initialize, :after_save

  private
  def encrypt(attribute)
    unless attribute.nil?
      public_key = OpenSSL::PKey::RSA.new(File.read(PUBLIC_KEY_FILE))
      encrypted_string = Base64.encode64(public_key.public_encrypt(attribute))
    end
  end

  def decrypt(attribute)
    unless attribute.nil?
      private_key = OpenSSL::PKey::RSA.new(File.read(PRIVATE_KEY_FILE), PASSWORD)
      string = private_key.private_decrypt(Base64.decode64(attribute)).force_encoding(Encoding::UTF_8)
    end
  end
end
