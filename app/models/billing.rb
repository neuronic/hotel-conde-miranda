# == Schema Information
#
# Table name: billings
#
#  id             :integer          not null, primary key
#  reservation_id :integer
#  concept        :string(255)
#  amount         :float(24)
#  night          :integer
#  service_id     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  tax            :integer
#  price          :float(24)
#  description    :string(255)
#  invoice_id     :integer
#

class Billing < ActiveRecord::Base
	CONCEPTS = ["apartment", "service"]
  belongs_to :reservation
  belongs_to :service
  belongs_to :invoice

  validates :night, presence: true
  validates :service, presence: true, :if => :no_apartment?
  validates :price, presence: true
  validates :tax, presence: true
  validates :amount, presence: true

  scope :services, -> {where(:concept => "service")}
  scope :apartments, -> {where(:concept => "apartment")}
  scope :no_invoice, -> {where(:invoice_id => nil)}
  scope :invoice, -> {where.not(:invoice_id => nil)}
  # before_create :set_datas_services_create
  # before_save :set_datas_services

	def is_service?
		self.concept == "service"
	end

	private

	def no_apartment?
		self.concept != "apartment"
	end
	# def set_datas_services
	# 	if self.is_service?
	# 		self.tax = self.service.tax
	# 		# self.night = 1 if self.service.unique_price?
	# 		self.price = self.service.unique_price? ? self.service.unique_price  : self.service.price
	# 		self.amount = self.price * self.night
	# 	end
	# end

	# def set_datas_services_create
	# 	self.description = Service::DESCRIPTION[0] if self.is_service?
	# end

	def self.to_csv(options = {})
    column_names = [:concept, :tax, :amount]
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |inv|
        csv << inv.attributes.values_at(*column_names)
      end
    end
  end

end
