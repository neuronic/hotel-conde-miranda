# == Schema Information
#
# Table name: photos
#
#  id                  :integer          not null, primary key
#  photographable_id   :integer
#  photographable_type :string(255)
#  caption             :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  picture             :string(255)
#  accordion_pic       :boolean          default(FALSE)
#

class Photo < ActiveRecord::Base
	mount_uploader :picture, PictureUploader
	belongs_to :photographable, :polymorphic => true

	translates :caption
  globalize_accessors :locales => [:en, :es, :pt], :attributes => [:caption]

  validates :caption_es, presence: true
  validates :picture, presence: true
  scope :no_accordion, -> {where(accordion_pic: false)}
  scope :accordion, -> {where(accordion_pic: true)}
end
