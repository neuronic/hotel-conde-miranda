# == Schema Information
#
# Table name: clients
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  surname                  :string(255)
#  nif                      :string(255)
#  email                    :string(255)
#  phone                    :string(255)
#  cif                      :string(255)
#  address                  :string(255)
#  city                     :string(255)
#  country                  :string(255)
#  business_name            :string(255)
#  type                     :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  juvenile                 :boolean          default(FALSE)
#  agree_ads                :boolean          default(FALSE)
#  date_birth               :date
#  passport                 :string(255)
#  nationality              :string(255)
#  gender                   :string(255)
#  expedition_date_passport :date
#  expedition_date_nif      :date
#  building_id              :integer
#

class Business < Client
	has_many :clients_reservations
	has_many :reservations, through: :clients_reservations
	has_many :invoices, as: :customer

	def datas_invoice
		"#{business_name} " + (cif.present? ? ", CIF: #{cif}" : ", sin CIF")
	end

end
