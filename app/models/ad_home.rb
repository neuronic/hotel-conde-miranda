# == Schema Information
#
# Table name: ads
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  image       :string(255)
#  type        :string(255)
#  active      :boolean
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AdHome < Ad
  scope :actives, -> {where(active: true)}
end
