# == Schema Information
#
# Table name: pages
#
#  id              :integer          not null, primary key
#  building_id     :integer
#  content         :text(65535)
#  longitude       :float(24)
#  latitude        :float(24)
#  address         :text(65535)
#  metatitle       :string(255)
#  metadescription :string(255)
#  metakey         :string(255)
#  type            :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Page < ActiveRecord::Base
  translates :content, :metatitle, :metadescription, :metakey, :address, :title
  globalize_accessors locales: [:en, :es, :pt], attributes: [:title, :content, :metatitle, :metadescription, :metakey, :address]
  belongs_to :building
end
