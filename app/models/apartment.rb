# == Schema Information
#
# Table name: apartments
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  ref               :string(255)
#  description       :text(65535)
#  apartment_type_id :integer
#  building_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  slug              :string(255)
#  metatitle         :string(255)
#  metadescription   :string(255)
#  metakey           :string(255)
#  num_persons       :integer
#  num_bedrooms      :integer
#  bed_kind          :string(255)
#  badroom           :integer          default(0)
#  double_couch      :integer          default(0)
#  living_room       :integer
#  cleaning          :string(255)
#  active            :boolean          default(TRUE)
#  deleted_at        :datetime
#

class Apartment < ActiveRecord::Base
  include AASM
  translates :description
  globalize_accessors :locales => [:en, :es, :pt], :attributes => [:description]
  KIND_OF_LIVING_ROOM = { :living_room_normal => 1, :living_room_fire_place => 2 }

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  belongs_to :apartment_type
  belongs_to :building
  has_many :block_apartment_reservations, as: :userable

  has_many :photos, :dependent => :destroy, :as => :photographable
  accepts_nested_attributes_for :photos, allow_destroy: true

  has_many :disabled_dates, :dependent => :destroy, :class_name => "ValidDate", :as => :validable
  accepts_nested_attributes_for :disabled_dates, allow_destroy: true

  has_and_belongs_to_many :equipments
  has_and_belongs_to_many :reservations
  has_and_belongs_to_many :coupons

  validates :name, presence: true
  validates :num_persons, presence: true, numericality: { only_integer: true }
  validates :num_bedrooms, presence: true, numericality: { only_integer: true }
  validates :bed_kind, presence: true
  validates :double_couch, presence: true, numericality: { only_integer: true }
  validates :badroom, presence: true, numericality: { only_integer: true }
  validates :living_room, presence: true
  validates :apartment_type, presence: true

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :no_deleted, -> { where(deleted_at: nil) }

  aasm :column => 'cleaning' do
    state :dirty
    state :cleaned, :initial => true

    event :clean do
      transitions :from => :dirty, :to => :cleaned
    end

    event :dirty do
      transitions :from => :cleaned, :to => :dirty
    end
  end

  def apartment_name
    "#{self.ref} - #{self.name} - (#{self.apartment_type.name}, PAX: #{self.num_persons}, HAB: #{self.num_bedrooms})"
  end

  def apartment_description
    "#{self.name} - (PAX: #{self.num_persons}, HAB: #{self.num_bedrooms})"
  end

  def get_price(start_date, end_date)
    @apartment_type = self.apartment_type
    @prices = @apartment_type.prices.between_times(start_date.to_time, end_date.to_time)

    if @prices.count < (end_date - start_date).to_i
      nil
    else
      @total_price = @prices.map { |p| p.price }.sum
    end
  end

  def booked?(start_date, end_date)
    @reservations = self.reservations.actives.between_times(start_date.beginning_of_day, end_date.end_of_day-1.days).count
    @reservations.zero? ? false : true
  end

  def get_booking_today
    @reservations = self.reservations.actives.between_times(Date.today, Date.today+1)
    @reservations.length == 0 ? nil : @reservations.first
  end

  def self.search_apartment_with(room, guests, building)
    Apartment.where('num_bedrooms >= ? and num_persons >= ? and building_id = ?', room, guests, building).active
  end

  def get_value_kind_of_living_room
 		KIND_OF_LIVING_ROOM.invert[self.living_room.to_i]
	end

	def should_generate_new_friendly_id?
  	name_changed?
	end

  def self.to_csv(options = {})
    column_names = [:ref, :name, :apartment_type_id, :num_persons, :num_bedrooms, :badroom, :cleaning, :active]
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |apart|
        csv << apart.attributes.values_at(*column_names)
      end
    end
  end

  #Dadas dos fechas te devuelve los apartamentos libres en ese rango
  def self.get_free_apartments_for_dates(start_date, end_date, building)
    apartments = []
    building.apartments.no_deleted.each do |ap|
      if !ap.booked?(start_date, end_date)
        apartments << ap
      end
    end
    apartments
  end


  #Dadas dos fechas te devuelve los apartamentos libres en ese rango de fechas y para la reserva actual.
  def self.get_free_apartments_for_dates_and_reservation(start_date, end_date, building, reservation)
    apartment_ids = reservation.apartments.pluck(:id)
    apartments = []
    building.apartments.no_deleted.each do |ap|
      if !ap.booked?(start_date.to_date, end_date.to_date) || apartment_ids.include?(ap.id)
        apartments << ap
      end
    end
    apartments
  end

  def self.get_reservation_json(building, month = nil, year = nil)
    res = []
    building.apartments.no_deleted.active.each do |apt|
      res << {
        apartment: apt.name,
        reference: apt.ref,
        reservations: apt.get_reservations(month, year)
      }
    end

    res.to_json
  end

  def get_reservations(month = nil, year = nil)
    res = []
    if month.nil? or year.nil?
      self.reservations.actives.finished.each do |r|
        res << {
          start_date: r.start_date,
          end_date: r.end_date,
          reference: "#{r.reference} - CLIENTE: #{r.get_renter_name} -  (#{r.start_date.strftime('%d/%m/%Y')} - #{r.end_date.strftime('%d/%m/%Y')}",
          color: r.channel.present? ? r.channel.color : "gray"
        }
      end
    else
      # month = Date::MONTHNAMES[month.to_i]
      date = DateTime.new(year.to_i, month.to_i, 1, 0, 0, 0)
      date_end = date.end_of_month

      self.reservations.actives.between_times(date-1.day, date_end).each do |r|
        res << {
          start_date: r.start_date,
          end_date: r.end_date,
          reference: "#{r.reference} - CLIENTE: #{r.get_renter_name} - (#{r.start_date.strftime('%d/%m/%Y')} - #{r.end_date.strftime('%d/%m/%Y')}",
          color: r.channel.present? ? r.channel.color : "gray"
        }
      end
    end
    res
  end

  # SE RESTA UN DÍA PARA QUE CONTEMPLE QUE LA FECHA DE INICIO DEBE ESTAR ESTRICTAMENTE EN RANGO, IGUALMENTE CON LA FECHA FIN
  # SI ESTAN BLOQUEADAS LAS FECHAS DEVUELVE TRUE, SI NO ESTÁ BLOQUEADO DEVUELVE FALSE
  def is_disabled_for_dates?(start_date, end_date)
    start_date = start_date.to_date
    end_date = end_date.to_date
    self.disabled_dates.between_times(start_date - 1.days, end_date + 1.days).count != 0
  end
end
