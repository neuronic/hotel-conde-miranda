# == Schema Information
#
# Table name: transactions
#
#  id                       :integer          not null, primary key
#  reference                :string(255)
#  reservation_id           :integer
#  amount                   :float(24)
#  datas                    :text(65535)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  Ds_ErrorCode             :string(255)
#  Ds_ErrorCode_description :string(255)
#  Ds_Response              :string(255)
#  Ds_Response_description  :string(255)
#  sent                     :boolean          default(FALSE)
#  type                     :string(255)
#  token_paypal             :string(255)
#  payer_id                 :string(255)
#  lang                     :string(255)
#

class TransactionTpv < Transaction
  after_create :set_reference

  private
  def set_reference
    self.reference = loop do
      ref = (0...8).map { [*'0'..'9',*'A'..'Z'].to_a[rand(26)] }.join
      nums_code = ('0'..'9').to_a.sort_by{rand}.join()[0..3]
      ref = nums_code.to_s + ref
      break ref unless Reservation.exists?(reference: ref)
    end
    self.save!
  end


end
