# == Schema Information
#
# Table name: clients
#
#  id                       :integer          not null, primary key
#  name                     :string(255)
#  surname                  :string(255)
#  nif                      :string(255)
#  email                    :string(255)
#  phone                    :string(255)
#  cif                      :string(255)
#  address                  :string(255)
#  city                     :string(255)
#  country                  :string(255)
#  business_name            :string(255)
#  type                     :string(255)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  juvenile                 :boolean          default(FALSE)
#  agree_ads                :boolean          default(FALSE)
#  date_birth               :date
#  passport                 :string(255)
#  nationality              :string(255)
#  gender                   :string(255)
#  expedition_date_passport :date
#  expedition_date_nif      :date
#  building_id              :integer
#

class Client < ActiveRecord::Base
  attr_accessor :reservation_id, :add_renter

  has_and_belongs_to_many :reservations
  belongs_to :building

  scope :users,    -> { where(type: 'User') }
  scope :businesses, -> { where(type: 'Business')}

  validates :agree_policy_privacy, presence: true

  def is_user?
    self.type == 'User'
  end

  def is_business?
    self.type == 'Business'
  end

  def self.to_csv(options = {})
    column_names = [:name, :surname, :nif, :email, :phone]
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |cli|
        csv << cli.attributes.values_at(*column_names)
      end
    end
  end

  def get_name
    if is_business?
      self.business_name
    else
      self.name
    end
  end

  def get_identity_card
    if is_business?
      self.cif
    else
      self.nif
    end
  end

end
