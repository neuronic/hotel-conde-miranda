# == Schema Information
#
# Table name: coupon_types
#
#  id         :integer          not null, primary key
#  coupon_id  :integer
#  service_id :integer
#  discount   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CouponType < ActiveRecord::Base
  belongs_to :coupon
  belongs_to :service

  validates :service, presence: true
  validates :discount, presence: true
end
