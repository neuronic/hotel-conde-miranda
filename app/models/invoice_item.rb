# == Schema Information
#
# Table name: invoice_items
#
#  id               :integer          not null, primary key
#  invoice_id       :integer
#  concept          :string(255)
#  description      :text(65535)
#  vat_type         :integer
#  unit             :integer
#  unit_price_base  :float(24)
#  unit_price       :float(24)
#  unit_vat         :float(24)
#  total_price_base :float(24)
#  total_price      :float(24)
#  total_vat        :float(24)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class InvoiceItem < ActiveRecord::Base
  belongs_to :invoice
end
