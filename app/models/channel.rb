# == Schema Information
#
# Table name: channels
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  contact_name  :string(255)
#  contact_email :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  color         :string(255)
#

class Channel < ActiveRecord::Base
	has_and_belongs_to_many :buildings
	has_many :reservations

	validates :name, presence: true
	validates :contact_name, presence: true
	validates :contact_email, presence: true

	TIPOS = {amarillo: "yellow", azul: "blue", naranja: "orange", rosa: "pink", verde: "green",
		violeta: "violet", cyan: "cyan", marron: "brown", coral: "coral", carmesi: "crimson"}

end
