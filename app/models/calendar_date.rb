class CalendarDate < ActiveRecord::Base
  belongs_to :building

  validates :date, presence: true
  validates :min_nights, numericality: { greater_than_or_equal_to: 0 }, if: :min_nights_present?
  validates_uniqueness_of :date

  by_star_field :date

  def as_json
    {
      date: date,
      min_nights: min_nights,
      blocked: blocked
    }
  end

  private

  def min_nights_present?
    min_nights.present?
  end
end
