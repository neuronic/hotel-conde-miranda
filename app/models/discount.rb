# == Schema Information
#
# Table name: discounts
#
#  id            :integer          not null, primary key
#  days          :integer
#  percentage    :integer
#  building_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  discount_type :string(255)
#  deleted_at    :datetime
#

class Discount < ActiveRecord::Base

	acts_as_paranoid
	
	translates :description
	globalize_accessors :locales => [:en, :es, :pt], :attributes => [:description]

	belongs_to :building
	#has_many :reservations

  has_many :discounts_reservations
  has_many :reservations, :through => :discounts_reservations


	validates :discount_type, presence: true
	validates :days, presence: true
	validates :percentage, presence: true
	validates :description_en, :description_es, presence: true

	scope :discount_nights, -> {where(:discount_type => 0)}
	scope :discount_beforehands, -> {where(:discount_type => 1)}
	
	TYPES = {0 => "discount_nights", 1 => "discount_beforehand"}

	def is_discount_night?
		self.discount_type == 0
	end

	def is_discount_beforehand?
		self.discount_type == 1
	end


	def self.check_if_discount_avaliable(nights)
		if discounts = Discount.discount_nights.where("days <= ?", nights).order("days ASC")
			return discounts.last
		else
			nil
		end
	end

	def self.check_if_days_before_discount_available(days_before)
		if discounts = Discount.discount_beforehands.where("days <= ?", days_before).order("days ASC")
			return discounts.last
		else
			nil
		end
	end

  translation_class.class_eval do
    def destroy
      run_callbacks :destroy
      true
    end
  end
end
