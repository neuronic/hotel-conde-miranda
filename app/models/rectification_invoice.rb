# == Schema Information
#
# Table name: rectification_invoices
#
#  id                 :integer          not null, primary key
#  number             :string(255)
#  invoice_id         :integer
#  attachment         :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  base               :float(24)
#  total              :float(24)
#  tax_amount         :float(24)
#  concept            :text(65535)
#  date_rectification :date
#

class RectificationInvoice < ActiveRecord::Base
  belongs_to :invoice
  mount_uploader :attachment, AttachmentUploader
  by_star_field :date_rectification

  def self.get_next_number_invoice(building)
    prefix         = building.invoice_address.prefix_invoice_number
    number_invoice = building.rectification_invoices.any? ? building.rectification_invoices.last.number.split('-') : ["#{prefix}000", '16']
    number         = number_invoice[0].scan(/\d+/).first
    year           = Date.today.year.to_s.last(2)

    if year != number_invoice[1]
      "#{prefix}R1-#{year}"
    else
      "#{prefix}R#{number.to_i + 1}-#{year}"
    end
  end
end
