# == Schema Information
#
# Table name: billing_addresses
#
#  id             :integer          not null, primary key
#  country        :string(255)
#  address        :string(255)
#  address_1      :string(255)
#  city           :string(255)
#  zip_code       :string(255)
#  reservation_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  nationality    :string(255)
#

class BillingAddress < ActiveRecord::Base
	
  ZIP_CODE_DEFAULT = 28

  belongs_to :reservation
	# has_one :billing_address
end
