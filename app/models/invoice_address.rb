# == Schema Information
#
# Table name: invoice_addresses
#
#  id                        :integer          not null, primary key
#  corporate_name            :string(255)
#  tax_domicile              :string(255)
#  country                   :string(255)
#  city                      :string(255)
#  cif                       :string(255)
#  email_contact             :string(255)
#  web_site                  :string(255)
#  logo                      :string(255)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  registry_data             :string(255)
#  free_text                 :text(65535)
#  client_accountant_account :string(255)
#  printed                   :string(255)
#  invoice_description       :string(255)
#  type_opration_one         :string(255)
#  type_opration_two         :string(255)
#

class InvoiceAddress < ActiveRecord::Base

	mount_uploader :logo, PictureUploader

	has_many :buildings

	validates :corporate_name, presence: true
	validates :tax_domicile, presence: true
	validates :country, presence: true
	validates :city, presence: true
	validates :cif, presence: true
	validates :email_contact, presence: true
	validates :web_site, presence: true


end
