# == Schema Information
#
# Table name: discounts_reservations
#
#  id             :integer          not null, primary key
#  discount_id    :integer
#  reservation_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class DiscountsReservation < ActiveRecord::Base
	belongs_to :reservation 
	belongs_to :discount
end
