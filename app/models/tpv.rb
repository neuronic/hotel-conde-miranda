class Tpv
  include Rails.application.routes.url_helpers
	require 'digest/sha1'
	attr_reader :currency, :reservation_ref, :terminal_number, :merchant_code,  :transaction_ref, :amount, :server_host, :response_code, :lang, :url, :description, :transaction_type
  
  # transaccion_id es el numero de transacción que nos devolverá en el post.
  def initialize(transaction)
    @config           = YAML.load_file(File.join(Rails.root, "config", "tpv.yml"))[Rails.env]
    @currency         = 978
    @transaction_type = 0
    @amount           = scape_zeros(transaction.amount)
    @merchant_code    = @config['merchant_code']
    @terminal_number  = @config['terminal_number']
    @url              = @config['url']
    @server_host      = @config['server_host'] # FIXME this parameter should be optional, delegating the host value to rails config
    @transaction_ref  = transaction.reference
    @lang             = generate_language
    @description      = transaction.reservation.description
    @reservation       = transaction.reservation
  end

  def merchant_parameters_hash
    url_ok = reservation_tpv_ok_url(@reservation.reference)
    url_ko = reservation_tpv_ko_url(@reservation.reference)
    parameters = {
      "DS_MERCHANT_AMOUNT" => @amount,
      "DS_MERCHANT_CURRENCY" => 978,
      "DS_MERCHANT_MERCHANTCODE" => @merchant_code,
      "DS_MERCHANT_MERCHANTURL" => tpv_notifier_reservations_url,
      "DS_MERCHANT_URLOK" => url_ok,
      "DS_MERCHANT_URLKO" => url_ko,
      "DS_MERCHANT_TERMINAL" => @terminal_number,
      "DS_MERCHANT_TRANSACTIONTYPE" => 0,
      "DS_MERCHANT_ORDER" => @transaction_ref
    }
  end


  def signed_request_order
    merchant_parameters = self.merchant_parameters_hash
    encoded_parameters = encode_parameters(merchant_parameters)
    signature = calculate_signature(encoded_parameters, merchant_parameters["DS_MERCHANT_ORDER"])
    {
      "Ds_SignatureVersion" => "HMAC_SHA256_V1",
      "Ds_MerchantParameters" => encoded_parameters,
      "Ds_Signature" => signature
    }
  end

  def self.redsys_response_parameters(params)
    JSON.parse(Base64.decode64(params["Ds_MerchantParameters"].tr("-_", "+/")))
    # decode_parameters(params["Ds_MerchantParameters"])
  end

  def check_response_signature?(transaction_ref, params)
    response_signature = Base64.strict_encode64(Base64.urlsafe_decode64(params["Ds_Signature"]))
    response_signature == calculate_signature(params["Ds_MerchantParameters"], transaction_ref)
  end



  def valid_transaction?(code)
    # valid transaction is in < 100 rangue
    return false if code.nil?
    (0..99).include?(code.to_i)
  end


  private 
  def encode_parameters(parameters)
   Base64.strict_encode64(parameters.to_json)
  end

  # def decode_parameters(parameters)
  #    JSON.parse(Base64.decode64(parameters.tr("-_", "+/")))
  # end


  def calculate_signature(b64_parameters, transaction_ref)
    unique_key_per_order = encrypt_3DES(transaction_ref, Base64.decode64(@config['ssha_key']))
    sign_hmac256(b64_parameters, unique_key_per_order)
  end

  def sign_hmac256(data, key)
    Base64.strict_encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), key, data))
  end

  def generate_language
    case I18n.locale
      when :en
        "002"
    else
      "001"
    end
  end

  def scape_zeros(price)

    string_price = price.to_f.round(2).to_s
    "#{ string_price.split(".").first }#{ string_price.split(".").last.ljust(2, "0") }"
  end


  def encrypt_3DES(data, key)
    cipher = OpenSSL::Cipher::Cipher.new("DES-EDE3-CBC")
    cipher.encrypt
    cipher.key = key
    if (data.bytesize % 8) > 0
      data += "\0" * (8 - data.bytesize % 8)
    end
    cipher.update(data)
  end

  def sign_hmac256(data, key)
    Base64.strict_encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), key, data))
  end

  # def generate_signature(merchant_url)
  #   string = "#{ @amount }#{ @transaction_ref }#{ @merchant_code }#{ @currency }#{ @transaction_type }#{ merchant_url }#{ @config['secret_seed'] }"
  #   Digest::SHA1.hexdigest(string).upcase
  # end

  # def notifier_signature_valid?(signature, response)
  #   signature == generate_notifier_signature(response)
  # end



  # private
  # def generate_notifier_signature(response)
  #   string = "#{ @amount }#{ @transaction_ref }#{ @merchant_code }#{ @currency }#{ response }#{ @config['secret_seed'] }"
  #   Digest::SHA1.hexdigest(string).upcase
  # end





end