# == Schema Information
#
# Table name: services
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  price        :float(24)
#  active       :boolean          default(FALSE)
#  building_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  published    :boolean
#  unique_price :float(24)
#  tax          :integer
#  rest_one_day :boolean          default(FALSE)
#

class Service < ActiveRecord::Base
	DESCRIPTION = ["Servicio opcional", "Optional service"]
	belongs_to :building
	has_many :coupon_types, dependent: :nullify
	translates :name, :description
	globalize_accessors :locales => [:en, :es, :pt], :attributes => [:name, :description]

	validates :name_es, :name_en, presence: true
	validates :price, presence: true, :if => :unique_no_present?
	validates :tax, presence: true

	scope :for_reservation, -> {where('published = true and active=true')}

	def unique_no_present?
		!self.unique_price.present?
	end

	def label_description
		self.unique_price? ? "#{self.name} (#{self.price}€)" : "#{self.name} (#{self.price}€/dia)"
	end

	# def label_description
	# 	"#{self.name}"
	# end

	def unique_price?
		self.unique_price.present?
	end

end
