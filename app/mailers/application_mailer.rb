# Application class
class ApplicationMailer < ActionMailer::Base
  add_template_helper(Admin::ReservationsHelper)
  default from: 'info@palacioplcondedemiranda.com'
  layout 'mailer'

  def mail_config(building)
    config = building.load_mail_config

    ActionMailer::Base.smtp_settings = {
      user_name: Settings[:mail_config][config][:user_name],
      password: Settings[:mail_config][config][:password],
      domain: Settings[:mail_config][config][:domain],
      address: Settings[:mail_config][config][:address],
      port: Settings[:mail_config][config][:port],
      authentication: Settings[:mail_config][config][:authentication],
      enable_starttls_auto: Settings[:mail_config][config][:enable_starttls_auto]
    }
  end
end
