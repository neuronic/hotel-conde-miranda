# Users mails
class UserMailer < ApplicationMailer
  def reservation_accepted(reservation, email = nil)
    @building    = reservation.building
    config       = mail_config(@building)
    @reservation = reservation
    @client      = @reservation.get_renter

    mail(
      from: config[:user_name],
      to: email || @client.email,
      subject: I18n.t('email.subject_resevation', building_name: @building.name)
    )
  end

  def new_reservation_to_admin(reservation)
    @building    = reservation.building
    config       = mail_config(@building)
    @reservation = reservation

    mail(
      from: config[:user_name],
      to: 'info@palacioplcondedemiranda.com',
      subject: "Nueva reserva - #{@building.name}"
    )
  end

  def send_invoice_to_user(email:, invoice:, message:)
    building = invoice.reservation.building
    config   = mail_config(building)
    @message = message
    attachments["invoice_#{invoice.number}.pdf"] = File.read(invoice.attachment.url)

    mail(from: config[:user_name], to: email, subject: "Invoice #{invoice.number}") do |format|
      format.html { render layout: 'mailer_notifications' }
    end
  end
end
