class MailPreview < MailView
  # Pull data from existing fixtures
  def invitation
    # I18n.locale = :en
    reservation = Rails.env == "production" ?  Reservation.find_by_reference('DA9JC5D') : Reservation.actives.last
    UserMailer.reservation_accepted(reservation)
  end

  def mail_to_admin
    reservation = Rails.env == "production" ?  Reservation.find_by_reference('DA9JC5D') : Reservation.actives.last
    UserMailer.new_reservation_to_admin(reservation)
  end
end