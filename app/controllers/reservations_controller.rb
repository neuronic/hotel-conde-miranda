class ReservationsController < BasePublicController
	skip_before_filter :verify_authenticity_token
  skip_before_filter :set_guest_session, only: :tpv_notifier

	def tpv_notifier
		#Ds_ErrorCode=SIS0051&Ds_TransactionType=0&Ds_Date=19%2F08%2F2015&Ds_SecurePayment=0&Ds_Order=1325H3JAJL3L&Ds_Signature=D61C9ECEBFD44D76CA25571EEE9D1C81B04B2B85&
		#Ds_Hour=12%3A47&Ds_Response=0913&Ds_AuthorisationCode=++++++&Ds_Currency=978&Ds_ConsumerLanguage=1&Ds_MerchantCode=334414075&Ds_Amount=69500&Ds_Terminal=001

		#Ds_TransactionType=0&Ds_Card_Country=724&Ds_Date=19%2F08%2F2015&Ds_SecurePayment=1&Ds_Signature=068B9439E4F9C8D2E4A33FCF609678CBB7CCD1C1&
		#Ds_Order=1325H3JAJL3L&Ds_Hour=12%3A41&Ds_Response=0000&Ds_AuthorisationCode=410463&Ds_Currency=978&Ds_ConsumerLanguage=2&
		#Ds_MerchantCode=334414075&Ds_Amount=69500&Ds_Terminal=001

    #Parameters: {
    #"Ds_SignatureVersion"=>"HMAC_SHA256_V1",
    #"Ds_Signature"=>"SqBUHr0ZCLWwuj71DwLUWzPjn6ypjAGSPg1yoniF8Ho=",
    #"Ds_MerchantParameters"=>"eyJEc19EYXRlIjoiMTZcLzAzXC8yMDE2IiwiRHNfSG91ciI6IjE4OjA0IiwiRHNfU2VjdXJlUGF5bWVudCI6IjEiLCJEc19DYXJkX0NvdW50cnkiOiI3MjQiLCJEc19BbW91bnQiOiI3MDIwMCIsIkRzX0N1cnJlbmN5IjoiOTc4IiwiRHNfT3JkZXIiOiI2NTM3MEsyMk83Q04iLCJEc19NZXJjaGFudENvZGUiOiIzMzQ0MTQwNzUiLCJEc19UZXJtaW5hbCI6IjAwMSIsIkRzX1Jlc3BvbnNlIjoiMDAwMCIsIkRzX01lcmNoYW50RGF0YSI6IiIsIkRzX1RyYW5zYWN0aW9uVHlwZSI6IjAiLCJEc19Db25zdW1lckxhbmd1YWdlIjoiMSIsIkRzX0F1dGhvcmlzYXRpb25Db2RlIjoiNjQ3NjA3In0="}

    # {"Ds_Date"=>"16/03/2016", "Ds_Hour"=>"19:33", "Ds_SecurePayment"=>"1", "Ds_Card_Country"=>"724", "Ds_Amount"=>"20000", "Ds_Currency"=>"978",
    # "Ds_Order"=>"3149A9NDIKC9", "Ds_MerchantCode"=>"334414075", "Ds_Terminal"=>"001", "Ds_Response"=>"0000", "Ds_MerchantData"=>"", "Ds_TransactionType"=>"0",
    # "Ds_ConsumerLanguage"=>"1", "Ds_AuthorisationCode"=>"647897"}
    params_merchant_decodec = Tpv.redsys_response_parameters(params)
		@transaction = Transaction.find_by_reference(params_merchant_decodec['Ds_Order'])
		@tpv = Tpv.new(@transaction)

		if !params_merchant_decodec['Ds_ErrorCode'].present? and @tpv.check_response_signature?(params_merchant_decodec['Ds_Order'], params) and @tpv.valid_transaction?(params_merchant_decodec['Ds_Response'])
			if @transaction
				@reservation = @transaction.reservation
				@reservation.process = "finished"
				@reservation.payment_summaries.create(:amount => money_format(params_merchant_decodec['Ds_Amount']), :method => PaymentSummary::METHODS[0] , :kind => PaymentSummary::TYPES[2], :transaction_id => @transaction.id)
				@reservation.paid_50!
				@transaction.datas = params_merchant_decodec
			end
		else
			@transaction.Ds_ErrorCode = params_merchant_decodec['Ds_ErrorCode'] if params_merchant_decodec['Ds_ErrorCode'].present?
			@transaction.datas = params_merchant_decodec
		end
		@transaction.Ds_Response = params_merchant_decodec['Ds_Response']
		@transaction.Ds_Response_description = response_code_message(params_merchant_decodec['Ds_Response'])
		if @transaction.save! and @tpv.valid_transaction?(params_merchant_decodec['Ds_Response'])
			I18n.locale = @transaction.lang || 'es'
			@reservation.calculate_rest_money
			UserMailer.reservation_accepted(@reservation).deliver_now
			UserMailer.new_reservation_to_admin(@reservation).deliver_now
		end
		render :nothing => true
	end


	def tpv_ok
		@reservation = Reservation.find_by_reference(params[:reservation_id])
		transaction = @reservation.transaction_tpv
		I18n.locale = transaction.lang || 'es'
    flash[:success] = "#{I18n.t 'reservation.ok'}"
		redirect_to reservation_build_path(@reservation.reference, :confirmation)
	end

	def tpv_ko
		@reservation = Reservation.find_by_reference(params[:reservation_id])
		transaction = @reservation.transaction_tpv
		I18n.locale = transaction.lang || 'es'
		flash[:error] = "#{I18n.t 'reservation.ko'}"
		redirect_to reservation_build_path(@reservation.reference, :payment)
	end

	def check_send
		@transaction = Transaction.find_by_reference(params[:transaction_id])
		@transaction.update_attribute(:sent, true)
		render :nothing => true
	end

	def paypal_ok
    unless params[:token] and params[:PayerID]
			flash[:error] = "#{I18n.t 'reservation.ko'}"
			redirect_to reservation_build_path(@reservation.reference, :payment)
      return
    end
    @transaction = Transaction.find_by_token_paypal(params[:token])
    begin
      @reservation = @transaction.reservation
      if @transaction && @reservation.process!="finished"
        I18n.locale = @transaction.lang || 'es'
      	@transaction.update_attribute(:payer_id, params[:PayerID])
  			@reservation = @transaction.reservation

        payment_request = Paypal::Payment::Request.new(
          :currency_code => :EUR,   # if nil, PayPal use USD as default
          :description   => "#{I18n.t 'reservation.paypal_desc'} PALACIO PL CONDE DE MIRANDA - #{@reservation.apartments.first.apartment_description} | #{@reservation.start_date.strftime('%d/%m/%Y')} - #{@reservation.end_date.strftime('%d/%m/%Y')}",    # item description
          :quantity      => 1,      # item quantity
          #:amount        => @reservation.total,   # item value
          :amount        => @reservation.calculate_deposit,
          :invoice       => @reservation.reference,
          :custom_fields => {
            CARTBORDERCOLOR: "f7f7f7",
            LOGOIMG: "https://www.palacioplcondedemiranda.com/assets/logo-s-paypal-18ea49d7a3a068f5dbb34fc493ffd55a88e63dedb5e1bbbbf1001a62c00052ef.jpg"
          }
        )
        client = Paypal::Express::Request.new PAYPAL_CONFIG
        response = client.checkout!(@transaction.token_paypal, @transaction.payer_id, payment_request)


  			@reservation.process = "finished"
  			@reservation.payment_summaries.create(:amount => @transaction.amount, :method => PaymentSummary::METHODS[1] , :kind => PaymentSummary::TYPES[3], :transaction_id => @transaction.id)
        @reservation.paid_50!
  			@transaction.datas = params
        if @transaction.save!
          @reservation.calculate_rest_money
          UserMailer.reservation_accepted(@reservation).deliver_now
          UserMailer.new_reservation_to_admin(@reservation).deliver_now if Rails.env.production?
          flash[:success] = "#{I18n.t 'reservation.ok'}"
          redirect_to reservation_build_path(@reservation.reference, :confirmation)
        else
          flash[:error] = "#{I18n.t 'reservation.ko'}"
          redirect_to reservation_build_path(@reservation.reference, :payment)
        end
  		else
  			flash[:error] = "#{I18n.t 'reservation.ko'}"
  			redirect_to reservation_build_path(@reservation.reference, :payment)
  		end
    rescue Exception => e
      flash[:error] = "#{I18n.t 'reservation.ko'}"
      redirect_to reservation_build_path(@reservation.reference, :payment)
    end
	end

	def paypal_ko
		@reservation = Reservation.find_by_reference(params[:reservation_id])
		transaction = @reservation.transaction_paypal
		I18n.locale = transaction.lang || 'es'
		flash[:error] = "#{I18n.t 'reservation.ko'}"
		redirect_to reservation_build_path(@reservation.reference, :payment)
    return
	end

	private

	def money_format(money)
		(money.to_i / 100).to_f
	end

	def response_code_message(code)
    case code.to_i
      when 0..99
        "Transacción autorizada para pagos y preautorizaciones"
      when 900
        "Transacción autorizada para devoluciones y confirmaciones"
      when 101
        "Tarjeta caducada"
      when 102
        "Tarjeta en excepción transitoria o bajo sospecha de fraude"
      when 104
        "Operación no permitida para esa tarjeta o terminal"
      when 116
        "Disponible insuficiente"
      when 118
        "Tarjeta no registrada o Método de pago no disponible para su tarjeta"
      when 129
        "Código de seguridad (CVV2/CVC2) incorrecto"
      when 180
        "Tarjeta no válida o Tarjeta ajena al servicio o Error en la llamada al MPI sin controlar."
      when 184
        "Error en la autenticación del titular"
      when 190
        "Denegación sin especificar Motivo"
      when 191
        "Fecha de caducidad errónea"
      when 202
        "Tarjeta en excepción transitoria o bajo sospecha de fraude con retirada de tarjeta"
      when 912,9912
        "Emisor no disponible"
      when 913
        "Pedido repetido"
      else
        "Transacción denegada"
      end
  	end

end
