# Controller home
class HomeController < BasePublicController
  layout 'public'

  def index
    @default_min_night    = @building.config_calendars.first.nights
    @restriction_calendar = @building.calendar_dates
                                     .where('date > ?', Date.today - 1)
                                     .pluck(:date, :min_nights, :blocked)
                                     .map { |e| [ e[0].strftime("%d/%m/%Y"), e[1], e[2] ] }
    @slider = @building.slider

    render :index, layout: 'public_v1'
  end

  def location
    @location = @building.page_location
  end

  def promotions
    @promotions = @building.promotions.includes(:translations).actives
  end

  def faqs
    @faq_page = @building.faq_page
    @faqs = @faq_page.questions.includes(:translations)
  end

  def contact
    @contact = ContactForm.new
  end

  def create_contact
    begin
      @contact_form = ContactForm.new(params_permited)
      @contact_form.request = request
      if @contact_form.deliver
        respond_to do |format|
          format.js { @status = true }
        end
      else
        respond_to do |format|
          format.js {
            @status = false
            @errors = @contact_form.errors.full_messages
          }
        end
      end
    rescue ScriptError
      format.js {
        @status = false
        @errors = @contact_form.errors.full_messages
      }
    end
  end

  def services; end

  private

  def params_permited
    params.require(:contact_form).permit(
      :name, :email, :message,
      :agree_ads, :agree_policy_privacy
    )
  end
end
