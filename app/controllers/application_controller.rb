class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale, :set_guest_session

  layout :layout_by_resource

	def set_locale
	  I18n.locale = params[:locale] || extract_locale_from_accept_language_header
	end

	private
  def extract_locale_from_accept_language_header
    if request.present? and request.env['HTTP_ACCEPT_LANGUAGE'].present?
      lang = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      lang = I18n.default_locale if !['es','en','pt'].include?(lang)
    else
      lang = 'es'
    end

    lang
  end

  def layout_by_resource
    if devise_controller?
      "devise"
    elsif current_admin and params[:controller].include?("admin")
      "admin"
    else
    	'public'
    end
  end

  def set_guest_session
    if !current_admin
      if (session[:expires_at] && session[:expires_at].to_time < Time.current && session[:guest_user_id]) || !session[:guest_user_id]
        @guest = Guest.create()
        session[:guest_user_id] = @guest.id
        session[:expires_at] = Time.current + 10.minutes
      else
        @guest = Guest.find_by(id: session[:guest_user_id])
        session[:expires_at] = Time.current + 10.minutes
      end
    else
      session[:guest_user_id] = nil
      session[:expires_at] = nil
    end
  end

  def current_user
    @guest || current_admin
  end
end
