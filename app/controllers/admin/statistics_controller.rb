class Admin::StatisticsController < Admin::BaseController
  
  def index
  	@end_time = Time.now + 8.days
  	@start = Time.now
    @reservations_by_apartments = @building.apartments.no_deleted.joins(:apartments_reservations, :reservations).distinct.where.not("reservations.status = 'canceled' or reservations.process = 'created'").group("reservations.id").pluck("name, count(reservations.id)")
    @reservation = @building.reservations.all
    @res_apto_month = @building.apartments.no_deleted.map{|apt| {name: apt.name, data: apt.reservations.actives.group_by_month(:start_date, last: 8).count} }
    @res_apto_year = @building.apartments.no_deleted.map{|apt| {name: apt.name, data: apt.reservations.actives.group_by_year(:start_date, last: 8).count} }
    @reservations_by_registrable = @building.reservations.all.actives.group(:createable_type).count
    @reservations_by_canal = @building.channels.all.joins(:reservations).distinct.where.not("reservations.status = 'canceled' or reservations.process = 'created'").group("reservations.channel_id").pluck("name, count(reservations.channel_id)")
  end

  def search_statistics
    @start = permit_params_search[:start].to_time || nil
    @end_time = permit_params_search[:end].to_time || nil

    if @start != nil and !@end_time.nil?
      @reservations_by_apartments = @building.apartments.no_deleted.joins(:apartments_reservations, :reservations).distinct.where("reservations.status != 'canceled' and reservations.process != 'created' and DATE(reservations.start_date) >= ? and DATE(reservations.start_date) <= ?", @start.to_date, @end_time.to_date).group("reservations.id").pluck("name, count(reservations.id)")
    	@reservation = @building.reservations.between_times(@start.beginning_of_day, @end_time.end_of_day)
      @res_apto_month = @building.apartments.no_deleted.map{|apt| {name: apt.name, data: apt.reservations.actives.group_by_month(:start_date, last: 8, range: @start ..@end_time).count} }
      @res_apto_year = @building.apartments.no_deleted.map{|apt| {name: apt.name, data: apt.reservations.actives.group_by_year(:start_date, last: 8, range: @start ..@end_time).count} }
      # @reservations_by_registrable = Reservation.all.where("reservations.status != 'canceled' and reservations.process != 'created' and DATE(reservations.start_date) >= ? and DATE(reservations.start_date) <= ?", @start.to_date, @end_time.to_date).group(:createable_type).count
      @reservations_by_registrable = @building.reservations.all.actives.between_times(@start.beginning_of_day, @end_time.end_of_day).group(:createable_type).count
      @reservations_by_canal = @building.channels.joins(:reservations).distinct.where("reservations.status != 'canceled' and reservations.process != 'created' and DATE(reservations.start_date) >= ? and DATE(reservations.start_date) <= ?", @start.to_date, @end_time.to_date).group("reservations.channel_id").pluck("name, count(reservations.channel_id)")      
      render 'index'
    else
      redirect_to admin_building_statistics_path
    end

  end

  private
  def permit_params_search
    params.require(:search).permit(:end, :start)
  end
end
