class Admin::PricesController < Admin::BaseController
  before_action :set_admin_price, only: [:show, :edit, :update, :destroy]
  before_action :set_building_and_type, only: [:index, :new, :create, :read_prices]
  # GET /admin/prices
  # GET /admin/prices.json
  def index

  end

  # GET /admin/prices/1
  # GET /admin/prices/1.json
  def show
  end

  # GET /admin/prices/new
  def new
    
    @from = Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
    @price = @apartment_type.prices.new
    @old_price = Price.where(:night => @from, :apartment_type_id => @apartment_type.id).first.price if !Price.where(:night => @from, :apartment_type_id => @apartment_type.id).empty? 
    respond_to do |format|
      format.js
    end
  end

  # GET /admin/prices/1/edit
  def edit
  end

  # POST /admin/prices
  # POST /admin/prices.json
  def create
    @from = permit_params[:from].to_date
    @to = permit_params[:to].empty? ? @from : permit_params[:to].to_date
    (@from..@to).each do |day|
      price = Price.where(night: day, :apartment_type_id => @apartment_type.id).first || Price.new(night: day, :apartment_type_id => @apartment_type.id)
      price.price = permit_params[:price]
      price.save!
    end 
    respond_to do |format|
        format.html { redirect_to @admin_price, notice: 'Price was successfully created.' }
        format.json { render :show, status: :created, location: @admin_price }
        format.js
    end
  end

  def read_prices
    @prices = @apartment_type.prices.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @prices.as_json("worker")}
    end
  end

  # PATCH/PUT /admin/prices/1
  # PATCH/PUT /admin/prices/1.json
  def update
    respond_to do |format|
      if @admin_price.update(admin_price_params)
        format.html { redirect_to @admin_price, notice: 'Price was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_price }
      else
        format.html { render :edit }
        format.json { render json: @admin_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/prices/1
  # DELETE /admin/prices/1.json
  def destroy
    @admin_price.destroy
    respond_to do |format|
      format.html { redirect_to admin_prices_url, notice: 'Price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_price
      @admin_price = Price.find(params[:id])
    end

    def set_building_and_type 
      @apartment_type = @building.apartment_types.find(params[:apartment_type_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permit_params
      params.require(:price).permit(:price, :from, :to)
    end
end
