class Admin::PromotionsController < Admin::BaseController

  before_action :set_admin_promotion, only: [:update, :edit, :destroy]

  def index
    @admin_promotions = @building.promotions.all
  end

  def new
    @admin_promotion = @building.promotions.new
  end

  def create
    @admin_promotion = @building.promotions.new(permit_params)

    if @admin_promotion.save  
      flash[:success] = "Promoción creada satisfactoriamente"
      redirect_to admin_building_promotions_path(@building)
    else
      flash[:error] = "Algo ha ido mal. Por favor, inténtalo de nuevo"
      render :new
    end
  end

  def edit; end

  def update
    if @admin_promotion.update(permit_params)
      redirect_to admin_building_promotions_path(@building), notice: 'La promoción ha sido actualizada correctamente'
    else
      render :edit 
    end
  end

  def change_status_promotions
    @promotion = @building.promotions.find(params[:promotion_id])
    
    if @promotion.active == true
      @promotion.update(:active => false)
    else
      @promotion.update(:active => true)
    end
    render :nothing => true
  end

  def destroy
    if @admin_promotion.destroy
      flash[:success] = "Promoción eliminada"
      redirect_to admin_building_promotions_path(@building)
    else
      flash[:error] = "Algo ha ido mal. Por favor, inténtelo de nuevo."
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_promotion
      @admin_promotion = @building.promotions.find(params[:id])
    end

    def permit_params
      params.require(:promotion).permit(:title_es, :title_en, :title_pt, :text_es, :text_en, :text_pt, :active, :image)
    end
end
