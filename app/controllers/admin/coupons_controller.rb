class Admin::CouponsController < Admin::BaseController

  before_action :set_admin_coupon, only: [ :update, :show]

  def index
    @admin_coupons = @building.coupons.all
  end

  def new
    @admin_coupon = @building.coupons.new
    @admin_coupon.code = Coupon.generate_code
    @apartments = @building.apartments.no_deleted
    @select_all = false
  end

  def create
    @admin_coupon = @building.coupons.new(permit_params)

    if @admin_coupon.save!
      # format.html { redirect_to admin_building_coupons_path(@building), notice: 'Has creado un nuevo cupón' }
      flash[:success] = "Cupón creado satisfactoriamente"
      redirect_to admin_building_coupons_path
    else
      @select_all = false
      @apartments = @building.apartments.no_deleted
      flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
      render :new
    end
  end

  def edit
    @admin_coupon = @building.coupons.find(params[:id])
    @apartments = @building.apartments.no_deleted
    @select_all = @admin_coupon.apartments.count == @apartments.count
    @disabled = @admin_coupon.reservations.actives.any?
  end

  def update
    respond_to do |format|
      if @admin_coupon.update(permit_params)
        format.html { redirect_to admin_building_coupons_path(@building), notice: 'El cupón ha sido actualizado correctamente' }
      else
        @apartments = @building.apartments.no_deleted
        @select_all = @admin_coupon.apartments.count == @apartments.count
        format.html { render :edit }
      end
    end
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_coupon
      @admin_coupon = @building.coupons.find(params[:id])
    end

    def permit_params
      params.require(:coupon).permit(:code, :description, :discount, :minimum_nights, 
        :maximum_nights, :deadline, apartment_ids:[], reservation_condition_ids:[], 
        valid_dates_attributes: [:id, :start_date, :end_date, :_destroy],
        coupon_types_attributes: [:id, :service_id, :discount, :_destroy]
        )
    end
end
