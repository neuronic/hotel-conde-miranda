class Admin::SlidersController < Admin::BaseController

  before_action :set_admin_slider, only: [ :update, :edit]

  def index
    @slider = @building.slider || @building.create_slider
  end

  def edit; end

  def update
    respond_to do |format|
      if @slider.update(permit_params)
        format.html { redirect_to admin_building_sliders_path(@building), notice: 'El slider ha sido actualizado correctamente' }
      else
        render :edit
      end
    end
  end

  private
  def set_admin_slider
    Slider.find(params[:id]) == @building.slider ? (@slider = @building.slider) : (redirect_to admin_building_sliders_path(@building), notice: 'El slider que quieres editar no pertenece a este edificio.')
  end


  def permit_params
    params.require(:slider).permit(photos_attributes: [:id, :caption_es, :caption_en, :caption_pt, :_destroy, :picture, :accordion_pic])
  end

end
