class Admin::ReservationsController < Admin::BaseController
  # before_action :set_building

  def index
    @q = @building.reservations.current.actives.ransack(params[:q])
    @reservations = @q.result(distinct: true).includes(:apartments)
    @title_view = "Reservas activas"

    respond_to do |format|
      format.html {
        @reservations = @reservations.paginate(:page => params[:page], :per_page => 10)
      }
      format.csv { render :export } #{send_data @admin_apartments.to_csv}
      format.xls { render :export } #{send_data @admin_apartments.to_csv(col_sep: "\t")}
    end
  end

  def index_finished
    @q = @building.reservations.past.actives.ransack(params[:q])
    @reservations = @q.result(distinct: true).includes(:apartments)
    @title_view = "Reservas finalizadas"

    respond_to do |format|
      format.html {
        @reservations = @reservations.paginate(:page => params[:page], :per_page => 10)
        render :index
      }
      format.csv { render :export }
      format.xls { render :export }
    end
  end

  def index_canceled
    @q = @building.reservations.canceled.ransack(params[:q])
    @reservations = @q.result(distinct: true).includes(:apartments)
    @title_view = "Reservas canceladas"

    respond_to do |format|
      format.html {
        @reservations = @reservations.paginate(:page => params[:page], :per_page => 10)
        render :index
      }
      format.csv { render :export }
      format.xls { render :export }
    end
  end

  def index_no_complete
    @reservations_unfinised =
    @building.reservations.includes(:apartments).created.where("created_at > ? ", 7.days.ago).order(created_at: :desc)
  end

  def show
    @reservation = @building.reservations.find(params[:id])
    @paid = @reservation.payment_summaries.sum(:amount)
    @rest =  @reservation.total_price - @paid
  end

  def new

  end

  def reservation
    @apartment = @building.apartments
                          .no_deleted
                          .find(params[:apartment_id])

    @q          = @building.clients.ransack(params[:q])
    @clients    = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
    @start_date = params[:start_date].to_time || nil
    @end_date   = params[:end_date].to_time || nil

    @reservation =  @apartment.reservations.new(
                      :start_date => @start_date,
                      :end_date => @end_date
                    )

    block_reservation_apartment = BlockReservationService.new(
                                    current_admin,
                                    @start_date,
                                    @end_date,
                                    @apartment.id
                                  )
    if block_reservation_apartment.blocked_dates_for_apartment?
      flash[:error] = "#{I18n.t 'reservation.error.in_process_booking'}"
      redirect_to admin_building_reservations_path
    else
      block_reservation_apartment.block_apartment_and_date!
    end
  end

  def create
    @reservation = @building.reservations.new(
      permit_params_reservation.except(:credit_card_number, :credit_card_date, :credit_card_holder)
    )
    @reservation.createable = current_admin
    @reservation.rest = @reservation.total_price
    @apartment = @building.apartments
                          .no_deleted
                          .find(params['reservation']['apartment_ids'])

    block_reservation_apartment = BlockReservationService.new(
      current_admin,
      @reservation.start_date,
      @reservation.end_date,
      @apartment.id
    )

    if !@apartment.booked?(@reservation.start_date, @reservation.end_date)
      unless block_reservation_apartment.blocked_dates_for_apartment?

        if @reservation.save!
          # Aqui se guarda la tarjeta cifrada
          @reservation.assign_attributes(permit_params_payment)
          if params['reservation']['client_ids'].present?
            c = @building.clients.find(params['reservation']['client_ids'])
            c.clients_reservations.create(reservation_id: @reservation.id, rol: "renter")
          end
          @apartment.reservations << @reservation

          nights = @reservation.nights
          #agregar a billing de la reserva
          @reservation.billings.create(
            :concept => "apartment",
            amount: @reservation.total_price,
            night: @reservation.nights,
            tax: Reservation::IVA,
            price: (@reservation.total_price/nights).round(2)
          )

          #Calculo precio restante
          @reservation.calculate_rest_money
          @reservation.finished!
          flash[:success] = "Nueva reserva creada correctamente. Referencia: #{@reservation.reference}"
          redirect_to admin_building_reservations_path
        else
          @start_date = permit_params_reservation[:start_date].to_time || nil
          @end_date   = permit_params_reservation[:end_date].to_time || nil
          @q          = @building.clients.ransack(params[:q])
          @clients    = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
          flash[:error] = "Algo fue mal durante la creación de la reserva, vuelva a intentarlo"
          render :reservation
        end
      else
        flash[:error] = "#{I18n.t 'reservation.error.in_process_booking'}"
        redirect_to admin_building_reservations_path
      end
    else
      flash[:error] = "La reserva existe en el sistema, por favor, compruébalo"
      redirect_to admin_building_reservations_path
    end
  end

  def copy_reservation
    reservation = @building.reservations.find(params[:id])
    begin
      @reservation = ReservationsService.duplicate(@building, reservation, current_admin)
      @apartment  = @reservation.apartments.first
      @q          = @building.clients.ransack(params[:q])
      @clients    = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
      render :edit
    rescue Exception => e
      flash[:error] = e.message
      redirect_to admin_building_reservations_path
    end
  end

  def edit
    @reservation = @building.reservations
                            .includes(reservation_condition: [:translations])
                            .find(params[:id])

    @payment_summaries  = @reservation.payment_summaries.where(:reason => "deposit")
    @apartment = @reservation.apartments.first

    @q        = @building.clients.ransack(params[:q])
    @clients  = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
  end


  def update
    @reservation = @building.reservations.find(params[:id])
    previous_process = @reservation.process
    @total_price = @reservation.total
    @apartment = @building.apartments
                          .no_deleted
                          .find(params['reservation']['apartment_reservation'])

    if !changed_apartment?(@reservation.apartments.first, @apartment) or
      (changed_apartment?(@reservation.apartments.first, @apartment) and
        !@apartment.booked?(params['reservation']['start_date'].to_date,params['reservation']['end_date'].to_date))

      if @reservation.update(permit_params_reservation)
        if changed_apartment?(@reservation.apartments.first, @apartment)
          @reservation.apartments.destroy_all
          @reservation.apartments << @apartment
        end
        if params['reservation']['client_ids'].present?
          c = @building.clients.find(params['reservation']['client_ids'])
          @reservation.change_rol_renter(c) if !@reservation.is_renter?(c)
        end

        #actualizar el precio de la reserva reserva
        reservation_nights = @reservation.nights
        if bill = @reservation.billings.find_by_concept("apartment")
          bill.update_attributes(
            amount: @reservation.total_price,
            price: (@reservation.total_price / reservation_nights).round(2),
            tax: Reservation::IVA,
            night: @reservation.nights
          )
        else
          @reservation.billings.create(
            :concept => "apartment",
            amount: @reservation.total_price,
            night: @reservation.nights,
            tax: Reservation::IVA,
            price: (@reservation.total_price / reservation_nights).round(2),
            description: Reservation::DESCRIPTION
          )
        end

        #Calculo precio restante
        @reservation.calculate_rest_money
        @reservation.no_paid! if @reservation.rest > 0 and !@reservation.no_paid?

        if @reservation.process != previous_process
          if @reservation.process == 'finished'
            UserMailer.reservation_accepted(@reservation).deliver_now
            UserMailer.new_reservation_to_admin(@reservation).deliver_now
          end
        end

        flash[:success] = "Reseva actualizada correctamente: #{@reservation.reference}"
        redirect_to admin_building_reservations_path
      else
        @start_date = @reservation.start_date
        @end_date   = @reservation.end_date
        @q          = @building.clients.ransack(params[:q])
        @clients    = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
        flash[:error] = "Algo fue mal durante la actualización de la reserva, vulva a intentarlo"
        render :edit
      end
    else
      @start_date = @reservation.start_date
      @end_date   = @reservation.end_date
      @q          = @building.clients.ransack(params[:q])
      @clients    = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
      @apartments = Apartment.get_free_apartments_for_dates_and_reservation(
        @reservation.start_date,
        @reservation.end_date,
        @building,
        @reservation
      )

      flash[:error] = "El apartamento escogido ya no está disponible para estos días, escoja otro por favor."
      render :edit
    end
  end

  def add_users
    @reservation = @building.reservations.find(params[:id])
    @q = @building.users.ransack(params[:q])
    @users = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
  end

  def add_user
    @reservation = @building.reservations.find(params[:id])
    @user = @building.users.find(params[:user_id])
    @reservation.clients_reservations.create(user_id: @user.id, rol: 'companion') if !@reservation.users.include?(@user)
  end

  def delete_user
    @reservation = @building.reservations.find(params[:id])
    @user = @building.users.find(params[:user_id])
    @reservation.clients_reservations.find_by_user_id(@user.id).delete
  end

  def search_apartmens
    respond_to do |format|
      format.html{
        @start = permit_params_search[:start].to_date || nil
        @end_date = permit_params_search[:end].to_date || nil
        if @start.nil? or @end_date.nil?
          flash[:error] = "Debes seleccionar las fechas de la estancia"
        else
          @apartments = []
          @building.apartments.no_deleted.each do |ap|
            if !ap.booked?(@start, @end_date)
              @apartments << ap
            end
          end
        end

        render 'new'
      }

      format.json{
        apartments  = Hash.new
        reservation = @building.reservations.find(params[:reservation_id])
        apartment   = @building.apartments.no_deleted.find(params[:apartment_id])
        start_date  = params[:start_date].to_date || nil
        end_date    = params[:end_date].to_date || nil

        # apartments_available  = ApartmentsService.apartment_free_for(
                        # current_admin,
                        # @building,
                        # start_date,
                        # end_date
                      # )

        apartments_available = Apartments::Search.new(
          current_user: current_admin,
          building: @building,
          start_date: start_date,
          end_date: end_date,
          exclude_inactives: false
        ).call

        apartments[apartment.apartment_name] = apartment.id
        apartments_available.each do |apartment|
          apartments[apartment.apartment_name] = apartment.id
        end

        render :json => apartments.to_json
      }
    end
  end

  def read_reservations
    @reservations = @building.reservations.actives
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @reservations.as_json}
    end
  end

  def read_reservations_for_custom_calendar
    month = params[:month].present? ? params[:month] : nil
    year = params[:year].present? ? params[:year] : nil
    respond_to do |format|
      format.json { render json: Apartment.get_reservation_json(@building, month, year)}
    end
  end

  def cancel
    @reservation =  @building.reservations.find(params[:id])
    @reservation.cancel!
    redirect_to admin_building_reservations_path
  end

  def summary_reservation
    @start_date = params[:start_date].to_date
    @end_date = params[:end_date].to_date
    @apartment = @building.apartments.find(params[:apartment_id])

    @error = @apartment.booked?(@start_date, @end_date)
    @error = true if @start_date < Date.today

    respond_to do |format|
      format.js
    end
  end

  def new_send_confirmation
    @reservation = Reservation.find(params[:reservation_id])
    # emails asocciate to reservations
    clients_ids = @reservation.clients_reservations.pluck(:user_id, :business_id).flatten.compact
    @emails = Client.where(id: clients_ids).pluck(:email)
  end

  def send_confirmation
    set_reservation
    resevation_noti_serv = ReservationNotificationsService.new(@reservation, permit_params_send_confirmation[:email])

    if resevation_noti_serv.valid? && resevation_noti_serv.send_confirmation
      flash[:success] = "Correo de confirmación enviado correctamente a #{resevation_noti_serv.email}
        de reserva: #{@reservation.reference}"
    else
      flash[:error] = resevation_noti_serv.errors
      clients_ids = @reservation.clients_reservations.pluck(:user_id, :business_id).flatten.compact
      @emails = Client.where(id: clients_ids).pluck(:email)
      render :new_send_confirmation
    end
  end

  private

  def set_reservation
    @reservation =  @building.reservations.find(params[:reservation_id])
  end

  def permit_params_search
    params.require(:search).permit(:end, :start, :status)
  end

  def permit_params_reservation
    params.require(:reservation).permit(:process, :apartment_reservation, :reservation_condition_id,
      :start_date, :end_date, :admin_price, :discount, :total_price, :normal_price, :credit_card_number,
      :credit_card_holder, :credit_card_date, :adults, :childs, :client_comments, :channel_id,
      :booking_reference, :comment, :status, :deposit,
      {:apartment_ids => []}, {:client_ids =>[]}, payment_summaries_attributes: [:id, :kind, :amount,
      :method, :_destroy], billings_services_attributes: [:id, :service_id, :night, :price, :tax,
      :concept, :amount, :_destroy]
    )
  end

  def permit_params_payment
    params.require(:reservation).permit(:credit_card_number, :credit_card_holder, :credit_card_date)
  end

  def permit_params_send_confirmation
    params.require(:send_confirmation).permit(:email)
  end

  def changed_apartment?(old_apartment, new_apartment)
    old_apartment != new_apartment
  end
end
