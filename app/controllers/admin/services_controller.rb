class Admin::ServicesController < Admin::BaseController
  def index
    @services = @building.services.all
  end

  def edit
    @service = @building.services.find(params[:id])
  end

  def update
    respond_to do |format|
      @service = @building.services.find(params[:id])
      if @service.update(permit_params)
        format.html { redirect_to admin_building_services_path(@building), notice: 'El servicio ha sido actualizado correctamente' }
      else
        format.html { render :edit }
      end
    end

    
  end

  def new
    @service = @building.services.new
  end

  def create
    @service = @building.services.new(permit_params)

    if @service.save
      # format.html { redirect_to admin_building_coupons_path(@building), notice: 'Has creado un nuevo cupón' }
      flash[:success] = "Servicio creado satisfactoriamente"
      redirect_to admin_building_services_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
      render :new
    end
  end

  def show
    @service = @building.services.find(params[:id])
  end

  def destroy
  end

  def change_status_services
    @service = @building.services.find(params[:service_id])
    
    if @service.active == true
      @service.update(:active => false)
    else
      @service.update(:active => true)
    end
    render :nothing => true
  end

  def read_datas
    @service = @building.services.find(params[:id])
    respond_to do |format|
      format.json{ render :json => @service }
    end
  end


  private
  def permit_params
    params.require(:service).permit(:name_es, :name_en, :name_pt, :price, :unique_price, :active, :published, :tax,
      :description_es, :description_en, :description_pt, :rest_one_day)
  end
end
