class Admin::RectificationInvoicesController < Admin::BaseController
  before_action :set_building_and_reservation, except: :download

  def new
    @rectification_invoice = @invoice.build_rectification_invoice
    respond_to do |format|
      format.js {}
    end
  end

  def create
    ActiveRecord::Base.transaction do
      @rectification = @invoice.build_rectification_invoice(permit_params)
      @rectification.number = RectificationInvoice.get_next_number_invoice(@building)

      if @rectification.save!
        @invoice.delete_relationship

        pdf = RectificationInvoicePdf.new(@invoice, @reservation, @rectification)
        tempfile = Tempfile.new(["#{@rectification.number}", '.pdf'], Rails.root.join('tmp'))
        tempfile.binmode
        tempfile.write pdf.render
        tempfile.close
        @rectification.attachment = tempfile
        @rectification.save!
        # Close the tempfile and delete it
        tempfile.unlink
      end

      redirect_to admin_building_reservation_invoices_path(@building, @reservation)
    end
  end

  def download
    path = rectification_invoice_path(params[:id], params[:basename], params[:extension])

    if params[:action_pdf].present?
      send_file path, x_sendfile: true, disposition: 'inline'
    else
      send_file path, x_sendfile: true
    end
  end

  private

  def set_building_and_reservation
    # @building = Building.find(params[:building_id])
    @reservation = @building.reservations.find(params[:reservation_id])
    @invoice = @building.invoices.find(params[:invoice_id])
  end

  def permit_params
    params.require(:rectification_invoice).permit(:total, :base, :tax_amount, :concept, :date_rectification)
  end
end
