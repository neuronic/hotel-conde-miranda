class Admin::AdHomesController < Admin::BaseController

  def index
    @ad_home = if @building.ad_homes.first 
      @building.ad_homes.first
    else
      ad_home = @building.ad_homes.new(active: false)
      ad_home.save(validate: false)
      ad_home
    end
  end

  def show
    @ad_home = @building.ad_homes.find(params[:id])
  end

  def edit
    @ad_home = @building.ad_homes.find(params[:id])
  end

  def update
    @ad_home = @building.ad_homes.find(params[:id])

    if @ad_home.update(permit_params)
      flash[:success] = "Publicidad editada correctamente"
      redirect_to admin_building_ad_homes_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtelo de nuevo"
      render :edit
    end
  end

  def change_status
    @ad_home = @building.ad_homes.find(params[:ad_home_id])
    @building.ad_homes.update_all(:active => false)

    if @ad_home.active == true
      @ad_home.update(:active => false)
    else
      @ad_home.update(:active => true)
    end   
  end

  private
  def permit_params
      params.require(:ad_home).permit(:name, :image, :type, :active)
  end

end