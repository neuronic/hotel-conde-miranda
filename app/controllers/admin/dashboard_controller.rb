class Admin::DashboardController < Admin::BaseController
  # before_action :set_building!

  def index
    @apartments = @building.apartments.includes(:apartment_type).no_deleted
    @reservation_start_today = @building.reservations.actives.start_today
    @reservation_end_today = @building.reservations.actives.end_today
    @reservation_reserved_today = @building.reservations.actives.reserved_today
    @hosted_clients = []
  end

  def export
    # @users = Client.find(params[:users])
    # @reservation = Reservation.find(params[:reservation])
    @reservations = @building.reservations.today
    respond_to do |format|
      format.html
      format.csv #{send_data @admin_apartments.to_csv}
      format.xls #{send_data @admin_apartments.to_csv(col_sep: "\t")}
    end
  end

  def recreate_img_versions
    Photo.find_each do |p|
      p.picture.recreate_versions!
    end
    redirect_to :back
  end


end
