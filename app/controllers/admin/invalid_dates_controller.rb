class Admin::InvalidDatesController < Admin::BaseController
  def index
    @invalid_dates = @building.invalid_dates.all
  end

  def new
    @invalid_date = @building.invalid_dates.new
  end

  def create
    @invalid_date = @building.invalid_dates.new(permit_params)

    if @invalid_date.save
      flash[:success] = "Se ha añadido una nueva fecha inválida"
      redirect_to admin_building_invalid_dates_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
      render :new
    end
  end

  def edit
    @invalid_date = @building.invalid_dates.find(params[:id])
  end

  def update
    @invalid_date = @building.invalid_dates.find(params[:id])

    if @invalid_date.update(permit_params)
      flash[:success] = "Fecha inválida editada correctamente"
      redirect_to admin_building_invalid_dates_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
      render :edit
    end
  end

  def destroy
    @invalid_date = @building.invalid_dates.find(params[:id])

    if @invalid_date.destroy
      flash[:success] = "Fecha inválida eliminada correctamente"
      redirect_to admin_building_invalid_dates_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
      render :back
    end
  end

  def permit_params
    params.require(:invalid_date).permit(:date, :description)
  end
end
