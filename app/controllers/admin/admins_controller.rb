class Admin::AdminsController < Admin::BaseController
	def index
		@admins = Admin.all
	end

	def new
		@admin = Admin.new
	end

	def create
		@admin = Admin.new(permit_params)

		if Admin::SUPER_PASSWORD == permit_params[:super_password]
			if @admin.save
				flash[:success] = "Nuevo administrador creado"
				redirect_to admin_buildings_path
			else
				flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
				render :new
			end
		else
			flash[:error] = "La super contraseña no es correcta"
			render :new
		end
	end

	def edit
		@admin = Admin.find(params[:id])
	end

	def update
		@admin = Admin.find(params[:id])

		if Admin::SUPER_PASSWORD == permit_params[:super_password]
			if @admin.update(permit_params)
				flash[:success] = "Administrador editado correctamente"
				redirect_to admin_buildings_path
			else
				flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
				render :edit
			end
		else
			flash[:error] = "La super contraseña no es correcta"
			render :edit
		end
	end

	def destroy
		@admin = Admin.find(params[:id])

		if @admin.active == true
			@admin.active = false
			if @admin.save
				flash[:success] = "Admin eliminado satisfactoriamente"
				redirect_to admin_buildings_path
			else
				flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
				redirect_to :back
			end
		end
	end

	private

	def permit_params
		params.require(:admin).permit(:email, :password, :active, :encrypted_password, :super_password)
	end
end
