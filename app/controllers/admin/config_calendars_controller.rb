class Admin::ConfigCalendarsController < Admin::BaseController
  def index
    @config_calendars = @building.config_calendars.all
  end

  def show
    @config_calendar = @building.config_calendars.find(params[:id])
  end

  def new
    @config_calendar = @building.config_calendars.new
  end

  def create
    @config_calendar = @building.config_calendars.new(permit_params)

    if @config_calendar.save
      flash[:success] = "Número de noches mínimas establecido correctamente"
      redirect_to admin_building_config_calendars_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtelo de nuevo"
      render :new
    end
  end

  def edit
    @config_calendar = @building.config_calendars.find(params[:id])
  end

  def update
    @config_calendar = @building.config_calendars.find(params[:id])

    if @config_calendar.update(permit_params)
      flash[:success] = "Número de noches mínimas editado correctamente"
      redirect_to admin_building_config_calendars_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtelo de nuevo"
      render :edit
    end
  end

  def destroy
  end

  private
  def permit_params
    params.require(:config_calendar).permit(:nights)
  end
end
