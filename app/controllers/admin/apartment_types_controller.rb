class Admin::ApartmentTypesController < Admin::BaseController
  # before_action :set_building!
  before_action :set_admin_apartment_type, only: [:edit, :update]

  # GET /admin/apartment_types
  # GET /admin/apartment_types.json
  def index
    @apartment_types = @building.apartment_types
  end

  # GET /admin/apartment_types/1/edit
  def edit
  end

  # PATCH/PUT /admin/apartment_types/1
  # PATCH/PUT /admin/apartment_types/1.json
  def update
    respond_to do |format|
      if @apartment_type.update(admin_apartment_type_params)
        format.html { redirect_to admin_building_apartment_types_path(@building), notice: 'El apartamento ha sido editado correctamente' }
      else
        format.html { render :edit }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_apartment_type
      @apartment_type = @building.apartment_types.find(params[:id])
    end

    # def set_building!
    #   @building = Building.find(params[:building_id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_apartment_type_params
      params.require(:apartment_type).permit(:num_persons, :num_bedrooms, :bed_kind, :badroom, :double_couch)
    end
end
