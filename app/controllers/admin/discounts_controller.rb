class Admin::DiscountsController < Admin::BaseController

  before_action :set_admin_discount, only: [ :update]

  def index
    @admin_discounts = @building.discounts.all
  end

  def new
    @admin_discount = @building.discounts.new
  end

  def create
    @admin_discount = @building.discounts.new(permit_params)

    if @admin_discount.save  
      flash[:success] = "Descuento creado satisfactoriamente"
      redirect_to admin_building_discounts_path
    else
      flash[:error] = "Algo ha ido mal. Por favor, inténtalo de nuevo"
      render :new
    end
  end

  def edit
    @admin_discount = @building.discounts.find(params[:id])
  end

  def update
    respond_to do |format|
      if @admin_discount.update(permit_params)
        format.html { redirect_to admin_building_discounts_path(@building), notice: 'El descuento ha sido actualizado correctamente' }
      else
        flash[:error] = "Algo ha ido mal. Por favor, inténtalo de nuevo"
        format.html { render :edit }
      end
    end
  end

  def destroy
    @admin_discounts = Discount.find(params[:id])
    if @admin_discounts.destroy
      flash[:success] = "Descuento eliminado"
      redirect_to admin_building_discounts_path(@building)
    else
      flash[:error] = "Algo ha ido mal. Por favor, inténtelo de nuevo."
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_discount
      @admin_discount = @building.discounts.find(params[:id])
    end

    def permit_params
      params.require(:discount).permit(:days, :percentage, :description_es, :description_en,
        :description_pt, :discount_type)
    end
end
