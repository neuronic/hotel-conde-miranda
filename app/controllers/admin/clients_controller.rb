class Admin::ClientsController < Admin::BaseController

  def index
    @q = @building.clients.ransack(params[:q])

    respond_to do |format|
      format.html {
        @clients = @q.result.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
      }
      format.csv {
        @clients = @q.result
        @type    = params[:type]
        render :export
      }
      format.xls {
        @clients = @q.result
        @type    = params[:type]
        render :export
      }
    end
  end

  def new
    @type = params[:type]
    @add_renter = params[:add_renter] if params[:add_renter].present?
    @reservation = params[:reservation_id].present? ? @building.reservations.actives.find(params[:reservation_id]) : nil

    if params[:type].include?("User")
      @user = @building.users.new
    else
      @business = @building.businesses.new
    end

    respond_to do |format|
      format.js
    end
  end

  def create
    if params[:user].present?
      @client = @building.users.new(permit_params_users)
      @type = 'User'
      @reservation = @building.reservations.actives.find(params[:user]['reservation_id']) if params[:user]['reservation_id'].present?
      @add_renter = params[:user]['add_renter'] if params[:user]['add_renter'].present?
    else
      @reservation = @building.reservations.actives.find(params[:business]['reservation_id']) if params[:business]['reservation_id'].present?
      @add_renter = params[:business]['add_renter'] if params[:business]['add_renter'].present?
      @add_renter = params[:business]['add_renter'] if params[:business]['add_renter'].present?
      @client = @building.businesses.new(permit_params_business)
    end

    @business = @building.businesses.all
    @users = @building.users.all

    if @client.save!
      if @add_renter.present? and @reservation.present?
        @reservation.change_rol_renter(@client)
      end
      respond_to do |format|
        format.js
      end
    end
  end

  def edit
    @reservation = params[:reservation_id].present? ? @building.reservations.actives.find(params[:reservation_id]) : nil
    @client = @building.clients.find(params[:id])
    @type = @client.type
  end

  def update
    if params[:user].present?
      @user = @building.users.find(params[:id])
      @type = 'User'
      @client = @user.update(permit_params_users)
      @reservation = @building.reservations.actives.find(params[:user]['reservation_id']) if params[:user]['reservation_id'].present?
      @users = @building.users.all
    else
      @business = @building.businesses.find(params[:id])
      @client = @business.update(permit_params_business)
      @business = @building.businesses.all
    end
  end

  def search
    @q = @building.clients.ransack(params[:q])
    @clients = @q.result.paginate(:page => params[:page], :per_page => 10)
    @reservation = Reservation.find_by(id: params[:reservation_id])
  end


  private
  def permit_params_users
    params.require(:user).permit(:type, :name, :surname, :email, :nif,
      :date_birth, :expedition_date_nif, :expedition_date_passport, :gender,
      :passport, :nationality, :phone, :reservation_id, :add_renter,
      :juvenile, :agree_ads, :agree_policy_privacy)
  end

  def permit_params_business
    params.require(:business).permit(:type, :name, :cif, :business_name,
      :address, :email, :phone, :city, :country, :reservation_id,
      :add_renter, :agree_policy_privacy, :agree_ads)
  end
end
