class Admin::ReservationConditionsController < Admin::BaseController
  before_action :set_reservation_condition, only: [:show, :edit,
    :update, :destroy, :read_percent, :change_active_desactive]

  def index
    @building.reservation_conditions.create(
      title_en: "Default price",
      title_es: "Precio defecto",
      percent: 0,
      annotation: "Precio defecto del apartamento",
      default: true,
      payment_percent: 50
    ) if !@building.reservation_conditions.any?

    @reservation_conditions = @building.reservation_conditions.all
  end

  def new
    @reservation_condition = @building.reservation_conditions.new
  end

  def edit
    @disabled = @reservation_condition.reservations.any? || @reservation_condition.default
  end

  def read_percent
    respond_to do |format|
      format.json{
        render :json => @reservation_condition.percent
      }
    end
  end

  def create
    @reservation_condition = @building.reservation_conditions.new(reservation_condition_params)

    respond_to do |format|
      if @reservation_condition.save
        format.html {
          redirect_to admin_building_reservation_conditions_path,
                      notice: 'La condición de reserva ha sido creada correctamente.'
        }
      else
        format.html { render :new }
      end
    end
  end


  def update
    respond_to do |format|
      if @reservation_condition.update(reservation_condition_params)
        format.html {
          redirect_to admin_building_reservation_conditions_path,
                      notice: 'La condición de reserva ha sido modificada correctamente.'
        }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @reservation_condition.destroy
    respond_to do |format|
      format.html {
        redirect_to admin_building_reservation_conditions_path,
                    notice: 'Condición de reserva borrada correctamente'
      }
      format.json { head :no_content }
    end
  end

  def change_active_desactive
    new_value = @reservation_condition.active.nil? ? DateTime.now : nil
    @reservation_condition.update_column(:active, new_value)
    render :nothing => true, :status => 200
  end


  private
    def set_reservation_condition
      @reservation_condition = @building.reservation_conditions.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_condition_params
      params.require(:reservation_condition)
            .permit(
                    :description_en, :description_es, :description_pt,
                    :percent, :title_es, :title_en, :title_pt ,
                    :annotation, :payment_percent, :active
                   )
    end
end
