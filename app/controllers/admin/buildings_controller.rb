class Admin::BuildingsController < Admin::BaseController
  skip_before_filter :set_building!
  before_action :set_admin_building, only: [:edit, :update]

  def index
    @admin_buildings = Building.all
    @admins = Admin.all
  end

  def edit
    @invoice_address_attributes = @building.invoice_address || @building.build_invoice_address
  end

  def update
    respond_to do |format|
      if @building.update!(admin_building_params)
        format.html { redirect_to admin_buildings_path, notice: 'El edificio se ha actualizado correctamente' }
      else
        @invoice_address_attributes = @building.invoice_address || @building.build_invoice_address
        format.html { render :edit }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_building
      @building = Building.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_building_params
      params.require(:building).permit(:name, :address, *Building.globalize_attribute_names,
        :mail_config, invoice_address_attributes: [:id, :corporate_name, :tax_domicile, :country, :city,
          :cif, :email_contact, :web_site, :logo, :registry_data, :free_text, :client_accountant_account,
          :printed, :invoice_description, :type_opration_one, :type_opration_two, :prefix_invoice_number]
      )
    end
end
