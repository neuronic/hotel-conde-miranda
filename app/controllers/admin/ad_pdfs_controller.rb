class Admin::AdPdfsController < Admin::BaseController
  def index
    @ad_pdfs =  @building.ad_pdfs.all
  end

  def new
    @ad_pdf = @building.ad_pdfs.new
  end

  def create
    @ad_pdf = @building.ad_pdfs.new(permit_params)

    if @ad_pdf.save
      flash[:success] = "Publicidad creada correctamente"
      redirect_to admin_building_ad_pdfs_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtelo de nuevo"
      render :new
    end
  end

  def edit
    @ad_pdf = @building.ad_pdfs.find(params[:id])
  end

  def update
    @ad_pdf = @building.ad_pdfs.find(params[:id])

    if @ad_pdf.update(permit_params)
      flash[:success] = "Publicidad editada correctamente"
      redirect_to admin_building_ad_pdfs_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtelo de nuevo"
      render :edit
    end
  end

  def show
    @ad_pdf = @building.ad_pdfs.find(params[:id])
  end

  def destroy
    @ad_pdf = @building.ad_pdfs.find(params[:id])

    if @ad_pdf.destroy
      flash[:success] = "Publicidad eliminada correctamente"
      redirect_to admin_building_ad_pdfs_path
    else
      flash[:error] = "Algo ha ido mal, por favor, inténtelo de nuevo"
      redirect_to :back
    end
  end

  def change_status
    @ad_pdf = @building.ad_pdfs.find(params[:ad_pdf_id])
    @building.ad_pdfs.update_all(:active => false)

    if @ad_pdf.active == true
      @ad_pdf.update(:active => false)
    else
      @ad_pdf.update(:active => true)
    end
    render :js => "window.location.href='"+admin_building_ad_pdfs_path+"'"

    
  end

  private
  def permit_params
      params.require(:ad_pdf).permit(:name, :image, :active)
  end
end

