class Admin::DisableApartmentsController < Admin::BaseController
  def index
  	@apartment = @building.apartments.no_deleted.find(params[:apartment_id])
  end

  def add_disabled_dates
  	@apartment = @building.apartments.no_deleted.find(params[:apartment_id])
  	if @apartment.update(permit_params)
  		redirect_to admin_building_apartments_path(@building)
  	else
  		render :index
  	end

  end

  def permit_params
  	params.require(:apartment).permit(disabled_dates_attributes: [:id, :_destroy, :start_date, :end_date])
  end
end
