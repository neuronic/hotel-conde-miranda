class Admin::BaseController < ApplicationController
  before_action :set_building!
  before_action :authenticate_admin!

  protected

  def after_sign_in_path_for(resource)
    # admin_root_path
    admin_authenticated_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    root_locale_path
  end

  private
  def set_building!
    @building = Building.find(params[:building_id]) if params[:building_id].present?
  end
end
