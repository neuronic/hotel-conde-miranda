class Admin::FinancesController < Admin::BaseController
  def index
    @start = params[:search] ? params[:search][:start].to_time : 6.months.ago
    @end_time = params[:search] ? params[:search][:end].to_time : nil

    if @start.nil? and @end_time.nil?
      @invoices = @building.invoices.includes(:rectification_invoice, :customer, :invoice_items)
      @rectifications = @building.rectification_invoices.includes(:invoice)
    elsif @start.nil? and !@end_time.nil?
      @invoices = @building.invoices.includes(:rectification_invoice, :customer, :invoice_items).before(@end_time.end_of_day)
      @rectifications = @building.rectification_invoices.includes(:invoice).before(@end_time.end_of_day)
    elsif !@start.nil? and @end_time.nil?
      @invoices = @building.invoices.includes(:rectification_invoice, :customer, :invoice_items).after(@start.beginning_of_day)
      @rectifications = @building.rectification_invoices.includes(:invoice).after(@start.beginning_of_day)
    else
      @invoices = @building.invoices.includes(:rectification_invoice, :customer, :invoice_items).between_times(@start.beginning_of_day, @end_time.end_of_day)
      @rectifications = @building.rectification_invoices.includes(:invoice).between_times(@start.beginning_of_day, @end_time.end_of_day)
    end

    respond_to do |format|
      format.html
      format.csv
      format.xls
    end
  end

  def export_finances
    @start = params[:search] ? params[:search][:start].to_time : nil
    @end_time = params[:search] ? params[:search][:end].to_time : nil

    if @start.nil? and @end_time.nil?
      @invoices = @building.invoices
      @rectifications = @building.rectification_invoices
    elsif @start.nil? and !@end_time.nil?
      @invoices = @building.invoices.before(@end_time.end_of_day)
      @rectifications = @building.rectification_invoices.before(@end_time.end_of_day)
    elsif !@start.nil? and @end_time.nil?
      @invoices = @building.invoices.after(@start.beginning_of_day)
      @rectifications = @building.rectification_invoices.after(@start.beginning_of_day)
    else
      @invoices = @building.invoices.between_times(@start.beginning_of_day, @end_time.end_of_day)
      @rectifications = @building.rectification_invoices.between_times(@start.beginning_of_day, @end_time.end_of_day)
    end

    respond_to do |format|
      format.csv
      format.xls
    end
  end
end
