class Admin::ApartmentsController < Admin::BaseController

  before_action :set_admin_apartment, only: [:show, :edit, :update, :destroy]

  # GET /admin/apartments
  # GET /admin/apartments.json
  def index
    @admin_apartments = @building.apartments.includes(:apartment_type).no_deleted
    respond_to do |format|
      format.html
      format.csv #{send_data @admin_apartments.to_csv}
      format.xls #{send_data @admin_apartments.to_csv(col_sep: "\t")}
    end
  end

  # GET /admin/apartments/1
  # GET /admin/apartments/1.json
  def show
    @admin_apartment = @building.apartments.includes(:apartment_type, :translations, :photos => [:translations]).find(params[:id])
  end

  # GET /admin/apartments/new
  def new
    @admin_apartment = @building.apartments.new
  end

  # GET /admin/apartments/1/edit
  def edit
  end

  # POST /admin/apartments
  # POST /admin/apartments.json
  def create
    @admin_apartment = @building.apartments.new(admin_apartment_params)

    respond_to do |format|
      if @admin_apartment.save
        format.html { redirect_to admin_building_apartments_path(@building), notice: 'Has creado un nuevo apartamento' }
        format.json { render :show, status: :created, location: @admin_apartment }
      else
        format.html { render :new }
        format.json { render json: @admin_apartment.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /admin/apartments/1
  # PATCH/PUT /admin/apartments/1.json
  def update
      if @admin_apartment.update(admin_apartment_params)
        redirect_to admin_building_apartments_path(@building), notice: 'El apartamento ha sido actualizado correctamente'
      else
        render :edit 
      end
  end

  def change_clean
    @apartment = Apartment.find_by_id(params[:apartment_id])
    if @apartment.cleaned?
      @apartment.dirty!
    else
      @apartment.clean!
    end
    render :nothing => true
  end

  def change_status
    @apartment = Apartment.find_by_id(params[:apartment_id])
    if @apartment.active == true
      @apartment.update(:active => false)
    else
      @apartment.update(:active => true)
    end
    render :nothing => true
  end

  def calendar
    @apartment = @building.apartments.no_deleted.find(params[:id])
  end

  def read_reservations
    @apartment = @building.apartments.no_deleted.find(params[:id])
    @reservations = @apartment.reservations.actives
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @reservations.as_json({:individual => true})}
    end
  end

  def read_price
    apartment = @building.apartments.no_deleted.find(params[:id])
    price = apartment.get_price(params[:start_date].to_date, params[:end_date].to_date)
    
    respond_to do |format|
      format.json{ render :json => price }
    end
  end

  def destroy
    @apartment = @building.apartments.no_deleted.find(params[:id])
    @apartment.update(deleted_at: DateTime.now)
    redirect_to admin_building_apartments_path(@building)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_apartment
      @admin_apartment = @building.apartments.includes(:apartment_type).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_apartment_params
      params.require(:apartment).permit(:ref, :name, :living_room, :num_persons, :num_bedrooms, 
        :bed_kind, :badroom, :double_couch, :description_es, :description_en, :description_pt, 
        :apartment_type_id, :metatitle, :metadescription, :metakey, equipment_ids: [], 
        photos_attributes: [:id, :caption_es, :caption_en, :caption_pt, :_destroy, :picture, :accordion_pic]
      )
    end
end
