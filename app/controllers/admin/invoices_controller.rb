class Admin::InvoicesController < Admin::BaseController
  before_action :set_building_and_reservation, except: :download
  before_action :set_invoice, only: [:new_send_invoice]

  def index
    @invoices = @reservation.invoices
  end

  def new
    @invoice = @reservation.invoices.new
    @total = @reservation.billings.no_invoice.sum(:amount)
    unless @reservation.billing_address
      renter = @reservation.get_renter
      @reservation.build_billing_address(
        country: renter.country,
        address: renter.address,
        city: renter.city,
        nationality: renter.nationality
      )
    end
  end

  def create
    ActiveRecord::Base.transaction do
      billing_address = @reservation.billing_address || @reservation.create_billing_address
      billing_address.update(permit_params[:billing_address])

      client   = @building.clients.find(permit_params[:customer_invoice])
      @invoice = @reservation.invoices.new(permit_params)

      @invoice.billing_address = billing_address
      @invoice.customer_id     = client.id
      @invoice.customer_type   = client.type
      @invoice.number          = Invoice.get_next_number_invoice(@building)

      if permit_params_billing.present? and @reservation.update(permit_params_billing) and @invoice.save!
        @reservation.billings.no_invoice.each do |b|
          b.update_attribute(:invoice_id, @invoice.id)
        end
        @invoice.update_attribute(:total, @invoice.total)

        # creamos los items de la factura
        @invoice.billings_in.each do |item|
          vat      = item.amount - (item.amount / ((item.tax/100.0) + 1))
          vat_unit = item.price - (item.price / ((item.tax/100.0) + 1))
          concept  = item.service.present? ? "#{item.service.name_es}/#{item.service.name_en}" : item.reservation.apartments.first.name

          description =
            if item.description.blank?
              item.concept.include?("service") ? "Servicio/Service" : "Apto./Apartment"
            else
              item.description
            end

          @invoice.invoice_items.create(
            concept: concept, description: description, vat_type: item.tax, unit: item.night,
            unit_price_base: (item.price - vat_unit), unit_price: item.price,
            total_price_base: (item.amount - vat),
            total_price: item.amount, unit_vat: vat_unit , total_vat: vat
          )
        end

        #crear pdf y guardar
        pdf = InvoicePdf.new(@invoice, @reservation)
        tempfile = Tempfile.new(["#{@invoice.id}", '.pdf'], Rails.root.join('tmp'))
        tempfile.binmode
        tempfile.write pdf.render
        tempfile.close
        @invoice.attachment = tempfile
        @invoice.save!
        # Close the tempfile and delete it
        tempfile.unlink

        redirect_to admin_building_reservation_invoices_path(@building,@reservation)
      else
        flash[:alert] = "No se pude crear una factura sin items o con items en blanco"
        @total = @reservation.billings.no_invoice.sum(:amount)
        render :new
      end
    end
  end

  def download
    path = invoice_path(params[:id], params[:basename], params[:extension])
    if params[:action_pdf].present?
      send_file path, :x_sendfile=>true, :disposition  => "inline"
    else
      send_file path, :x_sendfile=>true
    end
  end

  def destroy_all
    @building.invoices.destroy_all_invoices
    redirect_to :back
  end

  def new_send_invoice
    @send_invoice_form = Invoices::SendInvoiceForm.new
    @candidate_emails  = @send_invoice_form.candidate_emails(
      reservation: @reservation,
      invoice: @invoice
    )
  end

  def send_invoice
    @send_invoice_form = Invoices::SendInvoiceForm.new(permit_params_send_invoice)
    @invoice           = @send_invoice_form.invoice
    if @send_invoice_form.submit!
      flash[:success] = "Correo enviado a #{@send_invoice_form.email} con la factura adjunta #{@invoice.number}"
    else
      @candidate_emails  = @send_invoice_form.candidate_emails(
        reservation: @reservation,
        invoice: @invoice
      )

      render :new_send_invoice
    end
  end

  private
  def set_building_and_reservation
    @reservation = @building.reservations.find(params[:reservation_id])
  end

  def set_invoice
    @invoice =
      if params[:invoice_type] == 'invoice'
        @reservation.invoices.find(params[:id])
      else
        @reservation.rectification_invoices.find(params[:id])
      end
  end

  def permit_params
    params.require(:invoice).permit(:date, :customer_invoice, billing_address: [:address, :address_1, :country, :city, :zip_code, :nationality])
  end

  def permit_params_billing
    params.require(:invoice).permit(billings_attributes:[:id, :_destroy, :description, :price, :night, :tax, :amount, :service_id, :concept])
  end

  def permit_params_send_invoice
    params.require(:invoices_send_invoice_form).permit(:email, :invoice_id, :invoice_type, :message)
  end
end
