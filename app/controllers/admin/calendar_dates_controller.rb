class Admin
  # Controller to manage calendar dates
  # Only for admins
  class CalendarDatesController < Admin::BaseController
    before_action :load_calendar_date, only: %i[edit update destroy]

    def index
      @q              = @building.calendar_dates.ransack(params[:q])
      @calendar_dates = @q.result(distinct: true)
                          .order(created_at: :desc)
                          .paginate(page: params[:page], per_page: 20)
      flash.clear
    end

    def new
      create_new_calendar_date
    end

    def create
      @calendar_date = @building.calendar_dates.new(permit_params)
      if @calendar_date.save
        flash[:success] = I18n.t(
          'calendar_dates.form.success.save',
          date: @calendar_date.date.strftime('%d/%m/%y')
        )
        create_new_calendar_date
        @calendar_dates = @building.calendar_dates
                                   .order(created_at: :desc)
                                   .paginate(page: params[:page], per_page: 20)
      else
        flash[:error] = I18n.t('calendar_dates.form.error')
        render :new
      end
    end

    def edit; end

    def update
      if @calendar_date.update(permit_params)
        flash[:success] = I18n.t(
          'calendar_dates.form.success.update',
          date: @calendar_date.date.strftime('%d/%m/%y')
        )
      else
        flash[:error] = I18n.t('calendar_dates.form.error')
        render :new
      end
    end

    def destroy
      if @calendar_date.destroy
        flash[:success] = I18n.t(
          'calendar_dates.form.success.destroy',
          date: @calendar_date.date.strftime('%d/%m/%y')
        )
      else
        flash[:error] = I18n.t('calendar_dates.form.error')
      end
    end

    private

    def create_new_calendar_date
      @calendar_date = @building.calendar_dates.new
    end

    def load_calendar_date
      @calendar_date = @building.calendar_dates.find(params[:id])
    end

    def permit_params
      params.require(:calendar_date)
            .permit(:date, :min_nights, :blocked, :description)
    end
  end
end
