class Admin::ChannelsController < Admin::BaseController

	before_action :set_channel, only: [ :update, :show]

  def index
  	@channels = Channel.all
  end

  def new
  	@channel = @building.channels.new
  end

  def create
  	@channel = Channel.new(permit_params)
  	if @channel.save
  		@building.channels << @channel
  		flash[:success] = "Canal creado satisfactoriamente"
      redirect_to admin_building_channels_path
    else
    	flash[:error] = "Algo ha ido mal, por favor, inténtalo de nuevo"
      render :new
    end
  end

  def edit
  	@channel = @building.channels.find(params[:id])
  end

  def update
  	respond_to do |format|
      if @channel.update(permit_params)
        format.html { redirect_to admin_building_channels_path(@building), notice: 'El canal ha sido actualizado correctamente' }
      else
        format.html { render :edit }
      end
    end
  end

  def show
  end

  private

  	def set_channel
      @channel = @building.channels.find(params[:id])
    end

    def permit_params
    	params.require(:channel).permit(:name, :contact_name, :contact_email, :color)
    end
end
