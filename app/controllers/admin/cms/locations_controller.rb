class Admin::Cms::LocationsController < Admin::BaseController
  before_action :set_admin_slider, only: [ :update, :edit]

  def index
    @location = @building.page_location || @building.create_page_location
  end

  def edit

  end

  def update
    respond_to do |format|
      if @location.update(permit_params)
        format.html { redirect_to admin_building_cms_locations_path(@building), notice: 'La página de localización ha sido actualizado correctamente' }
      else
        render :edit
      end
    end
  end

  private
  def set_admin_slider
    @location = @building.page_location
  end


  def permit_params
    params.require(:cms_location).permit(:content_es, :content_en, :content_pt,
      :metatitle_es, :metatitle_en, :metatitle_pt,
      :metakey_es, :metakey_en, :metakey_pt,
      :metadescription_es, :metadescription_en, :metadescription_pt,
      :longitude, :latitude, :address_es, :address_en, :address_pt)
  end

end
