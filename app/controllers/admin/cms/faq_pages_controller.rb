class Admin::Cms::FaqPagesController < Admin::BaseController

  def edit
    @faq_page = @building.faq_page || @building.create_faq_page
  end

  def update
    @faq_page = @building.faq_page
    if @faq_page.update(permit_params)
      puts @faq_page.questions.inspect
      flash[:success] = 'La página de F.A.Q. ha sido actualizado correctamente'
      puts @faq_page.questions.inspect
      redirect_to edit_admin_building_cms_faq_page_path(@building, @faq_page)
    else
      flash[:error] = 'Por favor, revise los errores'
      render :edit
    end
  end

  private
  def permit_params
    params.require(:cms_faq_page).permit(
      :title_es, :title_en, :title_pt,
      :content_es, :content_en, :content_pt,
      :metatitle_es, :metatitle_en, :metatitle_pt,
      :metakey_es, :metakey_en, :metakey_pt,
      :metadescription_es, :metadescription_en, :metadescription_pt,
      :longitude, :latitude, :address_es, :address_en, :address_pt,
      questions_attributes: [:id, :_destroy, :question_es, :response_es,
      :question_en, :response_en, :question_pt, :response_pt,
      :image])
  end
end
