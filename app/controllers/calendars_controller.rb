# Controller to manage calendar public
class CalendarsController < BasePublicController
  def dates_conditions
    results = @building.calendar_dates
                       .where('date > ?', Date.today - 1)
                       .select(:date, :min_nights, :blocked)

    render json: {
      special_min_dates: results,
      custom_min_dates: @building.config_calendars.first
    }.as_json
  end
end
