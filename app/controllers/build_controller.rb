class BuildController < BasePublicController
  include Wicked::Wizard
  # http_basic_authenticate_with name: "admin", password: "admin", only: :new
  steps :data_user, :services, :payment, :confirmation
  before_action :validations_before_reservation_form, only: :new

  def new
    @start_date = params[:start_date].to_date
    @end_date   = params[:end_date].to_date
    condition   = @building.reservation_conditions.actives.find(params[:condition_reservation])

    @reservation = @apartment.reservations.new(
      start_date: params[:start_date],
      end_date: params[:end_date],
      building_id: @building.id
    )

    # Asignamos descuentos bien sean de cupones o de systema.
    coupon_service = CouponService.new(
      @coupon,
      @start_date,
      @end_date
    )

    if coupon_service.coupon_is_on_time_and_conditions? && coupon_service.coupon_is_valid_for_apartment?(@apartment)
      @reservation.coupon   = @coupon
      @reservation.discount = @coupon.discount
    end

    if @coupon.nil?
      discount_nights     = @building.discounts.check_if_discount_avaliable(@reservation.nights)
      discount_beforehand = @building.discounts.check_if_days_before_discount_available(@reservation.days_before)
      discount_system     = discount_nights || discount_beforehand
      if discount_system
        @reservation.discount_systems << discount_system
        @reservation.discount = discount_system.percentage
      end
    end

    # Precios
    reservation_service = ReservationService.new(
      @coupon,
      @building,
      @start_date,
      @end_date,
      discount_nights,
      discount_beforehand
    )

    total_price                        = @apartment.get_price(@start_date, @end_date).try(:to_f)
    prices_for_apartment_and_condition = reservation_service.get_prices_for_apartment_and_conditions(
      @apartment,
      total_price,
      condition
    )

    @reservation.reservation_condition = condition
    @reservation.apartments << @apartment

    @reservation.normal_price = prices_for_apartment_and_condition[0][:normal_price]
    @reservation.total_price  = prices_for_apartment_and_condition[0][:discount_price]
    @reservation.process      = 'created'

    block_reservation_apartment = BlockReservationService.new(
      current_user,
      @start_date,
      @end_date,
      @apartment.id
    )

    unless block_reservation_apartment.blocked_dates_for_apartment?
      # bloqueamos el apartamentos para esa fecha
      block_reservation_apartment.block_apartment_and_date!
      @reservation.save!

      reservation_nights = @reservation.nights
      @reservation.billings.create(
        concept: Billing::CONCEPTS[0],
        amount: @reservation.total_price,
        night: reservation_nights,
        tax: Reservation::IVA,
        price: (@reservation.total_price / reservation_nights).round(2)
      )

      redirect_to wizard_path(steps.first, reservation_id: @reservation.reference)
    else
      flash[:error] = I18n.t 'reservation.error.in_process_booking'
      redirect_to apartments_path
    end
  end

  def show
    @step = step
    @reservation = @building.reservations
                            .includes(:reservation_condition)
                            .unscoped
                            .find_by_reference(params[:reservation_id])
    @apartment = @reservation.apartments.first

    # condicional de si la condicion de pago es obligatorio a no
    # -> Obligatoria: Hay que llevar a usuario al pago
    # -> No obligatoria: Se ejecuta la reserva directamente sin pago.
    @has_payment_condition = @reservation.reservation_condition.has_payment_condition?

    block_reservation_apartment = BlockReservationService.new(
      current_user,
      @reservation.start_date,
      @reservation.end_date,
      @apartment.id
    )

    if !block_reservation_apartment.blocked_dates_for_apartment? || @step == :confirmation
      block_reservation_apartment.block_apartment_and_date!

      if step == :data_user
        # Vista de introducción de datos de usuario
        @billing_address = @reservation.billing_address.present? ? @reservation.billing_address : @reservation.build_billing_address
        if @client = @reservation.get_renter
          @user     = @client.is_user?     ? @client : @building.users.new
          @business = @client.is_business? ? @client : @building.businesses.new
        else
          @user = @building.users.new
          @business = @building.businesses.new
        end

      elsif step == :services
        # Vista de elección de servicios.
        @services = @building.services.all.for_reservation
        @billings = @reservation.billings if @reservation.billings.present?
        @coupon_service = CouponService.new(
          @reservation.coupon,
          @reservation.start_date,
          @reservation.end_date
        )

      elsif step == :payment
        # Vista de resumen y para salir a la pasarela de pago.
        @deposit = @reservation.calculate_deposit

        if @has_payment_condition
          # Si la condición de la reserva tiene pago obligatorio
          # Preparamos el pago para el tpv.
          if @reservation.has_tpv_payment?
            @transaction = @reservation.transaction_tpv
          else
            if (@reservation.transaction_tpv.present? and @reservation.transaction_tpv.is_sent?) or !@reservation.transaction_tpv.present?
              @transaction = @reservation.create_transaction_tpv(
                :amount => @deposit,
                :lang => I18n.locale.to_s
              )
            else
              @transaction = @reservation.transaction_tpv
            end
          end
          @tpv = Tpv.new(@transaction)
        end

      elsif step == :confirmation
        # Página de aceptación.
        # No pueden acceder a la página de confirmación si no se ha pagado anteriormente
        # o la condición no tenía el pago obligatorio.
        if !@reservation.has_payment_transaction? && @has_payment_condition
          redirect_to apartments_path
          return
        end
      end
      render_wizard
    else
      flash[:error] = "#{I18n.t 'reservation.error.in_process_booking'}"
      redirect_to apartments_path
    end
  end

  def update
    @reservation = @building.reservations
                            .includes(:reservation_condition)
                            .unscoped
                            .find_by_reference(params[:reservation_id])

    @apartment = @reservation.apartments.first
    @has_payment_condition = @reservation.reservation_condition.has_payment_condition?

    if @apartment.booked?(@reservation.start_date, @reservation.end_date)
      flash[:error] = "#{I18n.t 'reservation.error.already_booked'}"
      redirect_to apartments_path and return
    else
      block_reservation_apartment = BlockReservationService.new(
        current_user,
        @reservation.start_date,
        @reservation.end_date,
        @reservation.apartments.first.id
      )

      unless block_reservation_apartment.blocked_dates_for_apartment?
        block_reservation_apartment.block_apartment_and_date!
        if step == :data_user
          # Vista de datos de usuarios
          if permit_params[:user].present?
            @client = @building.users.find_or_initialize_by(email: permit_params[:user][:email])
            @client.update_attributes(permit_params[:user])
          else
            @client = @building.businesses.find_by_or_initialize_by(email: permit_params[:business][:email])
            @client.update_attributes(permit_params[:business])
          end

          #buscamos si tiene ya un renter asignado
          @reservation.createable = @client
          @reservation.clients_reservations.where(:rol => "renter").destroy_all
          @client.clients_reservations.create(reservation_id: @reservation.id, rol: "renter")

        elsif step == :services
          # Vista de los servicios
          @reservation.billings.services.destroy_all if @reservation.billings.present?

          if permit_params[:services].present?

            @coupon_service = CouponService.new(
              @reservation.coupon,
              @reservation.start_date,
              @reservation.end_date
            )

            nights = @reservation.nights
            permit_params[:services].each do |service|
              service = @building.services.find(service)
              discount = @coupon_service.discount_for_service(service)
              if service.unique_price?
                amount = service.unique_price - (service.unique_price*(discount/100.0))
                @reservation.billings.create(
                  :night => 1,
                  concept: Billing::CONCEPTS[1],
                  service_id: service.id,
                  amount: amount,
                  price: amount,
                  tax: service.tax
                )
              else
                real_nights = service.rest_one_day ? nights - 1 : nights

                price_per_day = (service.price - (service.price*(discount/100.0)))
                amount = real_nights * price_per_day
                @reservation.billings.create(
                  :night => real_nights,
                  concept: Billing::CONCEPTS[1],
                  service_id: service.id,
                  amount: amount,
                  price: price_per_day,
                  tax: service.tax
                )
              end
            end
          end
        elsif step == :payment
          if @has_payment_condition
            #pago con paypal
            request = Paypal::Express::Request.new PAYPAL_CONFIG
            @deposit = @reservation.calculate_deposit

            paypal_options = {
              no_shipping: true,  # if you want to disable shipping information
              allow_note: false,  # if you want to disable notes
              pay_on_paypal: true # if you don't plan on showing your own confirmation step
            }

            payment_request = Paypal::Payment::Request.new(
              :currency_code => :EUR,   # if nil, PayPal use USD as default
              :description   => "#{I18n.t 'reservation.paypal_desc'} PALACIO PL CONDE DE MIRANDA - #{@reservation.apartments.first.apartment_description} | #{@reservation.start_date.strftime('%d/%m/%Y')} - #{@reservation.end_date.strftime('%d/%m/%Y')}",    # item description
              :quantity      => 1,      # item quantity
              :amount        => @deposit,
              :invoice       => @reservation.reference,
              :custom_fields => {
                CARTBORDERCOLOR: "f7f7f7",
                LOGOIMG: "https://www.palacioplcondedemiranda.com/assets/logo-s-paypal-18ea49d7a3a068f5dbb34fc493ffd55a88e63dedb5e1bbbbf1001a62c00052ef.jpg"
              }
            )
            response = request.setup(
              payment_request,
              reservation_paypal_ok_url(@reservation.reference),
              reservation_paypal_ko_url(@reservation.reference),
              paypal_options
            )

            if trans_paypal = @reservation.transaction_paypal
              trans_paypal.update(
                token_paypal: response.token,
                :amount => @deposit,
                :lang => I18n.locale.to_s
              )
            else
              @reservation.create_transaction_paypal(
                token_paypal: response.token,
                :sent => true,
                :amount => @deposit,
                :lang => I18n.locale.to_s
              )
            end

            redirect_to response.redirect_uri and return
          else
            @reservation.assign_attributes(permit_params_payment)
            @reservation.process = "finished"
            @reservation.calculate_rest_money
            @reservation.save!
            UserMailer.reservation_accepted(@reservation).deliver_now
            UserMailer.new_reservation_to_admin(@reservation).deliver_now if Rails.env.production?
            flash[:success] = "#{I18n.t 'reservation.ok'}"
            redirect_to reservation_build_path(@reservation.reference, :confirmation) and return
          end
        end
        @reservation.update_attributes(permit_params)
        render_wizard @reservation
      else
        flash[:error] = "#{I18n.t 'reservation.error.in_process_booking'}"
        redirect_to apartments_path and return
      end
    end
  end


  private

  def permit_params
    params.fetch(:reservation, {}).permit(:accepted_conditions,
      :adults, :childs, :client_comments, services:[],
      user: [:name, :email, :surname, :date_birth, :phone, :agree_ads, :agree_policy_privacy],
      business: [:name, :email, :cif, :business_name, :phone, :agree_ads, :agree_policy_privacy],
      billing_address_attributes: [:address, :address_1, :country, :city, :zip_code]
    )
  end

  def permit_params_payment
    params.require(:reservation).permit(:credit_card_number, :credit_card_holder, :credit_card_date)
  end


  def validations_before_reservation_form
    start_date = params[:start_date].to_date
    end_date = params[:end_date].to_date
    @apartment = @building.apartments.no_deleted.find(params[:apartment_id])
    @coupon = @building.coupons.find_by_code(params[:coupon]) if !params[:coupon].blank?
    service_calendar = CalendarDatesService.new(start_date, end_date)
    if @apartment.booked?(start_date, end_date)
      flash[:error] = "#{I18n.t 'reservation.error.already_booked'}"
      redirect_to :back
    elsif @apartment.is_disabled_for_dates?(start_date, end_date)
      flash[:error] = "#{I18n.t 'reservation.error.apartment_no_available'}"
      redirect_to :back
    elsif !service_calendar.valid?
      flash[:error] = service_calendar.error
      redirect_to :back
    end
  end

end
