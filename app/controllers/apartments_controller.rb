class ApartmentsController < BasePublicController
  def index
    @apartments = @building.apartments
                           .includes(:apartment_type, :photos, :translations)
                           .no_deleted
                           .active
  end

  def search
    parse_dates_searches

    service_search_apts = Apartments::Search.new(
      current_user: current_user,
      building: @building,
      start_date: @start_date,
      end_date: @end_date,
      exclude_inactives: true
    )

    if service_search_apts.valid? && service_search_apts.call
      apts = service_search_apts.apartments_available

      @coupon = @building.coupons.find_by_code(permit_params_search[:code]) unless permit_params_search[:code].blank?
      coupon_service = CouponService.new(@coupon, @start_date, @end_date)
      days_before = (@start_date - Date.today).to_i

      if !@coupon
        @nights              = (@end_date - @start_date).to_i
        discount_nights      = @building.discounts.check_if_discount_avaliable(@nights)
        discount_beforehand  = @building.discounts.check_if_days_before_discount_available(days_before)
        @applicable_discount = discount_nights || discount_beforehand
      elsif @coupon && !coupon_service.coupon_is_on_time_and_conditions?
        @error_coupon = 'coupon_no_valid'
      end

      reservation_service = ReservationService.new(
        @coupon,
        @building,
        @start_date,
        @end_date,
        discount_nights,
        discount_beforehand
      )

      @apartments = []
      apts.each do |apartment|
        price = apartment.get_price(@start_date, @end_date).try(:to_f)
        if !price.nil? && !price.zero?
          @apartments << [apartment, reservation_service.get_prices_for_apartment_and_conditions(apartment, price)]
        end
      end

      render :search
    else
      flash[:danger] = I18n.t 'reservation.error.invalid_dates'
      redirect_to :back
    end
  end

  def show
    @apartment = @building.apartments.includes(:photos => [:translations]).no_deleted.find(params[:id])
    @from_reservation = params[:apartment_id].present? and params[:coupon].present? and params[:end_date].present? and params[:start_date].present? and params[:price].present?
  end

  def get_invalid_dates
    @invalid_dates = @building.invalid_dates.all.map(&:date)
    respond_to do |format|
      format.json { render json: @invalid_dates }
    end
  end

  def get_min_nights
    respond_to do |format|
      if (min_nights = @building.config_calendars.first)
        format.json { render json: min_nights.nights }
      else
        format.json { render json: 1 }
      end
    end
  end

  private

  def parse_dates_searches
    if permit_params_search[:dates]
      dates       = permit_params_search[:dates].tr(' ','').split('-')
      @start_date = dates[0].try(:to_date)
      @end_date   = dates[1].try(:to_date)
    else
      @start_date = permit_params_search[:check_in].try(:to_date)
      @end_date   = permit_params_search[:check_out].try(:to_date)
    end
  end

  def permit_params_search
    params.permit(:dates, :guests, :code, :check_in, :check_out)
  end
end
