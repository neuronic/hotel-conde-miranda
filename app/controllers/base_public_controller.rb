# Base controller public zone
class BasePublicController < ApplicationController
  before_action :set_building!

  protected

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

  def set_building!
    @building = Building.first
  end
end
