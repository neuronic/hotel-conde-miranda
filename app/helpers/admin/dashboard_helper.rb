module Admin::DashboardHelper
	def general_dashboard?
		(params[:controller].include?("admin/buildings") and params[:action].include?("index")) or 
		(params[:controller].include?("admin/admins"))
	end

end
