module Admin::ReservationsHelper
	def num_night(start_date, end_date)
		(end_date - start_date).to_i
	end

	def normal_price(amount)
		if amount.nil?
			content_tag(:span, content_tag(:span, "Imposible calcular el precio, algún o algunos días no tienen precio estipulado"), class: "strong")
		else
			number_to_currency(amount, :unit => "€", :separator => ",", :delimiter => ".", strip_insignificant_zeros: true) 
		end
	end
end
