module Admin::ReservationConditionsHelper
  def action_active_reservations?
    params[:action] == 'index'
  end

  def action_canceled_reservations?
    params[:action] == 'index_canceled'
  end
end
