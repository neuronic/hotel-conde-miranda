module ReservationsHelper
  def calculate_price_with_reservation_condition(condition, price, discount)
    if discount!=0
      price = (price*condition.percent)/100 + price
      price = price - price *(discount/100.0)
    else
      price = (price*condition.percent)/100 + price
    end
    number_to_currency(price, :unit => "€", :separator => ",", :delimiter => ".", strip_insignificant_zeros: true)
  end

  def calculate_price_without_reservation_condition(condition, price)
    price = (price*condition.percent)/100 + price
    number_to_currency(price, :unit => "€", :separator => ",", :delimiter => ".", strip_insignificant_zeros: true)
  end

end
