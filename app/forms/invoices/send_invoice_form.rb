class Invoices::SendInvoiceForm
  include ActiveModel::Model

  attr_accessor :email, :invoice_id, :invoice_type,
                :invoice, :message

  validates :email, presence: true
  validates :invoice_id, presence: true
  validates :invoice_type, presence: true
  validates :message, presence: true

  def initialize(params = {})
    @message = 'En el documento adjunto encontrará el recibo correspondiente al alquiler de este mes.'
    super(params)
    @invoice = invoice
  end

  def submit!
    return false if invalid?

    # se envia el correo
    UserMailer.send_invoice_to_user(
      email: email,
      invoice: @invoice,
      message: message
    ).deliver_now
  end

  def invoice
    return nil unless invoice_id && invoice_type

    @invoice = invoice_type.constantize.find(invoice_id)
  end

  def candidate_emails(reservation:, invoice:)
    @candidate_emails ||= begin
      clients_ids = reservation.clients_reservations.pluck(:user_id, :business_id).flatten.compact
      emails = Client.where(id: clients_ids).pluck(:email)
      if invoice.is_a?(Invoice)
        emails + [invoice.customer.email]
      else
        rectification_invoice = invoice
        emails + [rectification_invoice.invoice.customer.email]
      end.uniq.compact
    end
  end
end
