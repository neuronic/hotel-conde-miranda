class RectificationInvoicePdf < Prawn::Document

  # require "prawn/gmagick"

  def initialize(invoice, reservation, rectification)
    super()
    @invoice = invoice
    @reservation = reservation
    @rectification = rectification
    @building = @reservation.building
    header
    data_company
    summary_reservation
    line_items
    line_review
    concept
    
    payments
    ad
    footer
  end

  def header
    # logopath = "#{Rails.root}/app/assets/images/logo_pl.jpg"
 
    # Displays the image in your PDF. Dimensions are optional.
    # image logopath, :width => 197, :height => 60, :at => [0, cursor]
    image @building.invoice_address.logo.path, :width => 197, :height => 60, :at => [0, cursor]
    text_box @building.invoice_address.corporate_name, :at => [200, cursor],
      :align => :right, :size => 9
    move_down(12)
    text_box @building.invoice_address.tax_domicile, :at => [200, cursor],
      :align => :right, :size => 9
    move_down(12)
    text_box "#{@building.invoice_address.city}, #{@building.invoice_address.country}", :at => [200, cursor],
      :align => :right, :size => 9    
    move_down(12)
    text_box @building.invoice_address.cif, :at => [200, cursor], :align => :right, :size => 9
    move_down(12)
    text_box @building.invoice_address.email_contact, :at => [200, cursor],
      :align => :right, :size => 9
    move_down(12)
    text_box @building.invoice_address.web_site, :at => [200, cursor],
      :align => :right, :size => 9
    move_down(12)
    text_box @building.invoice_address.free_text, :at => [200, cursor], :align => :right, :size => 9

    #text_box "VIVIENDAS DE USO TURÍSTICO", :at => [200, cursor],
    #  :align => :right, :size => 9
    move_down(30)
    
    stroke_horizontal_rule
    move_down(10)
    text "FACTURA RECTIFICATIVA", :align => :center
    move_down(10)
    stroke_horizontal_rule
    move_down(10)
  end

  def footer
    # footer
    bounding_box [bounds.left, bounds.bottom + 25], :width  => bounds.width do
        font "Helvetica"
        stroke_horizontal_rule
        move_down(5)
        text @building.invoice_address.registry_data, :size => 7
    end
  end

  def ad
    if ad = @building.ad_pdfs.find_by(active: true)
        move_down(30)
        bounding_box [bounds.left, bounds.bottom + 125 ], :width  => bounds.width, :height => 100 do
          image ad.image.path, width: 540,  :height => 100, :at => [0, cursor]
        end
      
    end
  end

  def data_company
    customer = @invoice.customer
    address = @reservation.billing_address
    move_down(20)
    if customer.class.name == "User"
      name = "#{customer.name} #{customer.surname}"
      cif = !customer.nif.blank? ? "C.I.F. / N.I.F. #{customer.nif}" : "Pasaporte/passport: #{customer.passport}"
    else
      name = customer.business_name
      cif = "C.I.F. / N.I.F. #{customer.cif}"
    end
    data = [ ["Cliente / Client", "","No.Factura Rectif. / No. Rectification Invoice", "#{@rectification.number}"], 
    ["#{name}", "", "", ""], 
    ["#{address.address},  #{address.address_1 if address.address_1.present?}", "", "Fecha de factura / Invoice date", "#{@invoice.date.to_date}"], 
    ["#{address.zip_code}", "", "Código cliente / Client code", "#{customer.id}"], 
    ["#{address.city}", "", "Rectificación factura / Invoice rectified","#{@invoice.number}"],
    ["#{address.nationality}", "", "", ""], 
    ["#{cif}", "","",""]]

    table(data, :width => 540, :cell_style => { :size => 9, :text_color => "346842" }) do

      cells.padding = [1, 5]
      cells.borders = []
      row(0).border_width = 1
      row(0).font_style = :bold
      

      columns(0).borders = [:right, :left]
      columns(2).borders = [:left]
      columns(3).borders = [:right]

      row(6).columns(2).borders = [:bottom, :left]
      row(6).columns(3).borders = [:bottom, :right]

      row(0).columns(0).borders = [:bottom, :right, :left, :top]
      row(0).columns(2).borders = [:bottom, :left, :top]
      row(0).columns(3).borders = [:bottom, :right, :top]
      
      row(0).columns(0..3).padding = 5
      row(6).columns(0).borders = [:bottom, :right, :left]
    end

  end

  def summary_reservation
    move_down(20)

    data = [ ["Entrada / Check-in", "Salida / Check-out","Apartamento / Apartment"], 
    ["#{@reservation.start_date.strftime('%d/%m/%y')}", "#{@reservation.end_date.strftime('%d/%m/%y')}", "#{@reservation.apartments.first.apartment_description}"]]

    table(data, :width => 540, :cell_style => { :size => 9, :text_color => "346842" }) do

      row(0).font_style = :bold
      self.row_colors = ["DDDDDD", "FFFFFF"]
      self.header = true
      self.cell_style = { size: 8 }
    end
  end


  def line_items
    move_down(20)


    table item_table_data, :width => 540 do
      # row(0).font_style = :bold
      # columns(1..3).align = :right
      # self.header = true
      # self.column_widths = {0 => 200, 1 => 100, 2 => 100, 3 => 100}
      row(0).font_style = :bold
      self.row_colors = ["DDDDDD", "FFFFFF"]
      self.header = true
      self.cell_style = { size: 8 }
    end

  end

  def item_header
    # ["Concepto / Concept", "Descripción / Description", "Precio /Price", "Noches/Unid. / Nights/unit", "Impuesto / Tax", "Total" ] 
    ["Concepto/Concept", "Descripción/Description", "Base", "Noches/Unid./Nights/unit", "IVA/VAT", "Total" ] 
  end

  def item_rows
    # @invoice.billings_in.map { |b|
    #   [b.service.present? ? b.service.name : b.reservation.apartments.first.name, b.description, b.service.present? ? precision(b.price) : precision(b.amount / b.night), b.night, percent(b.tax), precision(- b.amount)]
    # }

    @invoice.invoice_items.map { |i|
      [i.concept, i.description, precision(i.unit_price_base), i.unit, percent(i.vat_type), precision(- i.total_price)]
    }
  end

  def item_table_data
    [item_header, *item_rows] 
  end

  def line_review

    move_down(30)
    data = [ ["","IVA/VAT", precision(@rectification.tax_amount)],["","SUBTOTAL", precision(@rectification.base)], ["","TOTAL", precision(@rectification.total)]]
    
    table(data, :width => 540, :column_widths => [300, 140, 100], :cell_style => { :size => 9, :text_color => "000000" }) do

      # cells.padding = [1, 5]
      cells.borders = []
      # row(0).border_width = 1
      row(2).font_style = :bold
      row(1).columns(1..2).borders = [:bottom]
      # row(0).columns(0..3).padding = 5
      # row(4).font_style = :bold
    end
  end

  def concept
    move_down(20)
    stroke_horizontal_rule
    move_down(10)
    text "CONCEPTO / CONCEPT: "
    move_down(10)
    text "#{@rectification.concept}"
  end


  def payments
    bounding_box [bounds.left, bounds.bottom + 180], :width  => bounds.width do
        font "Helvetica"
        text "Métodos de pago/Payment methods", :size => 7
        stroke_horizontal_rule
        move_down(5)
        @reservation.payment_summaries.each do |p|
          text "Pago/Payment: #{p.translation_method} - #{p.created_at.strftime('%d/%m/%y')} - #{precision(p.amount)}", :size => 7
          move_down(5)
        end
    end
  end


  def precision(num)
    num.round(2).to_s + " €"
  end

  def percent(num)
    num.to_s + " %"
  end




end