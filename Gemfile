source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'
# Use mysql as the database for Active Record
gem 'mysql2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Bootsrap
gem "therubyracer"
gem "less-rails" #Sprockets (what Rails 3.1 uses for its asset pipeline) supports LESS
gem "twitter-bootstrap-rails"
gem 'devise-bootstrap-views'
gem "bootstrap-switch-rails"

# Devise
gem 'devise'

#haml
gem 'haml'
gem "haml-rails", "~> 0.9"

#simple form
gem 'simple_form'
gem 'country_select'
gem "nested_form"
gem 'momentjs-rails', '~> 2.9',  :github => 'derekprior/momentjs-rails'
gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true

#SEO
gem 'friendly_id', '~> 5.1.0' # Note: You MUST use 5.0.0 or greater for Rails 4.0+
gem 'metamagic'
gem "dynamic_sitemaps"
gem 'roboto'

#paginas estáticas
gem 'high_voltage', '~> 2.3.0'
#wizard
gem 'wicked'
#translations
gem 'globalize', '~> 5.0.0'
gem 'globalize-accessors'
#images
gem 'carrierwave'
# Comment require RMagic for production
# Descomment for develpment
gem 'rmagick', '2.13.2'#, :require => 'RMagick'
gem "jquery-fileupload-rails"
gem 'mini_magick'
gem 'prettyphoto-rails'

#translate routes
gem 'route_translator'

#status
gem 'aasm'

#contact
gem 'mail_form'

#text editor
gem 'ckeditor', github: 'galetahub/ckeditor'

#cookies
gem 'cookies_eu'

#google-analytics
#gem 'rack-google-analytics'
gem 'google-analytics-rails', '1.1.1'

#order dop and drag
# gem 'acts_as_list'

#busquedas
gem 'ransack', '~> 1.8.4'
gem 'will_paginate', '~> 3.1.0'
gem 'will_paginate-bootstrap'

#busquedas por fechas y control de las mismas
gem 'by_star', :git => "git://github.com/radar/by_star"

#calendarios parte publica
gem 'jquery-ui-rails'

#date validator
gem 'date_validator'

#librería javascript para chart
gem "chartkick"
gem 'groupdate'
gem 'active_median'

#gema para placeholder de dates
gem 'maskedinput-rails'

#paypal
gem "paypal-express"
#validation jquery
gem "jquery-form-validator-rails"

#view emails
gem "mail_view", "~> 2.0.4"

#font-awesome public view
gem "font-awesome-rails"

#CATCH ERRORS PAGES
gem 'exception_handler'

#whenerver
gem 'whenever', :require => false

#pdf
gem 'prawn-rails'
gem "prawn-table"

#logic destroy
gem "paranoia", "~> 2.0"

#google maps
gem 'gmaps4rails'
gem 'underscore-rails'

gem 'exception_notification'

# canonical url SEO
gem 'canonical-rails', github: 'jumph4x/canonical-rails'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'capistrano-rails'
	gem 'capistrano', '~> 3.2.0'
	gem 'capistrano-rvm'
	gem 'capistrano-passenger'
  gem "letter_opener"
  gem 'pry-byebug'
  gem 'annotate'
  gem 'bullet'
  gem 'rubocop', require: false
end

group :test do
  #testing
  gem "rspec"
  gem 'factory_girl_rails'
  gem 'shoulda-matchers'
  gem 'faker'
  gem 'database_cleaner'
end

