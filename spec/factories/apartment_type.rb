FactoryGirl.define do
  factory :building do
    name { Faker::Company.name }
  end
end