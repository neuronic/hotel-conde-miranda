#  id                 :integer          not null, primary key
#  name               :string(255)
#  address            :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  invoice_address_id :integer

# == Schema Information
#
# Table name: invoice_addresses
#
#  id                        :integer          not null, primary key
#  corporate_name            :string(255)
#  tax_domicile              :string(255)
#  country                   :string(255)
#  city                      :string(255)
#  cif                       :string(255)
#  email_contact             :string(255)
#  web_site                  :string(255)
#  logo                      :string(255)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  registry_data             :string(255)
#  free_text                 :text(65535)
#  client_accountant_account :string(255)
#  printed                   :string(255)
#  invoice_description       :string(255)
#  type_opration_one         :string(255)
#  type_opration_two         :string(255)


FactoryGirl.define do
  factory :building do
    name { Faker::Company.name }
    address { Faker::Address.street_name }
    factory :invoice_address do
      corporate_name {Faker::Company.name  }
      tax_domicile { Faker::Address.street_name }
      country { Faker::Address.country_code }
      cif { Faker::Company.swedish_organisation_number }
      email_contact { 'empresa@empresa.com'}
      web_site { 'www.google.es'}
      registry_data { Faker::Company.swedish_organisation_number }
      free_text { 'Texto libre'}
      client_accountant_account { '004'}
      printed { 'printed'}
      invoice_description {' Invoice Description'}
      type_opration_one { '001'}
      type_opration_two { '001'}
    end
    after :create do |building|
      create_list :apartment_type, 3, building: building   # has_many
    end
  end
end