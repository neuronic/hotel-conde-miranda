require 'test_helper'

class Admin::ApartmentTypesControllerTest < ActionController::TestCase
  setup do
    @admin_apartment_type = admin_apartment_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_apartment_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_apartment_type" do
    assert_difference('Admin::ApartmentType.count') do
      post :create, admin_apartment_type: { num_bedrooms: @admin_apartment_type.num_bedrooms, num_persons: @admin_apartment_type.num_persons }
    end

    assert_redirected_to admin_apartment_type_path(assigns(:admin_apartment_type))
  end

  test "should show admin_apartment_type" do
    get :show, id: @admin_apartment_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_apartment_type
    assert_response :success
  end

  test "should update admin_apartment_type" do
    patch :update, id: @admin_apartment_type, admin_apartment_type: { num_bedrooms: @admin_apartment_type.num_bedrooms, num_persons: @admin_apartment_type.num_persons }
    assert_redirected_to admin_apartment_type_path(assigns(:admin_apartment_type))
  end

  test "should destroy admin_apartment_type" do
    assert_difference('Admin::ApartmentType.count', -1) do
      delete :destroy, id: @admin_apartment_type
    end

    assert_redirected_to admin_apartment_types_path
  end
end
