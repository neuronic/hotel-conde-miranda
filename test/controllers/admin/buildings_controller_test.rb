require 'test_helper'

class Admin::BuildingsControllerTest < ActionController::TestCase
  setup do
    @admin_building = admin_buildings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_buildings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_building" do
    assert_difference('Admin::Building.count') do
      post :create, admin_building: {  }
    end

    assert_redirected_to admin_building_path(assigns(:admin_building))
  end

  test "should show admin_building" do
    get :show, id: @admin_building
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_building
    assert_response :success
  end

  test "should update admin_building" do
    patch :update, id: @admin_building, admin_building: {  }
    assert_redirected_to admin_building_path(assigns(:admin_building))
  end

  test "should destroy admin_building" do
    assert_difference('Admin::Building.count', -1) do
      delete :destroy, id: @admin_building
    end

    assert_redirected_to admin_buildings_path
  end
end
