require 'test_helper'

class Admin::ReservationConditionsControllerTest < ActionController::TestCase
  setup do
    @admin_reservation_condition = admin_reservation_conditions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_reservation_conditions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_reservation_condition" do
    assert_difference('Admin::ReservationCondition.count') do
      post :create, admin_reservation_condition: {  }
    end

    assert_redirected_to admin_reservation_condition_path(assigns(:admin_reservation_condition))
  end

  test "should show admin_reservation_condition" do
    get :show, id: @admin_reservation_condition
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_reservation_condition
    assert_response :success
  end

  test "should update admin_reservation_condition" do
    patch :update, id: @admin_reservation_condition, admin_reservation_condition: {  }
    assert_redirected_to admin_reservation_condition_path(assigns(:admin_reservation_condition))
  end

  test "should destroy admin_reservation_condition" do
    assert_difference('Admin::ReservationCondition.count', -1) do
      delete :destroy, id: @admin_reservation_condition
    end

    assert_redirected_to admin_reservation_conditions_path
  end
end
