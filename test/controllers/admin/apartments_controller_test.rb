require 'test_helper'

class Admin::ApartmentsControllerTest < ActionController::TestCase
  setup do
    @admin_apartment = admin_apartments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_apartments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_apartment" do
    assert_difference('Admin::Apartment.count') do
      post :create, admin_apartment: {  }
    end

    assert_redirected_to admin_apartment_path(assigns(:admin_apartment))
  end

  test "should show admin_apartment" do
    get :show, id: @admin_apartment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_apartment
    assert_response :success
  end

  test "should update admin_apartment" do
    patch :update, id: @admin_apartment, admin_apartment: {  }
    assert_redirected_to admin_apartment_path(assigns(:admin_apartment))
  end

  test "should destroy admin_apartment" do
    assert_difference('Admin::Apartment.count', -1) do
      delete :destroy, id: @admin_apartment
    end

    assert_redirected_to admin_apartments_path
  end
end
