# CONFIGURAR PROYECTO PARA GUARDAR DATOS SEGUROS EN BD.

First we need to generate a key pair. This creates two keys, a public key which will only be used to encrypt data, and a private key, which will only be used to decrypt data. The private key is protected by a password know only to us. When it comes to choosing strong passwords, I suggest using [Diceware](http://world.std.com/~reinhold/diceware.html). _2048_ is the key size in bits. Bigger is better, but also slower; 2048 is considered a good trade off between speed and encryption strength. We are also limited by this to encrypting as most 2048 bits, more on this below.

Cuando pida una contraseña introducir y validar, ya que esta contraseña es la que se necesitará para desencriptar los datos de algunos atributos de el objeto reservas.
```
% openssl genrsa -des3 -out private.pem 2048
Generating RSA private key, 2048 bit long modulus
......+++
.+++
e is 65537 (0x10001)
Enter pass phrase for private.pem: (asignar contraseña)
Verifying - Enter pass phrase for private.pem: (verificar contraseña)
```

A continuación, extraemos la clave pública:
```
openssl rsa -in private.pem -out public.pem -outform PEM -pubout
Enter pass phrase for private.pem: (introducir la contraseña)
writing RSA key
```
Por último, nos vamos a los secrets.yml y añadimos esta línea:
```
development:
  secret_key_base: (la que ya tengas)
  key_pem: (contraseña escogida anteriormente)
```


mysqldump -u hotelconde -p hotelCondeMiranda_production > hotelCondeMiranda_pro_19_03_04.sql
➜  ~ scp  xxx@xxxxx.xx:/home/xxxxx/backup_mysql/xxxxxx.sql Documents/backup_mysql_db/xxxx.sql