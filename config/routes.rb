Rails.application.routes.draw do
  # get '', to: redirect("/#{I18n.locale}")
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admins
  mount_roboto
  get 'sitemap.xml' => 'sitemap#sitemap', format: :xml, as: :sitemap

  if Rails.env.production?
    match "/home/deployer/apps/conde_miranda/current/invoices/invoice/:id/:basename.:extension", :controller => "admin/invoices", :action => "download", :conditions => { :method => :get }, via: :get, as: :invoice
    match "/home/deployer/apps/conde_miranda/current/invoices/rectification_invoice/:id/:basename.:extension", :controller => "admin/rectification_invoices", :action => "download", :conditions => { :method => :get }, via: :get, as: :rectification_invoice
  else
    match "#{Rails.root}/invoices/invoice/:id/:basename.:extension", :controller => "admin/invoices", :action => "download", :conditions => { :method => :get }, via: :get, as: :invoice
    match "#{Rails.root}/invoices/rectification_invoice/:id/:basename.:extension", :controller => "admin/rectification_invoices", :action => "download", :conditions => { :method => :get }, via: :get, as: :rectification_invoice
  end

  mount MailPreview => 'mail_view' if Rails.env.development?

  get '/admin', to: redirect('admin/buildings')
  root to: 'home#index'
  get '/:locale' => 'home#index', as: :root_locale

  scope :admin do
    authenticated :admin do
      root to: 'admin/buildings#index', as: :authenticated_root
    end
  end

  namespace :admin do
    resources :buildings, only: [:edit, :update, :index] do
      resources :dashboard, only: [:index] do
        get :recreate_img_versions, on: :collection
        get :export, on: :collection
      end
      # get '/dashboard' => 'dashboard#index', as: :dashboard
      resources :apartment_types, except: [:destroy, :new, :create, :show] do
        resources :prices do
          get :read_prices, on: :collection
        end
      end
      resources :apartments do
        match :change_clean, via: [:put]
        match :change_status, via: [:put]
        get :calendar, on: :member
        get :read_reservations, on: :member
        get :read_price, on: :member
        resources :disable_apartments, only: [:index] do
          put :add_disabled_dates, on: :collection
        end
      end
      resources :coupons, except: [:destroy]
      resources :calendar_dates, except: [:show]
      resources :discounts
      resources :channels, except: [:destroy]

      resources :reservations, only: [:index, :show, :new, :create, :edit, :update] do
        collection do
          # match 'search' => 'reservations#search', via: [:get, :post], as: :search
          match 'search_apartmens' => 'reservations#search_apartmens', via: [:get, :post], as: :search_apartmens
          get :index_canceled
          get :index_finished
          get :index_no_complete
        end
        get ':apartment_id/reservation' => 'reservations#reservation', on: :collection, as: :reservation
        get :copy_reservation, on: :member
        get :read_prices, on: :collection
        get :add_users, on: :member
        put :add_user, on: :member
        put :delete_user, on: :member
        get :read_reservations, on: :collection
        get :read_reservations_for_custom_calendar, on: :collection
        put :cancel, on: :member
        get ':apartment_id/summary_reservation' => 'reservations#summary_reservation', on: :collection, as: :summary_reservation
        get :new_send_confirmation
        post :send_confirmation
        # get :export, on: :collection
        resources :invoices, except: [:destroy, :show] do
          resources :rectification_invoices, only: [:new, :create]
          get :destroy_all, on: :collection
          get :new_send_invoice, on: :member
          post :send_invoice
        end
      end
      resources :clients, except: [:destroy] do
        get :export, on: :collection
        get :search, on: :collection
      end

      resources :services do
        match :change_status_services, via: [:put]
        get :read_datas, on: :member
      end
      resources :finances, only: [:index] do
        get :search_invoices, as: :search_invoices, on: :collection
        get :export_finances, on: :collection
      end
      resources :statistics, only:[:index] do
        get :search_statistics, as: :search_statistics, on: :collection
      end

      resources :invalid_dates, only: [:index, :new, :create, :edit, :update, :destroy]

      resources :ad_pdfs, only: [:index, :new, :create, :edit, :update, :show, :destroy] do
        match :change_status, via: [:put]
      end

      resources :ad_homes, only: [:index, :edit, :update, :show] do
        match :change_status, via: [:put]
      end

      resources :config_calendars, only: [:index, :new, :create, :edit, :update, :show, :destroy]
      resources :sliders, only: [:edit, :index, :update]
      namespace :cms do
        resources :locations, only: [:edit, :index, :update]
        resources :faq_pages, only: [:edit, :update]
      end
      resources :reservation_conditions do
        get :read_percent, on: :member
        put :change_active_desactive, on: :member
      end
      resources :promotions do
        match :change_status_promotions, via: [:put]
      end
    end
    resources :admins
  end

  scope '/:locale', locale: /en|es|pt/ do
    get '/page/:id' => 'pages#show', as: :page, format: false
    get '/' => 'home#index', as: :root_public
    get 'reservation/new' => 'build#new'
    localized do
      get 'location' => 'home#location', as: :location
      get 'contact' => 'home#contact', as: :contact
      post :create_contact, to: 'home#create_contact', as: :create_contact
      get 'services' => 'home#services', as: :services
      get 'promotions' => 'home#promotions', as: :promotion
      get 'faqs' => 'home#faqs', as: :faqs
      resources :apartments, only: [:show, :index] do
        get 'search', on: :collection
        get 'get_invalid_dates', on: :collection
        get 'get_min_nights', on: :collection
      end
      resources :reservations, only: [] do
        resources :build, controller: 'build', only: [:new, :show, :update]
      end
    end
  end

  resources :calendars, only: [] do
    get :dates_conditions, on: :collection
  end

  resources :reservations, only: [] do
    post :tpv_notifier, on: :collection
    get :tpv_ok
    get :tpv_ko
    put 'check_send/:transaction_id' => 'reservations#check_send', on: :collection
    get :paypal_ok
    get :paypal_ko
  end
end
