
	host "www.palacioplcondedemiranda.com"

	sitemap :site_es do
	  url root_public_url(:locale => :es), last_mod: Time.now, change_freq: "monthly", priority: 1.0
   	url location_es_url 
  	url services_es_url
  	url contact_es_url
  	url promotion_es_url
	end

	sitemap_for Apartment.active, name: :apartamentos_activos do |apartment|
	  #url apartment
	  url apartment_es_url(apartment), last_mod: apartment.updated_at, priority: 0.7
	end

	host "www.palacioplcondedemiranda.com"

	sitemap :site_en do
	  url root_public_url(:locale => :en), last_mod: Time.now, change_freq: "monthly", priority: 1.0
  	url location_en_url
  	url services_en_url
  	url contact_en_url
  	url promotion_en_url
	end


	sitemap_for Apartment.active, name: :active_apartments do |apartment|
	  #url apartment
	  url apartment_en_url(apartment), last_mod: apartment.updated_at, priority: 0.7
	end

	# host "www.palacioplcondedemiranda.com"

	# sitemap :site_pt do
	#   url root_public_url(:locale => :pt), last_mod: Time.now, change_freq: "monthly", priority: 1.0
 #  	url location_pt_url
 #  	url services_pt_url
 #  	url contact_pt_url
 #  	url promotion_pt_url
	# end


	# sitemap_for Apartment.active, name: :active_apartments_pt do |apartment|
	#   url apartment
	#   url apartment_pt_url(apartment), last_mod: apartment.updated_at, priority: 0.7
	# end









	# You can have multiple sitemaps like the above – just make sure their names are different.

	# Automatically link to all pages using the routes specified
	# using "resources :pages" in config/routes.rb. This will also
	# automatically set <lastmod> to the date and time in page.updated_at:
	#
	#   sitemap_for Page.scoped

	# For products with special sitemap name and priority, and link to comments:
	#
	#   sitemap_for Product.published, name: :published_products do |product|
	#     url product, last_mod: product.updated_at, priority: (product.featured? ? 1.0 : 0.7)
	#     url product_comments_url(product)
	#   end





# If you want to generate multiple sitemaps in different folders (for example if you have
# more than one domain, you can specify a folder before the sitemap definitions:
# 
#   Site.all.each do |site|
#     folder "sitemaps/#{site.domain}"
#     host site.domain
#     
#     sitemap :site do
#       url root_url
#     end
# 
#     sitemap_for site.products.scoped
#   end

# Ping search engines after sitemap generation:
#
#   ping_with "http://#{host}/sitemap.xml"
# ping_with "http://#{host}/sitemap.xml"