# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, %w{deployer@82.223.53.72}
role :web, %w{deployer@82.223.53.72}
role :db,  %w{deployer@82.223.53.72}


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server '82.223.53.72', user: 'deployer', roles: %w{web app}, my_property: :my_value



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
 # set :ssh_options, {
 #   # keys: %w(/home/rlisowski/.ssh/id_rsa),
 #   forward_agent: false,
 #   auth_methods: %w(password)
 # }
#
# And/or per server (overrides global)
# ------------------------------------
# server '82.223.53.72',
#   user: 'deployer',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'deployer', # overrides user setting above
#     # keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: true,
#     auth_methods: %w(password),
#     password: 'h3S5neT2'
#   }

set :rvm_ruby_version, 'ruby-2.2.2@hotelcondemiranda'
set :rvm_path, '/home/deployer/.rvm/rubies'
set :rvm_ruby_string,  ENV['GEM_HOME'].gsub(/.*\//,"")
set :rvm_type, :user

on roles(fetch(:passenger_roles)), in: fetch(:passenger_restart_runner), wait: fetch(:passenger_restart_wait), limit: fetch(:passenger_restart_limit) do
  with fetch(:passenger_environment_variables) do
    # Version-specific restart happens here.
  end
end