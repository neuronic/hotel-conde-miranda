# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( admin.js public_v1.js )
Rails.application.config.assets.precompile += %w( admin.css application-print.css public_v1.css )
Rails.application.config.assets.precompile += %w( prettyPhoto/default/sprite.png prettyPhoto/default/sprite_x.png prettyPhoto/default/sprite_y.png prettyPhoto/default/sprite_next.png prettyPhoto/default/sprite_prev.png prettyPhoto/default/default_thumb.png prettyPhoto/default/loader.gif prettyPhoto/light_rounded/sprite.png prettyPhoto/light_rounded/btnNext.png prettyPhoto/light_rounded/btnPrevious.png prettyPhoto/dark_rounded/sprite.png
	prettyPhoto/dark_rounded/contentPattern.png prettyPhoto/dark_rounded/btnNext.png prettyPhoto/dark_rounded/btnPrevious.png prettyPhoto/dark_rounded/loader.gif prettyPhoto/dark_square/loader.gif prettyPhoto/dark_square/sprite.png prettyPhoto/dark_square/btnNext.png prettyPhoto/dark_square/btnPrevious.png prettyPhoto/light_square/sprite.png prettyPhoto/light_square/btnNext.png prettyPhoto/light_square/btnPrevious.png prettyPhoto/facebook/sprite.png prettyPhoto/facebook/contentPatternTop.png
	prettyPhoto/facebook/contentPatternLeft.png prettyPhoto/facebook/contentPatternRight.png prettyPhoto/facebook/loader.gif prettyPhoto/facebook/btnNext.png prettyPhoto/facebook/btnPrevious.png prettyPhoto/facebook/contentPatternBottom.png prettyPhoto/facebook/default_thumbnail.gif prettyPhoto/light_rounded/loader.gif)
Rails.application.config.assets.precompile += %w( ckeditor/* )