HighVoltage.configure do |config|
  config.route_drawer = HighVoltage::RouteDrawers::Root
  config.routes = false
  config.layout = 'public'
end