# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'conde_miranda'
set :repo_url, 'git@bitbucket.org:luismiv85/hotel-conde-miranda.git'
set :user, 'deployer'

# set :passenger_roles, :app                  # this is default
# set :passenger_restart_runner, :sequence    # this is default
# set :passenger_restart_wait, 5              # this is default
# set :passenger_restart_limit, 2             # this is default
# set :passenger_environment_variables, { :path => '/home/deployer/.rvm/gems/ruby-2.1.5@exclusiver-menorca/gems/passenger-4.0.53/bin:$PATH'}
# set :passenger_restart_command, 'passenger-config restart-app'
# set :passenger_restart_options, -> { "#{deploy_to} --ignore-app-not-running" }
set :passenger_rvm_ruby_version, 'ruby-2.1.5@exclusiver-menorca'
# Default branch is :master
set :branch, 'production'
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w[config/database.yml config/secrets.yml config/private.pem config/public.pem config/settings.yml]

# Default value for linked_dirs is []
set :linked_dirs, %w[log tmp/pids tmp/cache tmp/sockets vendor/bundle public/uploads public/sitemaps invoices]
set :bundle_binstubs, nil
# Default value for default_env is {}
# set :default_env, :staging

# Default value for keep_releases is 5
# set :keep_releases, 5
on roles(fetch(:passenger_roles)), in: fetch(:passenger_restart_runner), wait: fetch(:passenger_restart_wait), limit: fetch(:passenger_restart_limit) do
  with fetch(:passenger_environment_variables) do
    # Version-specific restart happens here.
  end
end

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end

set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }
